---
title: "Socratic FAQ"
description: "Pantrypoints FAQ"
image: /og/pantrypoints.jpg
---



{{< r ava="Asker" >}}
What is Pantrypoints?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
It's a points-based economic system that implements the **grain-based valuation** proposal of Adam Smith in "The Wealth of Nations". 
{{< /l >}}

{{< r ava="Asker" >}}
What's wrong with the current economic system?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
It's based on selfish-interest. This leads to marginal utility and 'diminishing returns' which then leads to the concept of profit maximization within Neoclassical Economics.
{{< /l >}}


## All-Revenue Maximization, Not Just Profits

{{< r ava="Asker" >}}
What's wrong with profit maximization?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
Adam Smith says that a society has 4 classes: rent-earners, wage-earners, donation-earners, and profit-earners. 

Profit maximization over-feeds the profit-earners to the point that it starves the other 3 classes. This leads to governments with huge debts, workers needing to work 2 or more jobs, and research not getting funded. Statistically, it leads to the productivity-wage gap. This is overfeeding is most obvious in the finance sector, as explained by Jeremy Grantham: 
{{< /l >}}

{{< youtube hIKG2YpFoFQ >}}

{{< l ava="Pantrypoints" >}}
If there is profit-maximization, then there should be:
- tax-maximization (or rent-maximization)
- wage-maximization
- donation-maximization (or grant-maximization). 
{{< /l >}}

{{< r ava="Asker" >}}
So Adam Smith's Wealth of Nations aimed to raise wages, tax revenue, and donations (not just business profits) so that total wealth would be increased. 

But how was this supposed to be done?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
Through friendly competition and incentivization of all sectors to address the needs of society and put all revenues on a more natural level. In this way, a nation would run on all gears fully engaged instead of being carried on a single gear of profits that grinds the others away, creating less total wealth. 

The latter is now seen as Western investors grinding away US and Europe to overfeed China.   
{{< /l >}}


{{< r ava="Asker" >}}
So your points-based economic system aims to compete with the money system to prevent money-bubbles and financial crsies?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
Yes. The idea is that you have an option to either buy with money or purchase with points. 
{{< /l >}}

{{< r ava="Asker" >}}
Why would people pay in points? Money already works.
{{< /r >}}

{{< l ava="Pantrypoints" >}}
Money is expensive. It needs to be printed or stored in vaults and guarded and carried in your pocket. It loses value because of inflation. Even credit cards are liable to fraud, fees, and interest rates. Money supply is ultimately controlled by banks and not by people, as explained by Jeremy Grantham above. 

So it's not really economically democratic and therefore can never unlock the full potential and maximize the real productivity or value of a nation. Rather, it represents what monied people think is productive. 
{{< /l >}}

{{< r ava="Asker" >}}
Are points cheaper and more transparent and democratic?  
{{< /r >}}


{{< l ava="Pantrypoints" >}}
Of course. Points are stored on the cloud or on bilateral paper-cards that cannot be used by third parties. No expensive vaults or ATMs needed. The value of each point is pegged to 1 kilogram of grain, such as rice or wheat, which 'inflates' far less than money.
{{< /l >}}

{{< r ava="Asker" >}}
There's a rice crisis now and rice prices are shooting up in most countries. Won't the sudden higher grain prices make the points system suddenly expensive too? 
{{< /r >}}

{{< l ava="Pantrypoints" >}}
The peg is based on the **common or usual price** of grains, not their crisis price. 
{{< /l >}}


{{< r ava="Asker" >}}
So points can reduce the cost of maintaining an economy. But how can they increase the revenue and productivity of the other classes?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
Each class has its own points category. For example we will use:
- Tax Points to increase goverment revenue (rent)
- Exchange Points to increase wages
- Donation Points to increase donations

The points create value out of nothing, by mere agreement between parties. The points-value then adds to the fiat-money-value to increase total wealth.
{{< /l >}}


## Points Banking as the Store of Value

{{< r ava="Asker" >}}
So the points make the system cheap to run, the grain-based valuation makes it stable, the point-types increase revenue. 

But if points are created by mere agreement, how do you prevent the sudden increase in points? The US fiat was increased by quantitative easing. It caused inflation eventually. Venezuela increased its fiat too, causing inflation immediately.  
{{< /r >}}


{{< l ava="Pantrypoints" >}}
Points are based on **real productivity**. Fiat money is not -- it is based on the whim of the central bank.

To create points, two parties have to agree based on the productivity that they **actually** have. The reality of their productivity is verified by a 'points-banker' who administers the local points system.
{{< /l >}}

{{< r ava="Banker" >}}
I'm a career banker. You say that points-banking is the administration of points. But does that mean that Pantrypoints wants to replace money-banking and finance? [This was actually asked by a Vice President of a division of an Australian Bank in 2018]
{{< /r >}}

{{< l ava="Pantrypoints" >}}
No. The points work side by side with money to circulate value. This is similar to mobile data working side by side with wifi, or 4G with 5G. You have the freedom to connect to the internet by wifi or by mobile data, by 4G or 5G. 

The points are for people you know. Money is for people you don't. 

The main difference is that the functions of accounting, audit, finance, banking, insurance, and taxation are rolled into a single person called the Points-banker. This is because the points-data is in one integrated system. This is different from the money system which is fragmented, as proven by the different currencies in the world, as well as the different paper bills, coins, and electronic cash.

In contrast, points are just points. 
{{< /l >}}

{{< r ava="Asker" >}}
The Points-banker seems to be too powerful, being able to do accounting, taxation, finance, insurance, etc at the same time. How do you prevent abuse?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
Through competition. This is why Smith emphasized the need to have so many banks to ensure that there is competition to dilute and balance the power.

*"The recent multiplication of banking companies in the United Kingdom has alarmed many people. Their free competition actually increases the security of the public by obliging all banks to be more circumspect in their own conduct (Book 2, Chapter 2)"*

There will also be automation which we call **ISAIAH** or the `I`mpartial `S`pectator `A`utomated `I`ntelligence `A`ggregation `H`ub to monitor the points-distribution and check against selfish-interest.
{{< /l >}}


{{< r ava="Asker" >}}
What is your alternative to selfish-interest?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
**Common interest.**

This is done through the concepts of 'mutual preservation' and 'multilateral preservation'. 

It means people think about society more and give importance to the society instead of just caring for the self. Adam Smith wrote this as fellow-feeling or sympathy in the Theory of Moral Sentiments, which is the foundation of The Wealth of Nations.

Unlike philosophers who only talk about the common interest, Pantrypoints actually pushes it as an economic system. You actually build common interest with every points-transaction without you knowing it.
{{< /l >}}

{{< r ava="Asker" >}}
How?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
**Mutual** preservation is promoted with each successful **bilateral** points transaction. 

**Multilateral** preservation is promoted with each successful **multilateral** points transaction via Points-banking.

In time (maybe after 400 years of continuous use), everything gets more prosperous and stable through natural metaphysics, induced systemically, without any artificial government or religous imposition. (This natural metaphysics is a new field of knowledge which we call [Superphysics](https://www.superphysics.org))

During a crisis, people revert to the money system because of the instinct of **self-preservation**. This manifests as hoarding and an 'each-man-for-himself' mentality. 

After humans see that mutual and multilateral preservation are far better and more effective than self-preservation, then all the crises will gradually become obsolete, whether they be war, recession, inequality, global warming, etc. 
{{< /l >}}


## Bardit as the Tool of Trade

{{< r ava="Banker" >}}
How do the bilateral and multilateral points transactions actually work?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
Through the *implementation* of the bilateral points system which we call *bardit* -- a combination of **barter and moneyless credit**. 
<!-- In this way, money, and therefore selfishness, does not enter the transaction. -->
<!-- and the formalization of the 'society-entity'. -->
- Barter implies fulfillment by both parties at the same time, something very hard to do. 
- Bardit, on the other hand, is exchange with the other party fulfilling his part at **a future date**. In this sense, one party has a barter debt and the other has a barter credit.
{{< /l >}}


{{< r ava="Asker" >}}
How does bardit build common interest?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
The bardits create a chain of transactions that traces the supply chain. 

This chain is what the Physiocrats called [the Economic Table](https://superphysics.org/social/economics/solutions/modern-economic-table). It will give real-time information to policymakers on the flow on an economy, as well as the blockages that stifle the circulation of value. This will allow them to correct imbalances very early so that it does not grow into a crisis. This makes economic crises obsolete. 
{{< /l >}}

{{< r ava="Banker" >}}
Did Adam Smith propose bardits?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
He proposed something similar to solve America's sovereign debt crisis:

*"It has been said that the Americans have no gold or silver money. How can we draw from them what they do not have? In the commerce between Great Britain and the tobacco colonies, the British goods are advanced to the colonists at a pretty long credit. **They are afterwards paid for in tobacco**. It is more convenient for the colonists to pay in tobacco than in gold and silver. (Book 5, Chapter 3)"*
{{< /l >}}


{{< r ava="Asker" >}}
So to prevent crisis, you want everyone to use a government-run bardit system? How does bardit look like in the first place?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
No. Bardits are inherently person-to-person just like barter. Only points-bankers are needed. Governments only come in once the points become of common use and regulation is needed. 

Concretely, we facilitate bardit transactions via offline bardit cards or online bardit apps run by private people. 

Abstractly, the bardit points form the **memory** of the society-entity, which treats a town, city, country, or world as one entity. This will make sense with 'Pool Clearing' later. 
<!-- Government is only needed for the 'clearing' process, as the administrator of that society-entity. -->
<!-- This 'memory' is also the foundation of economic singularity via artificial intelligence (AI), something that is impossible with the money system.  -->
 <!-- with specific dynamics as explained by our [Supersociology](https://www.superphysics.org/social/supersociology/principles/part-1/chapter-01/). -->
{{< /l >}}


{{< r ava="Asker" >}}
Why can't this be done with the current money-system or credit-card system?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
When you buy something with money, the transaction might not be recorded because the seller just wants to get your money and not provide any information about the transaction. 

This is why tax evasion and avoidance are very common in money-systems. Even commercial receipts just list the seller and the item sold. They have no information on the buyer and how he got the money to pay for the item.

Without such data, the 'memory' of the society is not formed.
{{< /l >}}


## Pantrypoints, Pantrypreneur, ISAIAH

{{< r ava="Banker" >}}
Personal barter is non-taxable. So people would use your system to evade taxes. [This was an actual comment of a Vice Presidenty of a Vietnamese bank in 2016]
{{< /r >}}


{{< l ava="Pantrypoints" >}}
No, we have a taxation system called [Pointtax](/pointtax) where people pay with their productivity at their own pace. 

This reduces budget deficits while making taxation easier, aligning with [Adam Smith's taxation maxims](https://superphysics.org/social/economics/principles/taxation-maxims).
{{< /l >}}

{{< r ava="Asker" >}}
I think it would be too big and clunky for a startup to have a transaction, taxation, and AI system rolled into one platform.
{{< /r >}}


{{< l ava="Pantrypoints" >}}
Yes, that's why we split it up into three major independent parts: Pantrypoints, Pantrypreneur, and ISAIAH.

**Pantrypoints** is for the 'public'-facing transactions such as commercial transactions between strangers. It has the Pointtax system.

**Pantrypreneur** is for private transactions, or those between friends or known customers which are non-taxable. It is made up of separate implementations that have basic Sales, Ordering, Moneyless Point-of-sales (POS), Customer Relationship Management (CRM), and Human Resources system.

You can say that Pantrypreneur is for private-internal transactions, and Pantrypoints is for public-external transactions. 

**ISAIAH** makes sense of the data from both. (This threefold division mirrors our division of Superphysics into Social, Bio, and Material respetively.)
 <!-- helps build Pantrypoints by letting private businesses get used to a common user interface (UI).  -->
{{< /l >}}

{{< r ava="Asker" >}}
Pantrypreneur sounds like an Enterprise Resource Planning (ERP) System
{{< /r >}}

{{< l ava="Pantrypoints" >}}
Yes, we call it Micro Enterprise Resource Planning (MERP) because it focuses on micro and small businesses, different from ERP which focuses on large and big corporations.

If Pantrypreneur is MERP, then Pantrypoints is SERP or Social Enterprise Resource Planning, with governments acting like an enterprise for the society-entity. 

<!-- This then leads to [a fourth branch of government](https://superphysics.org/social/supersociology/principles/part-5/fourth-branch/), as the Resources Branch, to augment the Executive, Legislative, and Judicial.  -->
{{< /l >}}

<!-- The public version, between people and businesses, is then called Social Enterprise Resource Planning (SERP). While MERP is for private interest, SERP is for social interest and is aimed for governments via Pantry Govern.   -->

{{< r ava="Asker" >}}
Is there anything that Pantrypreneur, as MERP, can't do for businesses?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
Pantrypreneur can't do Accounting, Material Requirements Planning (MRP), Product Lifecycle Management (PLM), Inventory, Logistics, and E-commerce because these require matter and the money system.

However, the system will be able to do world trade, as a separate platform called [Pantrypoints World](/world). This is because global free trade is one of the biggest features of Adam Smith's system which aims to unify the global society-entity. 

All parts of the system, whether MERP or SERP, are actually geared towards making free trade possible, with or without money. We start with person-to-person trade, then town-to-town, then country-to-country, then planet-to-planet. 
{{< /l >}}


## Pool Clearing


{{< r ava="Asker" >}}
How will Pantrypoints World solve problems with the global supply chain when it doesn't do logistics?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
It will solve it indirectly through Pool Clearing.   
{{< /l >}}

{{< r ava="Asker" >}}
What's Pool Clearing?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
It's a trade system proposed by [EF Schumacher](https://wikipedia.org/wiki/E._F._Schumacher) wherein the trading countries act as one ['pool'](https://superphysics.org/research/schumacher/pool-clearing/part-1) that coordinates their foreign trade to maximize efficiency. 

It requires member countries to have a common interest that overrides national interest in order to create a single pool that fully integrates their economies with each other. Think of it like small robots creating a giant robot, just like Voltes V, Voltron, and the Constructicons combining to create Devastator.

This is very different from the current world trade system established by Bretton Woods wherein the United States has a superior position in world trade with the US dollar as the international reserve currency. 
{{< /l >}}


{{< r ava="Asker" >}}
So if the IMF-World Bank imposes the materialist, self-interested money system onto the world from the top-down, you implement the non-materialist common-interested points system from the bottom-up?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
Yes, painstakingly since 99% of people seem to want money instead of predetermined* goods and services. (*This one we got from Spinoza)
{{< /l >}}


{{< r ava="Asker" >}}
What is your replacement to the US dollar as international reserve currency?
{{< /r >}}


<!-- So Pool Clearing is supposed to end trade wars by combining economies as one, as equals, right? -->

{{< l ava="Pantrypoints" >}}
We are working on a closed cryptocurrency [Point Coins](/services/coins/) which are pegged to grains such as rice or wheat. One coin in Asia represents the value of 1 kilogram of rice, for example. 

This will give the global economy a standard value to base all values on, instead of the US Dollar which is arbitrary. The closed nature means that Point Coins are only tradeable between governments. It matches 'bank gold' which is different from market gold traded by citizens.
{{< /l >}}

<!-- Yes. The pool system harmonizes and simplifies free trade agreements and makes currency crises obsolete through a global common interest.  -->

## Try It

{{< r ava="Asker" >}}
How do I sign up?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
You can register in [the waitlist](https://hub.pantrypoints.com), or send us a messsage [here](/contact/), or send us an email at pantrypoints@gmail.com or hello@pantrypoints.com. 
{{< /l >}}


{{< r ava="Asker" >}}
Who maintains these apps?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
We have a volunteers who test and contribute to maintain them. So please don't expect them to be bug-free yet. ~~We will register a real company in Q4 2022 or Q1 2023 depending on the commitments that we get.~~ We piggyback on an NGO to help us implement them.

We predicted that [a global stagflation by 2020](https://superphysics.org/social/supersociology/precrisis-years) would make people interested in our solution sooner or later.

The banking crisis of 2023 came a bit late, but at least it's here. 
{{< /l >}}



<!-- {{< r ava="Asker" >}}
When will you have the proper apps done?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
It depends on the customer or stakeholder. We have a few proper apps for specific stakeholders, which we call Partners. These are being used now by small businesses.

The idea is that those businesses will use our points-system to survive and gain market share during the stagflation. This will then create a points-network via [Pantry Hub](/hub) that is more resilient than the money economy. The Hubs then connect with other Hubs via [Pantry World](/world).

As of September 2022, we have the following prototypes:
- [Pantrypreneur Rewards](/rewards) as a customer loyalty system for restaurants
- [Pantrypreneur Circle](/circle) as a community-based circular economy for food waste, kitchen waste, and plastic waste
- [Pantrypreneur Learn](/learn) as a small school administration system
{{< /l >}}

{{< r ava="Asker" >}}
So Pantrypreneur, Pantry Hub, Pantry World, and Pantrylitics all facilitate an integrated economic system that scale globally and work with or without money?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
Yes.
{{< /l >}}

{{< r ava="Asker" >}}
How do you plan to scale it with just volunteers? Have you looked for investors?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
We tried to find investors, but they couldn't understand how it works. They equated it to immediate-barter which is very backward and inconvenient. 

But that's wrong because none of the hundreds of transactions that we've done are immediate. They are all debt measured in points to be paid or 'claimed' later. These require machine learning to know which debtors are good and which ones are not.   

We didn't have the manpower to show these things as complete prototypes immediately. So we developed it without any support and realized we didn't need investors. 

Instead, we need **creditors**. -->
<!-- The plan is to follow the [Odoo model](https://www.odoo.com) and open source Pantrypreneur. In this way, we only need to find creditors for Hub, World, and Pantrylitics. -->
<!-- {{< /l >}}

{{< r ava="Asker" >}}
How will a moneyless system pay back the creditors?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
Each creditor will lend to a volunteer who will grow a Pantry system in the creditor's city of choice. This will support that city's economy to prevent it from collapsing, and thus very indirectly support that creditor's own money-investments.   

This will let the system scale quickly, which is needed after the China war. 

Our system allows monetization by selling goods and services for money. The money-revenue will then pay the interest of the loans, but may or may not pay back the capital. It all depends on the economy where the hub is deployed. 
{{< /l >}}


{{< r ava="Asker" >}}
It sounds like public debt.
{{< /r >}}


{{< l ava="Pantrypoints" >}}
Yes, we predict that a public debt system will be needed for the Hubs as Social Resource Planning where only interest is paid and the debt is transferred between bondholders. 

The US has a 30 trillion debt but no one expects it to be paid off because it is the base of the economy itself. The important thing is for the GDP to be much higher than the debt in order to allow interest payments. Adam Smith calls this "perpetual funding".

In our case, the value of the points economy must be higher than the value of the money debt. The money revenue from sales should of course be higher than the interest payments. This is what we are working on.

For example, `Creditor A` will 'park' his money in our Points-banking system to establish a Pantry system for City A. He will get interest revenue while he has nothing to invest on. After he finds an investment opportunity, `Creditor B` will park his money to 'replace' the money taken out by `Creditor A`.
{{< /l >}}


{{< r ava="Asker" >}}
What about for creditors who want their money back and don't care about their society?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
They can go for the private debt system for the business platforms (MERP) since these make money. Adam Smith calls this "anticipated funding".
{{< /l >}}

{{< r ava="Asker" >}}
So the money-lending is using the money-economy to build the points-economy?
{{< /r >}}

    
{{< l ava="Pantrypoints" >}}
Yes. 

The goal is to grow the points-economy because it also means that common interest will grow and selfish-interest will shrink. This is because the moral system is imbued in the points-system. 

This will then lead to less corruption, crime, racism, conflicts, war, etc. and hopefully lessen global warming (since the zero-blockchain Clearing Funds of Pantry World are supposed to compete with crypto-blockchain speculation and make it obsolete). 
{{< /l >}} -->


<!-- {{< r ava="Asker" >}}
If you were [able to predict the 2020's global stagflation](https://superphysics.org/social/supersociology/precrisis-years) then why couldn't you have the system ready by now?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
We [tried](/news/page/4/). 

But everyone just wanted money. They didn't realize that that money-based mentality was the exact thing that would create the 2020's stagflation. 

Imagine Mark Zuckerberg making the current Facebook and Messenger with just his original dorm mates. It would take their limited manpower many years just to come up with something that the actual Facebook can make in a few months.   

But that was exactly what let us overhaul our structure into a scalable modular
{{< /l >}} -->

<!-- We ended up getting caught by the very stagflation that we were meant to solve. The good thing is that we've proven to ourselves that the system works and we just have to grow it. We expect it to be resilient even for the coming financial crisis caused by all the quantitative easing and stimulus from previous years.  -->

<!-- {{< l ava="Pantrypoints" >}}
It's a 

Unlike the current system, it doesn't use money. 
{{< /l >}}

 -->