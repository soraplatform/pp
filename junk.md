- Up-to-date SEC registration or Cooperative Development Authority (CDA) registration certificate
- Copy of Latest Income Tax Return;
- Mayor's permit where the business is located;
- Audited Financial Statements for the past three (3) years preceding the date of project implementation or in case of those with operation of less than 3 years, for the years in operation and proof of previous implementation of similar projects (or in the case of startups, at least for one (1) year)
- Document showing that NGO/PO has equity to 20 percent of the total project cost, which shall be in the form of labor, land for the project site, facilities, equipment and the like, to be used in the project;
h. List and/or photographs of similar projects previously completed, if any, indicating the source of funds for implementation;
i. Sworn affidavit of secretary of the NGO/PO that none of its incorporators, organizers, directors or officers is an agent of or related by consanguinity or affinity up to the fourth civil degree to the official of the agency authorized to process and/or approved the proposed MOA, and release of funds;



The Rise of Elixir

The pandemic led to people flocking online for remote work and online shopping. This has caused significant load on servers. 

This has led to the decline of slow, interpreted languages such as Ruby, PHP, and Python, and the rise of fast, compiled ones like Go, Rust, and Elixir.

The benefits of Go and Rust are already well-known. Here, we focus on Elixir which is less heard of. 


## Elixir and Erlang

Elixir was born out of the need to address scalability and maintainability issues in the Ruby on Rails framework. 

It was created by José Valim, a well-known Ruby developer and contributor to Ruby on Rails, in 2011.

It would run on the Erlang Virtual Machine (BEAM) with the goal of having the productivity of Ruby while taking advantage of Erlang's concurrency and fault-tolerance capabilities.


## Elixir Saves Money (and the Earth)

According to Paraxial, Elixir Saves Pinterest $2 Million a Year In Server Costs. 


Steve Cohen of Pinterest says that they were able to reduce their servers by 95% from numbering 1,400 after converting several parts to Elixir.

According to him, one of the systems that ran on 200 Python servers now runs on 4 Elixir servers.

The combined effect of better architecture and Elixir saved Pinterest over $2 million per year in server costs. In addition, the performance and reliability of the systems went up despite running on drastically less hardware. 

When their notifications system was running on Java, it was on 30 c32.xl instances. 

When they switched over to Elixir, they could run on 15. Despite running on less hardware, the response times dropped significantly, as did errors.


https://paraxial.io/blog/elixir-savings





https://madnight.github.io/githut/#/pushes/2023/2


Declining as of 2023:

Javascript
Ruby
Swift


Rising:

Go
Kotlin
Dart
Elixir





Erlang :: Java :: Ruby :: Python :: Javascript
Elixir :: Kotlin :: Ruby DSL :: Python DSL :: Dart
Beam :: JVM :: Ruby Interpreter :: Python Interpreter :: Dart Compiler 
OTP :: JDK :: Gems :: Packages :: Packages
Ecto SQL :: JDBC :: Active Record :: SQLAlchemy :: Postgrest 
GenServer :: Servelet :: Sinatra :: CherryPy :: Dart SDK
Mix :: Gradle, Jenkins :: Bundler :: Pip :: Build
Phoenix :: Spring MVC :: Rails :: Django :: Flutter Web
LiveView :: Spring-websocket :: ActionCable :: Django Sockets :: Flutter web_socket_channel


Translate to Vietnamese: How it Works.
Post your item using a points price instead of a money price.
Meetup with the buyers of your item.
Pay in points.
Resilient Against Banking Crises. Pantrypoints is a resilient, new economic system that uses bilateral points through meetups in order to stop inflation and allow full employment whatever the economic conditions.

Cách hoạt động:
Đăng bài về sản phẩm của bạn với giá bằng điểm thay vì giá bằng tiền.
Gặp gỡ với những người mua hàng của bạn.
Thanh toán bằng điểm.

Chống chịu với các cuộc khủng hoảng ngân hàng. Pantrypoints là một hệ thống kinh tế mới, chống chịu và sử dụng các điểm song phương thông qua việc gặp gỡ để ngăn chặn lạm phát và cho phép việc làm đầy đủ dù trong bất kỳ điều kiện kinh tế nào.


title: "Giới thiệu"

Pantry Network là mạng xã hội cho phép mọi người mua bán bằng tiền mặt và hiện vật. Để khai thác tối đa ứng dụng của chúng tôi, vui lòng hoàn tất tiểu sử của bạn và cho biết chi tiết về những điều bạn có thể làm và những điều bạn muốn người khác làm cho bạn.

Để khai thác tối đa ứng dụng của chúng tôi, vui lòng hoàn tất hồ sơ của bạn và cho biết chi tiết về những điều bạn có thể làm và những điều mà bạn muốn người khác làm cho bạn.

Nếu bạn có bất kỳ vấn đề nào với tài khoản của mình, vui lòng gửi email đến địa chỉ pantrypoints@gmail.com


subtitle: "Cho biết tên, email và sở thích của bạn để biết khi nào Pantry sẽ có mặt tại thành phố của bạn hoặc nhận thông tin cập nhật về các cơ hội hoặc lý thuyết kinh doanh"



{{< r ava="Asker" >}}
So you avoid selfish interest by avoiding money by using barter. But how is 'bardit' different from barter?
{{< /r >}}

But to create fiat money, the central bank just has to agree with itself. 

Unlike currency which is multilateral and can be amassed quickly, points are bilateral and takes time to build up. This prevents leveraging, sudden inflation, and [emphasizes primary arbitrage over secondary arbitrage](https://superphysics.org/social/economics/principles/arbitrage). Modern economics goes for the latter arbitrage as 'buy low, sell high'.
{{< /l >}}


{{< r ava="Asker" >}}
Sudden price hyperinflation is clearly bad. But what's wrong with 'buy low, sell high'?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
The person getting the high revenue has no problem with it. 

But if you take society as one unit, then that high revenue is paid for by someone else, manifesting as high prices or inflation.  

<!-- High prices hurts the weaker the person is economically.  -->

A profiteer who succeeds in "leveraging" arbitrage will earn fat revenues. But it means that the 3 other classes will pay for it and starve their own revenues. This imbalance and starvation manifests as a crash or financial crisis later. Smith writes:

*"In her present condition, Great Britain resembles an unwholesome body with some overgrown vital parts. Her body is liable to many dangerous disorders which affect unproportioned bodies. Her blood vessel was artificially swelled beyond natural by the industry and commerce forced to circulate in it. A small stop in that great blood-vessel will very likely bring the most dangerous disorders on the whole political body. (Book 4, Chapter 7)"*

Governments try to solve profiteering through taxation. But this merely leads to tax evasion through corrupt accountants or tax officials who also want a piece of those fat revenues. The cause of all of this is selfish interest.
{{< /l >}}



{{< r ava="Asker" >}}
Wow, you seem to have thought of everything.
{{< /r >}}


{{< l ava="Pantrypoints" >}}
The Scottish Enlightenment thought of it. We just implement their blueprint and maxims into a real-world system.   
{{< /l >}}


feedback:
  title: "Competitions"
  items:
    - user : "Urban Land Institute Philippines"
      image: "/icons/uli200.jpg"
      content: "Top 5 ULI Urban Innovation Challenge 2019"
      link: "http://philippines.uli.org"
    - user : "UBX"
      image: "/icons/uhack200.jpg"
      content: "Top 10 U:Hack 2019" 
      link: "http://ubx.ph"


feedback2:
  title: "Feedback"
  items:
    - user : "seditionmusic.com"
      image: "/icons/sedition.png"
      content: "'This is a very interesting concept. One thing you cannot neglect is the legal system. For this to become internationally adopted, I believe you're going to have to propose a movement towards a universal law system (which I support)'"
      # link: "https://www.seditionmusic.com"
    - user : "mylivewellhome.com"
      image: "/icons/livewell.png"
      content: "'I love the idea of streamlining and improving governmental processes.'" 
      # link: "https://mylivewellhome.com/"



<nav class="container-{{ if .Site.Params.options.fullWidth }}fluid{{ else }}xxl{{ end }} flex-wrap flex-lg-nowrap" aria-label="Main navigation" style="font-size: 14px">

    <a class="navbar-brand order-0" style="color: dodgerblue;" href="{{ "/" | relLangURL }}" aria-label="{{ .Site.Params.Title }}">
      {{ .Site.Params.Title }}
    </a>



    Found invalid redirect lines:
04:09:37.961    - #53: /news/hatch-2015/ /news/15-10-01/
04:09:37.961      Ignoring duplicate rule for path /news/hatch-2015/.
04:09:37.961    - #72: /hub /trisactions/
04:09:37.961      Ignoring duplicate rule for path /hub.

bouceback network
bitskwela


{{ if .IsHome -}}
  <title>{{ .Site.Params.title }} {{ .Site.Params.titleSeparator }} {{ .Site.Params.titleAddition }}</title>
{{ else -}}
  <title>{{ .Title }} {{ .Site.Params.titleSeparator }} {{ .Site.Params.title }}</title>
{{ end -}}



{{ with $.Params.images -}}
  <meta name="twitter:image" content="{{ $.Permalink }}{{ index . 0 }}">
{{ else -}}
  {{ $images := $.Resources.ByType "image" -}}
  {{ $featured := $images.GetMatch "*feature*" -}}
  {{ if not $featured -}}
    {{ $featured = $images.GetMatch "{*cover*,*thumbnail*}" -}}
  {{ end -}}
  {{ with $featured -}}
    <meta name="twitter:image" content="{{ $featured.Permalink }}">
  {{ else -}}
    {{ with $.Site.Params.images -}}
      <meta name="twitter:image" content="{{ index . 0 | absURL }}">
    {{ else -}}
      <meta name="twitter:card" content="summary">
    {{ end -}}
  {{ end -}}
{{ end -}}



{{ with $.Params.images -}}
  {{ range first 6 . -}}
  <meta property="og:image" content="{{ $.Permalink }}{{ . }}">
  {{ end -}}
{{ else -}}
  {{ $images := $.Resources.ByType "image" -}}
  {{ $featured := $images.GetMatch "*feature*" -}}
  {{ if not $featured -}}
    {{ $featured = $images.GetMatch "{*cover*,*thumbnail*}" }}
  {{ end -}}
  {{ with $featured -}}
    <meta property="og:image" content="{{ $featured.Permalink }}"/>
  {{ else -}}
    {{ with $.Site.Params.images -}}
      <meta property="og:image" content="{{ index . 0 | absURL }}"/>
      <meta property="og:image:alt" content="{{ $.Site.Params.title }}">
    {{ end -}}
  {{ end -}}
{{ end -}}



<html>
  <head>
    <title>reCAPTCHA demo: Explicit render for multiple widgets</title>
    <script type="text/javascript">
      var verifyCallback = function(response) {
        alert(response);
      };
      var widgetId1;
      var widgetId2;
      var onloadCallback = function() {
        // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
        // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
        widgetId1 = grecaptcha.render('example1', {
          'sitekey' : 'your_site_key',
          'theme' : 'light'
        });
        widgetId2 = grecaptcha.render(document.getElementById('example2'), {
          'sitekey' : 'your_site_key'
        });
        grecaptcha.render('example3', {
          'sitekey' : 'your_site_key',
          'callback' : verifyCallback,
          'theme' : 'dark'
        });
      };
    </script>
  </head>
  <body>
    <!-- The g-recaptcha-response string displays in an alert message upon submit. -->
    <form action="javascript:alert(grecaptcha.getResponse(widgetId1));">
      <div id="example1"></div>
      <br>
      <input type="submit" value="getResponse">
    </form>
    <br>
    <!-- Resets reCAPTCHA widgetId2 upon submit. -->
    <form action="javascript:grecaptcha.reset(widgetId2);">
      <div id="example2"></div>
      <br>
      <input type="submit" value="reset">
    </form>
    <br>
    <!-- POSTs back to the page's URL upon submit with a g-recaptcha-response POST parameter. -->
    <form action="?" method="POST">
      <div id="example3"></div>
      <br>
      <input type="submit" value="Submit">
    </form>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>
  </body>
</html>



ADD PANTRY ERRANDS


Team introduction - Team introduction, including team members’ professional backgrounds and interests in climate data visualization and data literacy. Participants can also indicate their mentoring requests (optional) during the application period. This document should be submitted in PDF format. - Mandatory
(2) Data Story Notebook - Data Story Notebook, which will be submitted as a .tar file downloaded from Observable and re-uploaded to the ADB Challenge platform along with a short summary of the submission and link to the publication URL - Mandatory
(3) Data Links and Citation - Mandatory
(4) Discussions of the techniques used to find, gather, analyze and visualize data. Mandatory

Here are the parameters/tips to guide you on your submission:


Submissions must use public data. No sensitive data or code may be used. Treat all information about the content of the submission as acceptable for public disclosure.

No representation of national/country borders may be used in the submissions. Any submissions using maps showing country or sub-country administrative areas with an international borders must ensure that the maps are drawn using abstracted representations of national borders (e.g., hexbins or grid cartograms) or that map layers show only natural geographic areas, with all representations of national borders removed.

Submissions must avoid discussion of, or reference to, disputed borders, areas, regions, etc.

Submissions must not be derogatory or negative in focus; Submissions must emphasize resilience to climate change.

Submissions should focus on one or more of ADB's developing member countries or on comparisons between member regions.

The languages of Submissions should be a combination of English and JavaScript.


Feel free to add a document regarding your specific mentoring needs if you are selected to proceed to the next phase.
If you have already submitted your deliverables, you can still edit your participation until the 4th of November at 23:59 (GMT+8).

Please do not hesitate to contact the Admin if you have any questions ❓

All the best,
Visualizing Climate Change & Resilience - Project Management Team


  {{ if eq .Kind "home" -}}
    {{ .Scratch.Set "class" "home" -}}

<!--  using either:
- mechanical composters for wealthy villages
- black soldier fly composting for poor villages -->

https://www.linkedin.com/in/madz-birondo-815126142/

https://www.facebook.com/MaeObusa



pantry
  - hub
    <!--     - employ (needed service) -->
    - services 
    - needed services (language exchange)
    - products 
    - health 
    // medicine 
pantry
  - preneur
    - appraise 
    - clock 
    - rewards /
    - jobs /
    - sched 
    - query  
  - learn
    - klock
    - appraise
    - jobs
  - med
    - clock   
    - sched

world
pantrylitics

------
Have you ever wondered why it's so hard to find a job?
Đã bao giờ bạn tự hỏi tại sao tìm được công việc như mong muốn lại khó khăn đến thế?

It's because employers need money before they can hire workers. 
Vì những nhà tuyển dụng cần tiền trước khi họ tuyển dụng các bạn.

But to have money, they need to get customers. 
Để có tiền họ cần nhiều khách hàng.

And to have customers they need to have a product. 
Tất nhiên để có khách hàng thì không thể thiếu sản phẩm.

And to have a product they need to hire workers. 
Khi các công ty cần tạo ra sản phẩm họ sẽ cần nhiều công nhân hơn. 

Đó là vấn đề cốt lõi các bạn gặp sẽ phải khi tìm kiếm một công việc tốt.

So a lot of opportunity is lost by not having money. 


Wouldn't it be great if there were a system that let you create products and services without needing too much money? 
Và đó chính là nguyên nhân khiến chúng ta đánh mất nhiều cơ hội tốt bởi thiếu tiền?

Sẽ là điều tuyệt vời thế nào nếu có một hệ thống mới được xây dựng nhằm giúp bạn tạo ra những sản phẩm và dịch vụ mà không cần quá nhiều tiền ?

That's what Adam Smith thought of in the 18th century.
Adam Smith – nhà kinh tế học vĩ đại tại thế kỉ 18 từng đề ra những sáng kiến đó

And that's what we have created in SORA. 
Đó chính là điều SORA – “một hệ thống kinh tế đổi mới” chúng tôi vận hành

SORA is a trading system that lets you trade in money or goods or services so that you can exchange your skills and products for other products and services that you need. 
done: Pantry – một hệ thống thương mại cho phép bạn mua bán hàng hóa không chỉ bằng TIỀN mà còn có thể thay thế  bằng sản phẩm hoặc dịch vụ để bạn có thể trao đổi những kỹ năng và sản phẩm của mình cho các sản phẩm và dịch vụ khác mà bạn mong muốn.

With SORA, you can look for English teachers who want to learn Vietnamese in exchange. 
Tại Pantry, bạn có thể tìm kiếm cho mình một giáo viên Tiếng Anh bản địa - người muốn trao đổi Tiếng Việt với bạn mà không cần trả tiền. 

Or you can borrow a book in exchange for another book that you already have. 
Hoặc bạn có thể trao đổi một cuốn sách bạn đang cần với cuốn sách bạn đã đọc xong. 

Or you can buy something and pay half in money and pay half in another thing. 
Hay tuyệt vời hơn nữa là bạn có thể mua bất cứ thứ gì trên SORA mà chỉ phải trả một phần tiền mặt và một thứ gì khác của bạn sẵn có.  

With Pantry, everyone can trade, even those without money.  
Đến với Pantry, bạn và chúng tôi đều có thể mua bán ngay cả khi KHÔNG có tiền. 

So what are you waiting for?
Còn chần chờ gì nữa nào? 

Tell the SORA community what you need and let us help you get it!
Kể cho SORA những gì bạn cần và hãy để chúng tôi sẽ giúp bạn có được nó nhé! Truy cập SORA để nhận những điều tuyệt vời hơn nhéhttps://sora.asia/

Không có tiền? Không vấn đề gì! SORA là một thị trường xã hội để trao đổi hàng hoá và dịch vụ bằng tiền hoặc không cần tiền. Sử dụng SORA để tìm kiếm cá nhân hoặc công ty cần sản phẩm hoặc dịch vụ của bạn. Phiên bản beta này được thiết kế cho sinh viên, các dịch giả tự do và các doanh nghiệp nhỏ có sản phẩm hoặc dịch vụ để cung cấp nhưng có thể không có đủ tiền. Mọi người đều có thể 
      buôn bán!

Check out %{app}   = Đăng xuất
About us  = Giới thiệu
Android app available soon = Có thể chạy được trên Android
Sign up to Facebook = Đăng nhập bằng Facebook
An economic system based on social contracts = Hệ sinh thái dựa trên nền tảng hợp đồng/giao dịch

Top Product or Service = Sản phẩm và dịch vụ
The Proper Replacement to GDP = Giải pháp thay thế phù hợp cho GDP
Classical economics meets modern data science =Kinh tế học cổ điển kết hợp khoa học dữ liệu hiện đại
The study of SOcial Resource Allocation (SORA) based on Adam Smith, David Hume, and Plato, using the effort theory of value = Học thuyết Phân bổ nguồn lực xã hội (SORA) dựa trên lý thuyết Kinh tế của Adam Smith, David Hume và Plato, sử dụng lý thuyết giá trị nỗ lực

try the web app = thử ứng dụng web
or try the android app = hoặc ứng dụng trên Android
check out (something) = Thoát

SORA is a new hyperlocal economic system designed for all kinds of economic crises
Will you survive the Great Stagflation? = SORA là một mạng xã hội & hệ kinh tế phục vụ tập trung theo khu vực tiên tiến nhất xây dựng nhằm cảnh báo và khắc phục hậu quả các cuộc khủng hoảng kinh tế //  Bạn sẽ ứng phó thế nào sau Đại lạm phát?

The economic boom from the industrial revolution led to the dominance of utility and pleasure, causing the 'marginal revolution' to establish wrong ideas = Sự bùng nổ kinh tế từ cuộc cách mạng công nghiệp dẫn đến sự thống trị của sự tiện ích và giải trí, khiến 'cuộc cách mạng cận biên' hình thành những ý tưởng sai lầm.

After that, the problems began as panics, crashes, world wars, and cold wars because people were taught that the gain of others caused a loss to themselves (which is only true for gamblers and speculators -- those whose morality is based only on themselves).
SORA is a new hyperlocal economic system designed for all kinds of crises = Sau đó, các vấn đề trở nên rối loạn, đổ vỡ, thế chiến và chiến tranh lạnh bởi vì mọi người được dạy rằng lợi ích của người khác gây ra thiệt hại cho chính họ (điều này chỉ đúng với những người chơi cờ bạc và đầu cơ - những người mà đạo đức của họ chỉ dựa trên bản thân họ ). SORA là một hệ thống kinh tế siêu địa phương mới, được thiết kế cho tất cả các loại khủng hoảng.

Update March 2020: The Great Stagflation begins as predicted, courtesy of the coronavirus! = Đến tháng 3 năm 2020: Đại thảm họa bắt đầu như đã được dự đoán trước đó, nguyên nhân đến từ đại dịch Coronavirus.

No Money? No Job? No Problem! = Không tiền? Không nghề nghiệp? … Không thành vấn đề!

SORA is based on the real value of the actual goods and services relative to each other and not to fiat money. Ecommerce won't work without credit card or cashless payments, but SORA can = SORA dựa trên giá trị thực của hàng hóa và dịch vụ thực tế so với nhau và không dựa trên tiền định danh. Thương mại điện tử sẽ không hoạt động nếu không có thẻ tín dụng hoặc thanh toán không dùng tiền mặt, nhưng SORA thì có thể.

Crisis-ready. Works in all conditions
Sẵn sàng đối đầu với khủng hoảng, hoạt động trong mọi tình huống.

SORA allows both money and barter transactions, allowing you to buy and sell whether people have money or not. It only fails when society itself fails = SORA cho phép cả giao dịch tiền và hàng đổi hàng, cho phép bạn mua và bán cho dù mọi người có tiền hay không. Nó chỉ sụp đổ khi khi bản chất mạng xã hội sụp đổ.

Say Goodbye to Total Uncertainty 
SORA uses Socrates' Law of Social Cycles to predict things = SORA sử dụng Quy luật Chu kỳ xã hội của Socrates để dự đoán mọi thứ

Update March 2020 We were able to predict the crisis year, but we were around 1 year off. So there will still be uncertain, but not as much as before
Đến tháng 3 năm 2020 
Chúng tôi đã có thể dự đoán khủng hoảng hàng năm, nhưng chúng tôi đã nghỉ khoảng 1 năm. Vì vậy, sẽ có sự không chắc chắn, nhưng không nhiều như trước.


The negroes on the coast of Africa have a sign of value without money. It is a nominal sign founded on the degree of esteem which they fix in their minds for every merchandise, relative to their need for it. A certain merchandise is worth three macoutes. Another is six macoutes. Another is ten macoutes. The price is formed by comparing all merchandise with each other. They have therefore no particular money, but each kind of merchandise is money to the other = Những người da đen ở bờ biển Châu Phi họ có thể dùng vật làm tín hiệu để giao dịch với nhau mà không cần tiền. Đó là những tín vật được hình thành dựa trên mức độ quý trọng mà họ in sâu trong suy nghĩ đối với mọi hàng hóa, liên quan đến nhu cầu của họ đối với nó. Một món hàng trị giá 3 macoutes. Một cái khác là 6 macoutes. Một cái khác là 10 macoutes. Giá cả được hình thành bằng cách so sánh tất cả hàng hóa với nhau. Do đó, họ không có tiền cụ thể, nhưng mỗi loại hàng hóa là tiền cho mặt hàng kia.

How It Works = Cách thức hoạt động

SORA prevents crises, unemployment, poverty, etc by enabling flexible contracts = SORA ngăn chặn khủng hoảng, thất nghiệp, nghèo đói, v.v. bằng cách cho phép các giao dịch linh hoạt

Everything in an economy, all in one place = mọi thứ trong một nền kinh tế, tất cả trong một

SORA is a complete  economic system made up of different apps and APIs that work online or offline. = SORA là một hệ thống kinh tế hoàn chỉnh phát triển trên các ứng dụng mã nguồn mở khác nhau cho phép hoạt động online hoặc offline.


Value is in the eye of the beholder = Giá trị nằm trong mắt của người xem

In the book Pantry Red Paper Clip, a man was able to trade a paper clip for a house after one year of successive barters. We call such wealth-creation method as resource arbitrage , safer but slower than money-based speculation = Trong cuốn sách “Một chiếc kẹp giấy đỏ”, một người đàn ông đã có thể đầu tư vào chiếc kẹp giấy để lấy một ngôi nhà sau một năm liên tiếp đạt được lợi nhuận. Chúng tôi gọi phương pháp tạo ra của cải như vậy là chênh lệch giá tài nguyên, an toàn hơn nhưng chậm hơn so với đầu cơ dựa trên tiền

Pantry city. Pantry server. = mỗi Thành phố trang bị một Máy chủ

What happens when you connect a city's economy to its government? A city-state! = Điều gì xảy ra khi bạn kết nối nền kinh tế của một thành phố với Chính phủ? Một tiểu bang thành phố!

SORA City is SORA Market connected to Social Governance or SOGO which is based on the city-states mentioned in The Republic. = Thành phố SORA là Thị trường SORA được kết nối với mạng xã hội Chính phủ hay còn gọi là SOGO dựa trên các tiểu bang thành phố được đề cập trong nền Cộng hòa.

---
Bank with or without money
Ngân hàng có hoặc không cần phải có tiền

You might not have money, but you certainly have your labour
Bạn có thể không có tiền, nhưng bạn chắc chắn có sức lao động để giao dịch.

SORA has its own proposed banking system based on resource credits. Think of them as future social contracts that are non-transferrable and immutable. 
SORA có hệ thống ngân hàng đề xuất của riêng mình dựa trên các khoản tín dụng tài nguyên. Hãy coi chúng như những khế ước xã hội trong tương lai không thể chuyển nhượng và bất di bất dịch.

And no, it's not a barter system, just as we don't say that a mother cooking for her child in exchange for good behavior is barter.
Và không, đó không phải là một hệ thống đổi hàng, cũng như chúng ta không nói rằng một người mẹ nấu ăn cho con mình để đổi lấy hành vi tốt là hàng đổi hàng. 

Rather, it's a "social contract for the allocation of food".
Đúng hơn, đó là một "hợp đồng/giao dịch xã hội để phân bổ thực phẩm".
---


Don't you wish you could know if your employee or employer was good to work with before you hired, or worked for, him? 
Bạn không ước mình có thể biết liệu nhân viên hoặc người sử dụng lao động của bạn có tốt khi làm việc cùng trước khi bạn thuê hay làm việc cho anh ta không?


---
What about marriages and relationships? 
Còn hôn nhân và các mối quan hệ thì sao?

SORA has its own maching system to make better social contracts whether they be employment, purchases, marriages, or investments get cured with or without money'. 
SORA có hệ thống lập bản đồ riêng của mình để tạo ra các hợp đồng xã hội tốt hơn cho dù đó là việc làm, mua bán, hôn nhân hoặc đầu tư được chữa khỏi bằng hoặc không bằng tiền '.

---
Have you been rejected by hospitals and insurance companies because you don't have money? Find alternative treatment, or when all else fails, check the database for symptoms and cures by yourself
Bạn từng bị các bệnh viện và công ty bảo hiểm từ chối vì không có tiền? Tìm phương pháp điều trị thay thế hoặc khi vẫn thất bại, hãy tự mình kiểm tra cơ sở dữ liệu để biết các triệu chứng và cách chữa...

No child left behind (all children will get education), even during pandemics
Không có đứa trẻ nào bị bỏ lại phía sau (tất cả trẻ em sẽ được học hành), kể cả trong thời kỳ đại dịch

SORA Learn and Apprentice are modular and community-based (decentralized) learning systems for white and blue-collar jobs as alternatives to the centralized education in univerisites.
SORA Learn and Apprentice là các hệ thống và cơ sở giao tiếp dựa trên cộng đồng (dữ liệu phi tập trung), hệ thống học tập dành cho những người làm công sở như là những lựa chọn thay thế cho giáo dục tập trung ở các trường lớp. 

It is directly connected to SORA Market and SORA Match so that students get a better skills-job match.
Nó được kết nối trực tiếp với SORA Market và SORA Match để sinh viên đạt được sự phù hợp hơn giữa kỹ năng và công việc.

---
invest in your own society
đầu tư vào xã hội của chính bạn

Donate to sick people via SORA Health.
Quyên góp cho người bệnh qua SORA Health. 

Support students and apprentices on SORA Learn. 
Hỗ trợ sinh viên và người học việc trên SORA Learn. 

Check the investee's profile in SORA Match.
Kiểm tra hồ sơ của người được đầu tư trong SORA Match.


--
get entrepreneurship intelligence
có tinh thần khởi nghiệp

Are you planning to develop a new product or service?
Bạn đang có kế hoạch phát triển một sản phẩm hoặc dịch vụ mới? 

Know first if it already exists or if there's real demand for it first!
Trước tiên, hãy biết liệu nó đã tồn tại chưa hoặc nếu được thì có thông tin về nhu cầu thực sự về nó trước!

---
Get cured with or without money
Được chữa khỏi bệnh cho dù có hoặc không có tiền

Have you been rejected by hospitals and insurance companies because you don't have money? 
Bạn từng bị các bệnh viện và công ty bảo hiểm từ chối vì không có tiền? 

Find alternative treatment, or when all else fails, check the database for symptoms and cures by yourself
Tìm phương pháp điều trị thay thế hoặc khi vẫn thất bại, hãy tự mình kiểm tra cơ sở dữ liệu để biết các triệu chứng và cách chữa

---
Subscribe to Pantry = `Đăng ký` Pantry

Indicate your name, email, and interest to know when SORA will be available in your city or get updates on business opportunities or theories.
Cho biết tên, email và sở thích của bạn để biết khi nào Pantry sẽ có mặt tại thành phố của bạn hoặc nhận thông tin cập nhật về các cơ hội hoặc lý thuyết kinh doanh.




      <div class="card my-1">
        <div class="card-body py-2">
          {{ .Title }} 
        </div>
      </div>

      

      {{ if ne .Params.toc false -}}
      <nav class="d-xl-none " aria-label="Quaternary navigation">
        {{ partial "sidebar/docs-toc.html" . }}
      </nav>
      {{ end -}}


      A matching system using machine learning will then predict the integrity of the points to ensure that the points-system stays sound and sustains itself. While the Inca system used manual matching, ours will use automated matching"



<!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) -->


    <div class="timeline-4 right-4">
      <div class="card gradient-custom-4">
        <div class="card-body p-4">
          <i class="fas fa-camera fa-2x mb-3"></i>
          <h4>8:00 AM</h4>
          <p class="small text-white-50 mb-4">May 18</p>
          <p>Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto
            mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua
            dignissim
            per, habeo iusto primis ea eam.
          </p>
          <h6 class="badge bg-light text-black mb-0">New</h6>
          <h6 class="badge bg-light text-black mb-0">Admin</h6>
        </div>
      </div>
    </div>
    <div class="timeline-4 left-4">
      <div class="card gradient-custom">
        <div class="card-body p-4">
          <i class="fas fa-campground fa-2x mb-3"></i>
          <h4>7:25 PM</h4>
          <p class="small text-white-50 mb-4">May 6</p>
          <p>Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto
            mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua
            dignissim
            per, habeo iusto primis ea eam.
          </p>
          <h6 class="badge bg-light text-black mb-0">New</h6>
          <h6 class="badge bg-light text-black mb-0">Admin</h6>
        </div>
      </div>
    </div>
    <div class="timeline-4 right-4">
      <div class="card gradient-custom-4">
        <div class="card-body p-4">
          <i class="fas fa-sun fa-2x mb-3"></i>
          <h4>3:55 PM</h4>
          <p class="small text-white-50 mb-4">Apr 26</p>
          <p>Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto
            mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua
            dignissim
            per, habeo iusto primis ea eam.
          </p>
          <h6 class="badge bg-light text-black mb-0">New</h6>
          <h6 class="badge bg-light text-black mb-0">Admin</h6>
        </div>
      </div>
    </div>
    <div class="timeline-4 left-4">
      <div class="card gradient-custom">
        <div class="card-body p-4">
          <i class="fas fa-palette fa-2x mb-3"></i>
          <h4>5:24 PM</h4>
          <p class="small text-white-50 mb-4">Apr 12</p>
          <p>Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto
            mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua
            dignissim
            per, habeo iusto primis ea eam.
          </p>
          <h6 class="badge bg-light text-black mb-0">New</h6>
          <h6 class="badge bg-light text-black mb-0">Admin</h6>
        </div>
      </div>
    </div>
    <div class="timeline-4 right-4">
      <div class="card gradient-custom-4">
        <div class="card-body p-4">
          <i class="fas fa-laugh-beam fa-2x mb-3"></i>
          <h4>11:25 AM</h4>
          <p class="small text-white-50 mb-4">Apr 11</p>
          <p>Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto
            mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua
            dignissim
            per, habeo iusto primis ea eam.
          </p>
          <h6 class="badge bg-light text-black mb-0">New</h6>
          <h6 class="badge bg-light text-black mb-0">Admin</h6>
        </div>
      </div>
    </div>
    <div class="timeline-4 left-4">
      <div class="card gradient-custom">
        <div class="card-body p-4">
          <i class="fas fa-pizza-slice fa-2x mb-3"></i>
          <h4>12:30 PM</h4>
          <p class="small text-white-50 mb-4">Apr 5</p>
          <p>Lorem ipsum dolor sit amet, quo ei simul congue exerci, ad nec admodum perfecto
            mnesarchum, vim ea mazim fierent detracto. Ea quis iuvaret expetendis his, te elit voluptua
            dignissim
            per, habeo iusto primis ea eam.
          </p>
          <h6 class="badge bg-light text-black mb-0">New</h6>
          <h6 class="badge bg-light text-black mb-0">Admin</h6>
        </div>
      </div>
    </div>


          {{ range .Site.Menus.footer -}}
            <li class="list-inline-item"><a href="{{ .URL | relURL }}">{{ .Name }}</a></li>
          {{ end -}}


    <div class="wrap container-{{ if .Site.Params.options.fullWidth }}fluid{{ else }}xxl{{ end }}" role="document">
    <div class="wrap container-fluid" role="document">     


  <!-- feature -->
  {{ with .Params.feature }}
    {{ partial "feature.html" . }}
  {{ end }}
  <!-- /feature -->


  {{ with .Params.platform }}
    {{ partial "platform.html" . }}
  {{ end }}


  {{ with .Params.timeline }}
    {{ partial "timeline.html" . }}
  {{ end }}


  {{ with .Params.carousel }}
    {{ partial "carousel.html" . }}
  {{ end }}
  

  {{ with .Params.segment }}
    {{ partial "segment.html" . }}
  {{ end }}


  {{ with .Params.req }}
    {{ partial "req.html" . }}
  {{ end }}


  {{ with .Params.feedback }}
    {{ partial "feedback.html" . }}
  {{ end }}



  {{ with .Params.call_to_action }}
    {{ partial "cta.html" . }}
  {{ end }}
  

  

{{ $fillImage := .Scratch.Get "fillImageCard" }}
{{ if not $fillImage -}}
  {{ $fillImage = site.Params.fillImage }}
{{ end -}}

{{ $image := .Resources.GetMatch (printf "**%s" (index .Params.images 0)) }}
{{ if not $image -}}
  {{ $image = resources.Get (printf "%s%s" "images/" site.Params.defaultImage) }}
{{ end -}}

{{ $webp := printf "%s%s" $fillImage " webp" }}
{{ $image = $image.Resize $webp}}

{{ $lqip := $image.Resize site.Params.lqipWidth -}}

<img
  class="card-img-top img-fluid lazyload blur-up"
  src="{{ $lqip.Permalink }}"
  data-src="{{ $image.Permalink }}"
  width="{{ $image.Width }}"
  height="{{ $image.Height }}"
  alt="{{ .Title }}">
          



{{ $last := sub (len .Params.contributors) 1 }}
<p><small>Posted{{ if .Params.categories -}}&nbsp;in&nbsp;{{ range $index, $category := .Params.categories -}}{{ if gt $index 0 -}}, {{ end -}}<a class="stretched-link position-relative link-muted" href="{{ "/categories/" | absURL }}{{ . | urlize }}/">{{ . }}</a>{{ end -}}{{ end -}}&nbsp;on&nbsp;{{ .PublishDate.Format "January 2, 2006" }} by {{ if .Params.contributors -}}{{ range $index, $contributor := .Params.contributors }}{{ if gt $index 0 }}{{ if eq $index $last }} and {{ else }}, {{ end }}{{ end }}<a class="stretched-link position-relative" href="{{ "/contributors/" | relURL }}{{ . | urlize }}/">{{ . }}</a>{{ end -}}{{ end -}}&nbsp;&hyphen;&nbsp;<strong>{{ .ReadingTime -}}&nbsp;min read</strong></small><p>