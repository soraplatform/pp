---
title : "Ang Solusyon sa Pagtaas ng Presyo at Krisis Pang-Ekonomiya"
description: "Ang Pantrypoints ay isang matatag na economic system, base sa Supereconomics, na gumagamit ng bilateral points para magkaroon ng trabaho at hindi maapektuhan ng Pagtaas ng Presyo"
lead: "Ang Pantrypoints ay isang matatag na economic system, base sa Supereconomics, na gumagamit ng bilateral points para magkaroon ng trabaho at hindi maapektuhan ng Pagtaas ng Presyo"
# wrong date
date: 2020-10-06T08:47:36+00:00
lastmod: 2022-07-27T08:47:36+00:00
youtube: T9HBJbyIh3w
# youtube: "rVP73TV2X0s"

banner: "/photos/holdhands.jpg"
buttons:
  - label : "Mag-Register sa Waitlist"
    color: "primary"
    link : "https://hub.pantrypoints.com/signup"
  - label : "Tingnan ang Supereconomics"
    color: "info"
    link : "https://superphysics.org/social/economics"


req:
  title: Para sa mga Mongol na hindi marunong sa pera, ang mga baka ang sukatan ng halaga. Ang kayamanan para sa kanila ay mga baka, samantalang para sa mga Espanyol ito ay ang ginto at pilak. Ang pananaw ng mga Mongol ang mas tama. (Adam Smith)
  # link: "https://superphysics.org/research/socrates/simple-republic/book-3/chapter-3"
  # btext: "View the requirements"
  avatar: "/avatars/smith.png"



### COUNTRIES ###

countries:
  title : "Mga Lokasyon"
  subtitle: Heto ang mga siyudad kung saan prototype ang aming sistema. 
  # subtitle: "We'll deploy an app for your city once the waitlist for that city gets filled."
  flags:
    - name : "Las Pinas"
      flag : "/flags/ph.png"
    - name : "San Pedro Laguna"
      flag : "/flags/ph.png"
    - name : "Kuala Lumpur (coming soon!)"
      flag : "/flags/my.png"
    - name : "Hanoi (coming soon!)"
      flag : "/flags/vn.png"
    - name : "Saigon (coming soon!)"
      flag : "/flags/vn.png"      



### FEATURES ###

features: 
  title: Gumagamit ng Tri-sactions
  # title: "A Complete Economic System, Resilient Against Crises"
  subtitle: "Sa trisactions, pwedeng mag-transact sa pamamagitan ng pera, barter, at cryptocurrencies para mapalaya ang ekonomiya."
  # subtitle: "Implements the time-tested, but forgotten principles of Classical Economics, from Socrates to Adam Smith, instead of Neoclassical Economics from Marshall to Keynes"
  image: "/photos/yay.jpg"
  items:
    - name : "1: Moneyless"
      icon : "<svg viewBox='0 0 576 512' fill='darkcyan' height='50px'><path d='M96,128A64,64,0,1,0,32,64,64,64,0,0,0,96,128Zm0,176.08a44.11,44.11,0,0,1,13.64-32L181.77,204c1.65-1.55,3.77-2.31,5.61-3.57A63.91,63.91,0,0,0,128,160H64A64,64,0,0,0,0,224v96a32,32,0,0,0,32,32V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V383.61l-50.36-47.53A44.08,44.08,0,0,1,96,304.08ZM480,128a64,64,0,1,0-64-64A64,64,0,0,0,480,128Zm32,32H448a63.91,63.91,0,0,0-59.38,40.42c1.84,1.27,4,2,5.62,3.59l72.12,68.06a44.37,44.37,0,0,1,0,64L416,383.62V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V352a32,32,0,0,0,32-32V224A64,64,0,0,0,512,160ZM444.4,295.34l-72.12-68.06A12,12,0,0,0,352,236v36H224V236a12,12,0,0,0-20.28-8.73L131.6,295.34a12.4,12.4,0,0,0,0,17.47l72.12,68.07A12,12,0,0,0,224,372.14V336H352v36.14a12,12,0,0,0,20.28,8.74l72.12-68.07A12.4,12.4,0,0,0,444.4,295.34Z'/></svg>"
      content : "Ang Moneyless o barter transactions ay gumagamit ng stored-value points na nakabase sa bigas"
    - name : "2: Money"
      icon : "<svg fill='darkcyan' height='50px' viewBox='0 0 640 512'><path d='M621.16 54.46C582.37 38.19 543.55 32 504.75 32c-123.17-.01-246.33 62.34-369.5 62.34-30.89 0-61.76-3.92-92.65-13.72-3.47-1.1-6.95-1.62-10.35-1.62C15.04 79 0 92.32 0 110.81v317.26c0 12.63 7.23 24.6 18.84 29.46C57.63 473.81 96.45 480 135.25 480c123.17 0 246.34-62.35 369.51-62.35 30.89 0 61.76 3.92 92.65 13.72 3.47 1.1 6.95 1.62 10.35 1.62 17.21 0 32.25-13.32 32.25-31.81V83.93c-.01-12.64-7.24-24.6-18.85-29.47zM48 132.22c20.12 5.04 41.12 7.57 62.72 8.93C104.84 170.54 79 192.69 48 192.69v-60.47zm0 285v-47.78c34.37 0 62.18 27.27 63.71 61.4-22.53-1.81-43.59-6.31-63.71-13.62zM320 352c-44.19 0-80-42.99-80-96 0-53.02 35.82-96 80-96s80 42.98 80 96c0 53.03-35.83 96-80 96zm272 27.78c-17.52-4.39-35.71-6.85-54.32-8.44 5.87-26.08 27.5-45.88 54.32-49.28v57.72zm0-236.11c-30.89-3.91-54.86-29.7-55.81-61.55 19.54 2.17 38.09 6.23 55.81 12.66v48.89z'/></svg>"
      content: "Pwedeng gumamit ng pera sa pamamagitan ng 'money points', gamit ang cash o cashless"
      # content : "Pantry allows points-transactions for donations, exchange (barter), and investments. This circulates value whatever the economic situation even during stagflation or financial crisis (when banks won't let you withdraw money in order to prevent bank runs), or when your customer delays his money payments."
    - name : "3: Metaverse"
      icon: <svg fill='darkcyan' height='50px' viewBox='0 0 320 512'><path d='M311.9 260.8L160 353.6 8 260.8 160 0l151.9 260.8zM160 383.4L8 290.6 160 512l152-221.4-152 92.8z'/></svg>
      content : "Ang points ay pwedeng ilipat sa 'Pantry Coins' para magamit sa ethereum transactions"
      

how:
  title : "Paano ba Yan Gumagana?"
  steps:
    - content: "I-post ang iyong item gamit ang points price"
      image: "/screens/pantryshow.jpg"
      id: 1
    - content: "Magkita sa mga buyers"
      image: "/photos/shake.jpg"
      id: 2    
    - content: "Magbayad sa points"
      image: "/screens/pay.jpg"
      id: 3
      


triobutton:
  title : "Para sa Komunidad, Negosyo, at Mamamayan "
  subtitle: "Sakop ng Pantrypoints ang buong ekonomiya, kahit na world trade at taxation"
  banner: "/photos/protests.jpg"
  items:
    - image: "/logos/comtri.png"
      text: "Ginagamit ang Trisactions ng Community Pantrypoints at Business Pantrypoints sa pamamagitan ng Pantrypoints City"
      linktext: "Ano ang Trisactions?"
      link: "/trisactions"
    - image: "/logos/pr.png"
      text: " Ang Pantrypreneur ay platform para sa micro and small business para sa trisactions"
      linktext: "Ano ang Pantrypreneur?"
      link: "/pantrypreneur"
    - image: "/logos/services.png"
      text: "Ang Serbisyo namin ay nagtatayo ng Pantrypoints economy"
      linktext: "Ano ang mga serbisyo ninyo?"
      link: "/services"      


duo2:
  title : "Inimbento ng mga Inca"
  subtitle: "Nagtayo ng isang moneyless system ang mga Inca na gumagamit ng hyperlocal warehouse at mga tali para sa record-keeping. Sinira ito ng mga Kastila at pinalitan ng sistemang gamit ang pera"
  banner: "/photos/inca.jpg"
  link: "https://gizmodo.com/the-greatest-mystery-of-the-inca-empire-was-its-strange-5872764"
  linktext: "Paano gumagana ang sistema ng mga Inca?"  
  items:
    - image: "/photos/qullqa.jpg"
      imagetext: "Ang Qulqa system ay pantries namin"
    - image: "/photos/quipu.jpg"
      imagetext: "Ang Quipu strings ay points namin, kaya Pantrypoints"


# numbers:
#   title : "Steady Progress"
#   subtitle: "Unlike the money system that has booms and busts, Pantry has steady growth despite crises, because it goes with Nature and not against it"
#   cards:
#     - value : "15"
#       name : "Partners"
#       footer: "<span class='has-text-dark'>As of October 2022</span>"
#     - value : "< 1,000"
#       name : "Registered users"
#       footer: "<span class='has-text-dark'>As of October 2022</span>"


feedback:
  title: "Feedback"
  items:
    - user : "Prof. Dhanjoo Ghista, University 2020 Foundation President"
      image: "/avatars/dhanjoo.jpg"
      content: "I very much like this work: A Research Proposal For The Formalization Of The Science Of Supereconomics And The Establishment Of A Point-Based Economic System"
      link: "http://www.dhanjooghista.com"
    - user : "Prof. Gavin Kennedy, a world expert on Adam Smith"
      image: "/avatars/gavin.jpg"
      content: "I believe it is developing into a worthwhile project" 
      link: "http://adamsmithslostlegacy.blogspot.com"

##################### Call to action #####################

cta:
  title : "Mag-Register sa Waitlist"
  link1text: "Oo ba!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "Magbabasa muna ako"
  link2 : "https://superphysics.org/social/economics/fallacies/equilibrium-fallacy"

---



<!-- # features: 
#   image: "/photos/yay.jpg"
#   title : "Gawa para sa Krisis"
#   items:
#     - name : "Wala ka bang pera? Ok lang yan!"
#       icon : "<svg  fill='darkcyan' height='50px' viewBox='0 0 640 512'><path d='M621.16 54.46C582.37 38.19 543.55 32 504.75 32c-123.17-.01-246.33 62.34-369.5 62.34-30.89 0-61.76-3.92-92.65-13.72-3.47-1.1-6.95-1.62-10.35-1.62C15.04 79 0 92.32 0 110.81v317.26c0 12.63 7.23 24.6 18.84 29.46C57.63 473.81 96.45 480 135.25 480c123.17 0 246.34-62.35 369.51-62.35 30.89 0 61.76 3.92 92.65 13.72 3.47 1.1 6.95 1.62 10.35 1.62 17.21 0 32.25-13.32 32.25-31.81V83.93c-.01-12.64-7.24-24.6-18.85-29.47zM48 132.22c20.12 5.04 41.12 7.57 62.72 8.93C104.84 170.54 79 192.69 48 192.69v-60.47zm0 285v-47.78c34.37 0 62.18 27.27 63.71 61.4-22.53-1.81-43.59-6.31-63.71-13.62zM320 352c-44.19 0-80-42.99-80-96 0-53.02 35.82-96 80-96s80 42.98 80 96c0 53.03-35.83 96-80 96zm272 27.78c-17.52-4.39-35.71-6.85-54.32-8.44 5.87-26.08 27.5-45.88 54.32-49.28v57.72zm0-236.11c-30.89-3.91-54.86-29.7-55.81-61.55 19.54 2.17 38.09 6.23 55.81 12.66v48.89z'/></svg>"
#       content : "Pwede sa Pantrynomics ang economic social contracts na gamit ang pera, barter, o resource credits. Dapat lang ay may kakayahan ka (labor theory of value)"
      
#     - name : "Handa para sa Krisis"
#       icon : "<svg  viewBox='0 0 320 512' fill='darkcyan' height='50px'><path d='M296 160H180.6l42.6-129.8C227.2 15 215.7 0 200 0H56C44 0 33.8 8.9 32.2 20.8l-32 240C-1.7 275.2 9.5 288 24 288h118.7L96.6 482.5c-3.6 15.2 8 29.5 23.3 29.5 8.4 0 16.4-4.4 20.8-12l176-304c9.3-15.9-2.2-36-20.7-36z'/></svg>"
#       content : "Ang mga social contracts ay maaring for-profit, o non-profit para sa mga oras ng sakuna"
      
#     - name : "Bye bye blind belief!"
#       icon : "<svg  viewBox='0 0 512 512' fill='darkcyan' height='50px'><path d='M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z'/></svg>"
#       content : "Ang Law of Social Cycles mula kay Socrates ang magbibigay alam kung ano ang posibleng mangyari, gamit ang data science"


features:
  image: "/photos/shake.jpg"
  title : "The Points Economy"
  subtitle: "Unlike money which is a single kind of store of value, points take various forms for easier points-taxation, points-accounting and analytics. All of these use the time-factor which is also essential for a high EQ or emotional quotient, as opposed to the money system which requires a high IQ and selfish-interest"
  items:
    - name : Loyalty Points
      icon : "<svg height='100px' fill='gold' viewBox='0 0 512 512'><path d='M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z'/></svg>"
      content : "These are points given by businesses to their customers to incentivize repeat purchases. This is used to 'train' people in the use of points"
      footer: "<span class='has-text-dark'>Target Date: Deployed</span>"          
    - name : Social Points
      icon : "<svg height='100px' fill='hotpink' viewBox='0 0 512 512'><path d='M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z'/></svg>"    
      content : "These are points used for donations and relief. We use this primarily to incentivize [food rescue](https://foodrescueph.site) and plastic waste collection to realize a moneyless circular economy"
      footer: "<span class='has-text-dark'>Target Date: 2023</span>"
    - name : Trade Points
      icon : "<svg height='100px' fill='darkcyan' viewBox='0 0 512 512'><path d='M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z'/></svg>"
      content : "These are points for barter transactions. Unlike social points which do not seek something in return, trade points do. This is the foundation of Points-banking"
      footer: "<span class='has-text-dark'>Target Date: 2023</span>"      
    - name : Investment Points
      icon : "<svg height='100px' fill='dodgerblue' viewBox='0 0 512 512'><path d='M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z'/></svg>"
      content : "These are trade points designed to spur investments when money is lacking. This requires the other point-types to be working beforehand."
      link: pantrylitics
  
  
how:
  title : "Paano ba yan gumagana?"
  steps:
    - image: "/screens/po1.jpg"
      id: 1
      content: "Mag-Post ng item"
    - image: "/screens/po2.jpg"
      id: 2
      content: "Maghanap ng interesado"
    - image: "/screens/po3.jpg"
      id: 3
      content: "Mag-palitan"
      
# duo:
#   title : "Made for the Crisis Years"
#   subtitle: "The false assumptions by the Marginal Revolution and Neoclassical Economics naturally create recurring crises. We started building Pantrynomics in 2015 to solve the long crisis period which we predicted to start in 2019 caused by Keynesian liquidity preference and Marginalist profit maximization"
#   banner: "/photos/protests.jpg"
#   items:
#     - image: "/graphics/crisisyears.png"
#       imagetext: "Predictions from Pantrynomics"
#     - image: "/graphics/paper.jpg"
#       imagetext: "Paper"





circles:
  title : "Mga Platform"
  cards:
    - name : "Health"
      image : /icons/ph.png
      link: "/health"      
      content : "Ang health diagnosis system na nag-rerefer sa medical services at treatments para sa mga taong walang pera. Target: Tentative in Pantry"
      footer: "<span class='has-text-dark'>Target Date: 2023</span>"

    - name : "Learn"
      image : "/icons/pl.png"
      link: "/learn"      
      content : "Ang module-based learning at apprenticeship system na pwede kahit sa pandemiya o kahirapan. Target: 2023 in Pantry"
      footer: "<span class='has-text-dark'>Target Date: 2024</span>"

    - name : "Rewards"
      image : "/icons/pr.png"
      link: "/rewards"      
      content : "Isang rewards platform para sa mga maliliit na negosyo"
      footer: "<span class='has-text-success'>Target Date: Deployed</span>"

    - name : Hub
      image : /icons/pantry.png
      content : "Isang economy-as-a-service na syang economic hub para sa mga komunidad kung saan pwede ang pera at points"
      link: /home
      footer: "<span class='has-text-dark'>Target Date: Rolling Deploy</span>"

    - name : World
      image : /icons/world.png
      content : "Isang sistema ng multilateral pool clearing para sa imports/exports sa pamamagitan ng local currency o points"
      link: /world
      footer: "<span class='has-text-dark'>Target Date: Tentative</span>"      

    - name : Pantrylitics
      image : /icons/pantrylitics.png
      content : "Analytics para sa Pantry Hub at Pantry World para sa real-time policymaking. Inspired ng Bloomberg terminal"
      link: pantrylitics
      footer: "<span class='has-text-dark'>Target Date: 2025</span>"      

circles2:
  title : "Features"
  cards:
    - name : Circle
      image : /icons/pc.png
      content : "Isang sistemang pang-circular economy para sa food security at pagbabawas ng basura"
      link: /circle
      footer: "<span class='has-text-dark'>Platforms: Hub</span>"  

    - name : Points Banking
      image : /icons/banking.png
      content : "Isang sistema ng banking na syang gumagamit ng Points imbis na pera"
      link: banking
      footer: "<span class='has-text-dark'>Platforms: Hub</span>"

    # - name : "Beacon"
    #   image : /icons/pb.png
    #   link: "beacon"
    #   content : "A local news system that funds local journalists and writers so they don't have to sell their integrity"
    #   footer: "<span class='has-text-dark'>Platforms: Hub</span>"

    - name : "Match"
      image : "/icons/match.png"
      link: "/match"  
      content : "Isang matching system para tumaas ang success rate ng points-transactions"
      footer: "<span class='has-text-dark'>Platforms: Health, Learn</span>"

    - name : "Govern"
      image : /icons/pg.png
      link: "/govern"      
      content : "Sistema na pang-gobyerno para sa government services at buwis na base sa points"
      footer: "<span class='has-text-dark'>Target Date: Tentative</span>"

    - name : Pointtax
      image : /icons/pt.png
      link: "/pointtax"
      content : "Ang points-based taxation system na pumipigil sa tax evasion at tinataas ang tax revenue sa pamamagitan ng pag-spread sa mas maraming tao"
      footer: "<span class='has-text-dark'>Platforms: Hub, Govern</span>"


    #   content : "Ang ISAIAH Predict ay nagbibigay ng insights sa mga potensyal na kaganapan, base sa nakaraang kaganapan"

duo2:
  title : "Dating inimplementa na mga Inca"
  subtitle: "Ginawa ng Inca ang isang sistemang pang-ekonomiya na walang pera. Sinira ito ng ma Kastila at pinalitan ng sistemang kailangan ang pera."
  banner: "/photos/inca.jpg"
  link: "https://gizmodo.com/the-greatest-mystery-of-the-inca-empire-was-its-strange-5872764"
  linktext: "Basahin kung paano gumana ang sistema ng Inca"  
  items:
    - image: "/photos/qullqa.jpg"
      imagetext: "Pantry ang version namin sa Qulqa"
    - image: "/photos/quipu.jpg"
      imagetext: "Points ang version namin sa Quipu"

 -->
 