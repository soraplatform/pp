---
title: "Die widerstandsfähige, Intelligente Stadt"
subtitle: "Pantrypoints City ist die Plattform für hyperlokale 'Traktionen' in der Stadt" 
logo: "/logos/city.png"
image: "/og/city.jpg"
description: "Pantrypoints City ist die Plattform für hyperlokale 'Traktionen' in der Stadt"
youtube: GVkVoJzLn2g
applink: https://hub.pantrypoints.com/signup
apptext: "Register in the Waitlist"


features:
  image: "/photos/congress.jpg"
  title : "Die widerstandsfähige Stadt"
  subtitle: "Führen Sie Tausch-Kredit-Transaktionen (Traktionen) durch, um Geld zu sparen, gegen Inflation anzukämpfen und gleichzeitig die Lebensmittelsicherheit und soziale Sicherheit zu fördern"
  items:      
    - name : "Punktebanking"
      icon : "<svg  viewBox='0 0 576 512' fill='dimgray' height='50px'><path d='M271.06,144.3l54.27,14.3a8.59,8.59,0,0,1,6.63,8.1c0,4.6-4.09,8.4-9.12,8.4h-35.6a30,30,0,0,1-11.19-2.2c-5.24-2.2-11.28-1.7-15.3,2l-19,17.5a11.68,11.68,0,0,0-2.25,2.66,11.42,11.42,0,0,0,3.88,15.74,83.77,83.77,0,0,0,34.51,11.5V240c0,8.8,7.83,16,17.37,16h17.37c9.55,0,17.38-7.2,17.38-16V222.4c32.93-3.6,57.84-31,53.5-63-3.15-23-22.46-41.3-46.56-47.7L282.68,97.4a8.59,8.59,0,0,1-6.63-8.1c0-4.6,4.09-8.4,9.12-8.4h35.6A30,30,0,0,1,332,83.1c5.23,2.2,11.28,1.7,15.3-2l19-17.5A11.31,11.31,0,0,0,368.47,61a11.43,11.43,0,0,0-3.84-15.78,83.82,83.82,0,0,0-34.52-11.5V16c0-8.8-7.82-16-17.37-16H295.37C285.82,0,278,7.2,278,16V33.6c-32.89,3.6-57.85,31-53.51,63C227.63,119.6,247,137.9,271.06,144.3ZM565.27,328.1c-11.8-10.7-30.2-10-42.6,0L430.27,402a63.64,63.64,0,0,1-40,14H272a16,16,0,0,1,0-32h78.29c15.9,0,30.71-10.9,33.25-26.6a31.2,31.2,0,0,0,.46-5.46A32,32,0,0,0,352,320H192a117.66,117.66,0,0,0-74.1,26.29L71.4,384H16A16,16,0,0,0,0,400v96a16,16,0,0,0,16,16H372.77a64,64,0,0,0,40-14L564,377a32,32,0,0,0,1.28-48.9Z'/></svg>"
      content : "Verwenden Sie Ihre Punkte bei Dritten"

    - name : "Lokale Transportrouten"
      icon : "<svg  viewBox='0 0 512 512' fill='dimgray' height='50px'><path d='M488 128h-8V80c0-44.8-99.2-80-224-80S32 35.2 32 80v48h-8c-13.25 0-24 10.74-24 24v80c0 13.25 10.75 24 24 24h8v160c0 17.67 14.33 32 32 32v32c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32v-32h192v32c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32v-32h6.4c16 0 25.6-12.8 25.6-25.6V256h8c13.25 0 24-10.75 24-24v-80c0-13.26-10.75-24-24-24zM160 72c0-4.42 3.58-8 8-8h176c4.42 0 8 3.58 8 8v16c0 4.42-3.58 8-8 8H168c-4.42 0-8-3.58-8-8V72zm-48 328c-17.67 0-32-14.33-32-32s14.33-32 32-32 32 14.33 32 32-14.33 32-32 32zm128-112H128c-17.67 0-32-14.33-32-32v-96c0-17.67 14.33-32 32-32h112v160zm32 0V128h112c17.67 0 32 14.33 32 32v96c0 17.67-14.33 32-32 32H272zm128 112c-17.67 0-32-14.33-32-32s14.33-32 32-32 32 14.33 32 32-14.33 32-32 32z'/></svg>"
      content : "Sehen Sie sich öffentliche Verkehrsrouten an, wie eine Buskarte"

    - name : "Versorgungsunternehmen über Energiepunkte"
      icon : "<svg  viewBox='0 0 320 512' fill='dimgray' height='50px'><path d='M296 160H180.6l42.6-129.8C227.2 15 215.7 0 200 0H56C44 0 33.8 8.9 32.2 20.8l-32 240C-1.7 275.2 9.5 288 24 288h118.7L96.6 482.5c-3.6 15.2 8 29.5 23.3 29.5 8.4 0 16.4-4.4 20.8-12l176-304c9.3-15.9-2.2-36-20.7-36z'/></svg>"
      content : "Vergleichen und kontrastieren Sie Versorgungsunternehmen, um das beste Angebot zu erhalten"      

    - name : "Jobs"
      icon : "<svg  viewBox='0 0 448 512' fill='gray' height='50px'><path d='M325.4 289.2L224 390.6 122.6 289.2C54 295.3 0 352.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-70.2-54-127.1-122.6-133.2zM32 192c27.3 0 51.8-11.5 69.2-29.7 15.1 53.9 64 93.7 122.8 93.7 70.7 0 128-57.3 128-128S294.7 0 224 0c-50.4 0-93.6 29.4-114.5 71.8C92.1 47.8 64 32 32 32c0 33.4 17.1 62.8 43.1 80-26 17.2-43.1 46.6-43.1 80zm144-96h96c17.7 0 32 14.3 32 32H144c0-17.7 14.3-32 32-32z'/></svg>"
      content : "Finden oder veröffentlichen Sie Jobs, die in Punkten bezahlt werden"

    - name : "Marktplätze"
      icon : "<svg  viewBox='0 0 640 512' fill='gray' height='50px'><path d='M320 384H128V224H64v256c0 17.7 14.3 32 32 32h256c17.7 0 32-14.3 32-32V224h-64v160zm314.6-241.8l-85.3-128c-6-8.9-16-14.2-26.7-14.2H117.4c-10.7 0-20.7 5.3-26.6 14.2l-85.3 128c-14.2 21.3 1 49.8 26.6 49.8H608c25.5 0 40.7-28.5 26.6-49.8zM512 496c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V224h-64v272z'/></svg>"
      content : "Suchen Sie nach Verkäufern, Trainern und Raumvermietungen, die Punkte akzeptieren"
          
    - name : "Veranstaltungen"
      icon : "<svg  viewBox='0 0 640 512' fill='gray' height='50px'><path d='M206.86 245.15c-35.88 10.45-59.95 41.2-57.53 74.1 11.4-12.72 28.81-23.7 49.9-30.92l7.63-43.18zM95.81 295L64.08 115.49c-.29-1.62.28-2.62.24-2.65 57.76-32.06 123.12-49.01 189.01-49.01 1.61 0 3.23.17 4.85.19 13.95-13.47 31.73-22.83 51.59-26 18.89-3.02 38.05-4.55 57.18-5.32-9.99-13.95-24.48-24.23-41.77-27C301.27 1.89 277.24 0 253.32 0 176.66 0 101.02 19.42 33.2 57.06 9.03 70.48-3.92 98.48 1.05 126.58l31.73 179.51c14.23 80.52 136.33 142.08 204.45 142.08 3.59 0 6.75-.46 10.01-.8-13.52-17.08-28.94-40.48-39.5-67.58-47.61-12.98-106.06-51.62-111.93-84.79zm97.55-137.46c-.73-4.12-2.23-7.87-4.07-11.4-8.25 8.91-20.67 15.75-35.32 18.32-14.65 2.58-28.67.4-39.48-5.17-.52 3.94-.64 7.98.09 12.1 3.84 21.7 24.58 36.19 46.34 32.37 21.75-3.82 36.28-24.52 32.44-46.22zM606.8 120.9c-88.98-49.38-191.43-67.41-291.98-51.35-27.31 4.36-49.08 26.26-54.04 54.36l-31.73 179.51c-15.39 87.05 95.28 196.27 158.31 207.35 63.03 11.09 204.47-53.79 219.86-140.84l31.73-179.51c4.97-28.11-7.98-56.11-32.15-69.52zm-273.24 96.8c3.84-21.7 24.58-36.19 46.34-32.36 21.76 3.83 36.28 24.52 32.45 46.22-.73 4.12-2.23 7.87-4.07 11.4-8.25-8.91-20.67-15.75-35.32-18.32-14.65-2.58-28.67-.4-39.48 5.17-.53-3.95-.65-7.99.08-12.11zm70.47 198.76c-55.68-9.79-93.52-59.27-89.04-112.9 20.6 25.54 56.21 46.17 99.49 53.78 43.28 7.61 83.82.37 111.93-16.6-14.18 51.94-66.71 85.51-122.38 75.72zm130.3-151.34c-8.25-8.91-20.68-15.75-35.33-18.32-14.65-2.58-28.67-.4-39.48 5.17-.52-3.94-.64-7.98.09-12.1 3.84-21.7 24.58-36.19 46.34-32.37 21.75 3.83 36.28 24.52 32.45 46.22-.73 4.13-2.23 7.88-4.07 11.4z'/></svg>"
      content : "Finden Sie Veranstaltungen in Ihrer Stadt."
      

how:
  title : "So funktioniert es"
  steps:
    - image: "/screens/register.png"
      id: 1
      content: "Registrieren Sie sich in der Warteliste-App und geben Sie Ihre Stadt an"
    - image: "/screens/po2.jpg"
      id: 2
      content: "Wir informieren Sie, wenn die App für Ihre Stadt bereit ist"
    - image: "/screens/po3.jpg"
      id: 3
      content: "Verwenden Sie die App für Punkte-Transaktionen"


req:
  title: Basierend auf den Anforderungen in Buch 5, Kapitel 2 der Republik
  # link: "https://superphysics.org/research/socrates/simple-republic/book-5/chapter-2"
  # btext: "Read the requirements"
  avatar: "/avatars/socrates.png"  


feedback:
  title: "Wettbewerbe"
  items:
    - user : "Urban Land Institute Philippines"
      image: "/icons/uli200.jpg"
      content: "Top 5 ULI Urban Innovation Challenge 2019"
      link: "http://philippines.uli.org"
    - user : "UBX"
      image: "/icons/uhack200.jpg"
      content: "Top 10 U:Hack 2019" 
      link: "http://ubx.ph"


feedback2:
  title: "Feedback"
  items:
    - user : "brandvertisor.com"
      image: "/icons/brandvertisor.jpg"
      content: "You might be interested to make a city-centric problems & solutions voting.. transparency in citizens votes per problem can help a lot in lost time/money"
      link: "https://brandvertisor.com"
    - user : "itini.me"
      image: "/icons/itini.jpg"
      content: "I'd like the following services: apartment rent pricing in different regions, events where cool people gather, local transportation, fresh food and supermarkets!" 
      link: "https://itini.me"
    - user : "writeemote.com"
      image: "/icons/emote.jpg"
      content: "Starting with helping owners file their property taxes more efficiently would be HUGE. Helping people find parking would be HUGE." 
      link: "https://writeemote.com"


########## Call to action ########
cta:
  enable : true
  title : "Do you want to try City?"
  link1text: "Yes, I'm in!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "I need to know more"
  link2 : "https://superphysics.org/research/socrates/simple-republic/book-5/chapter-2"

---
