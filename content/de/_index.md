---
title: Resistent gegen Bankenkrisen
# title : "لنبني عالمًا أفضل بعد إعادة الضبط العظمى"
banner: "/photos/holdhands.jpg"
lead : "Pantrypoints ist ein widerstandsfähiges, neues Wirtschaftssystem, das bilateral Punkte durch Treffen verwendet, um Inflation zu stoppen und Vollbeschäftigung unabhängig von den wirtschaftlichen Bedingungen zu ermöglichen"
description : "Pantrypoints ist ein widerstandsfähiges, neues Wirtschaftssystem, das bilateral Punkte durch Treffen verwendet, um Inflation zu stoppen und Vollbeschäftigung unabhängig von den wirtschaftlichen Bedingungen zu ermöglichen"
youtube: "k4OdiIY_v7M"

buttons:
  - label : "Registriere dich auf der Warteliste"
    color: "primary"
    link : "https://hub.pantrypoints.com/signup"
  - label : "Lies die Theorie."
    color: "info"
    link : "https://superphysics.org/social/economics/"



req:
  title: Für die Mongolen, die keine Ahnung von Geld hatten, war eine Kuh das Maß für Wert. Reichtum wurde von ihnen in der Anzahl der Kühe gemessen, genauso wie für die Spanier Reichtum in der Menge von Gold und Silber gemessen wurde. Die Vorstellung der Mongolen ist korrekter. (Adam Smith)
  # link: "https://superphysics.org/research/socrates/simple-republic/book-3/chapter-3"
  # btext: "View the requirements"
  avatar: "/avatars/smith.png"



# Regístrate en la aplicación. Lee la Ciencia de nuestro sistema.

##################### Feature ##########################

features:
  image: "/photos/yay.jpg"
  title : Vorstellung von Tri-saktionen
  subtitle: Tri-saktionen ermöglichen Geld-, Geldlos- oder Metaverse-Transaktionen, um die Wirtschaft unter allen sozialen Bedingungen laufen zu lassen und echte wirtschaftliche Freiheit zu ermöglichen.
  items:
    - name : "Geldlos"
      icon : "<svg viewBox='0 0 576 512' fill='darkcyan' height='50px'><path d='M96,128A64,64,0,1,0,32,64,64,64,0,0,0,96,128Zm0,176.08a44.11,44.11,0,0,1,13.64-32L181.77,204c1.65-1.55,3.77-2.31,5.61-3.57A63.91,63.91,0,0,0,128,160H64A64,64,0,0,0,0,224v96a32,32,0,0,0,32,32V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V383.61l-50.36-47.53A44.08,44.08,0,0,1,96,304.08ZM480,128a64,64,0,1,0-64-64A64,64,0,0,0,480,128Zm32,32H448a63.91,63.91,0,0,0-59.38,40.42c1.84,1.27,4,2,5.62,3.59l72.12,68.06a44.37,44.37,0,0,1,0,64L416,383.62V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V352a32,32,0,0,0,32-32V224A64,64,0,0,0,512,160ZM444.4,295.34l-72.12-68.06A12,12,0,0,0,352,236v36H224V236a12,12,0,0,0-20.28-8.73L131.6,295.34a12.4,12.4,0,0,0,0,17.47l72.12,68.07A12,12,0,0,0,224,372.14V336H352v36.14a12,12,0,0,0,20.28,8.74l72.12-68.07A12.4,12.4,0,0,0,444.4,295.34Z'/></svg>"
      content : "Geldlose Transaktionen verwenden gespeicherten Werttausch, der in Punkten gemessen wird und an Getreide gebunden ist. Dies implementiert die getreidebasierte Bewertung, die in 'Der Wohlstand der Nationen' von Adam Smith erwähnt wird"
      
    - name : "Geld"
      icon : "<svg fill='darkcyan' height='50px' viewBox='0 0 640 512'><path d='M621.16 54.46C582.37 38.19 543.55 32 504.75 32c-123.17-.01-246.33 62.34-369.5 62.34-30.89 0-61.76-3.92-92.65-13.72-3.47-1.1-6.95-1.62-10.35-1.62C15.04 79 0 92.32 0 110.81v317.26c0 12.63 7.23 24.6 18.84 29.46C57.63 473.81 96.45 480 135.25 480c123.17 0 246.34-62.35 369.51-62.35 30.89 0 61.76 3.92 92.65 13.72 3.47 1.1 6.95 1.62 10.35 1.62 17.21 0 32.25-13.32 32.25-31.81V83.93c-.01-12.64-7.24-24.6-18.85-29.47zM48 132.22c20.12 5.04 41.12 7.57 62.72 8.93C104.84 170.54 79 192.69 48 192.69v-60.47zm0 285v-47.78c34.37 0 62.18 27.27 63.71 61.4-22.53-1.81-43.59-6.31-63.71-13.62zM320 352c-44.19 0-80-42.99-80-96 0-53.02 35.82-96 80-96s80 42.98 80 96c0 53.03-35.83 96-80 96zm272 27.78c-17.52-4.39-35.71-6.85-54.32-8.44 5.87-26.08 27.5-45.88 54.32-49.28v57.72zm0-236.11c-30.89-3.91-54.86-29.7-55.81-61.55 19.54 2.17 38.09 6.23 55.81 12.66v48.89z'/></svg>"
      content : "'Geldpunkte' ermöglichen die Bezahlung von geldlosen Transaktionen über Bargeld oder bargeldlose Banking-Apps der Fiat-Wirtschaft"
      
    - name : "Metaverse"
      icon: <svg fill='darkcyan' height='50px' viewBox='0 0 320 512'><path d='M311.9 260.8L160 353.6 8 260.8 160 0l151.9 260.8zM160 383.4L8 290.6 160 512l152-221.4-152 92.8z'/></svg>    
      content : "Die Punkte können in 'Vorratsmünzen' umgewandelt werden, um regulierte Transaktionen vom Metaverse über Ethereum zu ermöglichen. Dies ist nützlich für grenzüberschreitende Transaktionen und unser vorgeschlagenes 'Krypto-Easing' (unsere Alternative zur quantitativen Lockerung)."


######################### How #####################
how:
  title : "Wie es funktioniert"
  steps:
    - image: "/screens/po1.jpg"
      id: 1
      content: "Poste dein Produkt mit einem Punktepreis anstelle eines Geldpreises"
    - image: "/screens/po2.jpg"
      id: 2
      content: "Treffe dich mit den Käufern deines Produkts"
    - image: "/screens/po3.jpg"
      id: 3
      content: "Bezahle mit Punkten"


##################### Call to action #####################

cta:
  title : "Registriere dich auf der Warteliste."
  link1text: "Ja!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "Nicht jetzt"
  link2 : "https://superphysics.org/"

---
