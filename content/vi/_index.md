---
# title : "Hệ thống Thương mại Thế giới bền vững"
# title: Chống chịu với các cuộc khủng hoảng ngân hàng. 
title: Nền kinh tế Vòng Tròn Giải Quyết Lạm Phát
lead: Pantrypoints là hệ thống kinh tế mới mạnh mẽ sử dụng điểm song phương thông qua các cuộc gặp gỡ để ngăn chặn lạm phát, tạo việc làm đầy đủ và thực hiện nền kinh tế vòng tròn.
description: Pantrypoints là hệ thống kinh tế mới mạnh mẽ sử dụng điểm song phương thông qua các cuộc gặp gỡ để ngăn chặn lạm phát, tạo việc làm đầy đủ và thực hiện nền kinh tế vòng tròn.
# description: "Pantrypoints (trước đây là SORA Saigon) đang xây dựng một hệ thống thương mại tự do bền vững dựa trên tác phẩm 'The Wealth of Nations' của Adam Smith"
# description: "Pantry là một mạng xã hội để tận dụng các sản phẩm, hàng hóa, công việc, sự kiện, bất động sản và năng lượng"
# lead: Pantrypoints (trước đây là SORA Saigon) đang xây dựng một hệ thống thương mại tự do bền vững dựa trên tác phẩm 'The Wealth of Nations' của Adam Smith
# lead: "Pantry là một mạng xã hội để tận dụng các sản phẩm, hàng hóa, công việc, sự kiện, bất động sản và năng lượng"
# wrong date
date: 2020-10-06T08:47:36+00:00
lastmod: 2022-07-27T08:47:36+00:00
# youtube: "rVP73TV2X0s"
youtube: "X_f5zAr-6rc"
banner: "/photos/holdhands.jpg"
buttons:
  - label: "Đăng ký vào danh sách chờ" # subscribe
  # - label : "Đăng xuất"
    color: "primary"
    link : "https://hub.pantrypoints.com/signup"
  - label : "Đọc về lý thuyết"
    color: "info"
    link : "https://superphysics.org/social/economics"


req:
  title: Đối với những người Mông Cổ không biết về tiền bạc, một con bò là đơn vị đo lường giá trị. Đối với họ, sự giàu có được đo bằng số lượng con bò, cũng như đối với người Tây Ban Nha sự giàu có được đo bằng số lượng vàng và bạc. Quan niệm của người Mông Cổ là chính xác hơn. (Adam Smith)
  # link: "https://superphysics.org/research/socrates/simple-republic/book-3/chapter-3"
  # btext: "View the requirements"
  avatar: "/avatars/smith.png"


# Cách hoạt động. 1- Đăng bài về sản phẩm của bạn sử dụng giá điểm thay vì giá tiền. 2- Hẹn gặp những người mua hàng của bạn. 3- 

### FEATURES ###

features: 
  title: Pantrypoints tạo ra "Trisactions".
  # title: "A Complete Economic System, Resilient Against Crises"
  subtitle: "'Trisactions' cho phép giao dịch bằng tiền, không sử dụng tiền, hoặc trong không gian ảo để cho phép nền kinh tế hoạt động dưới mọi điều kiện xã hội, mang lại sự tự do kinh tế thực sự."
  # subtitle: "Implements the time-tested, but forgotten principles of Classical Economics, from Socrates to Adam Smith, instead of Neoclassical Economics from Marshall to Keynes"
  image: "/photos/yay.jpg"
  items:
    - name : "1: Moneyless"
      icon : "<svg viewBox='0 0 576 512' fill='darkcyan' height='50px'><path d='M96,128A64,64,0,1,0,32,64,64,64,0,0,0,96,128Zm0,176.08a44.11,44.11,0,0,1,13.64-32L181.77,204c1.65-1.55,3.77-2.31,5.61-3.57A63.91,63.91,0,0,0,128,160H64A64,64,0,0,0,0,224v96a32,32,0,0,0,32,32V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V383.61l-50.36-47.53A44.08,44.08,0,0,1,96,304.08ZM480,128a64,64,0,1,0-64-64A64,64,0,0,0,480,128Zm32,32H448a63.91,63.91,0,0,0-59.38,40.42c1.84,1.27,4,2,5.62,3.59l72.12,68.06a44.37,44.37,0,0,1,0,64L416,383.62V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V352a32,32,0,0,0,32-32V224A64,64,0,0,0,512,160ZM444.4,295.34l-72.12-68.06A12,12,0,0,0,352,236v36H224V236a12,12,0,0,0-20.28-8.73L131.6,295.34a12.4,12.4,0,0,0,0,17.47l72.12,68.07A12,12,0,0,0,224,372.14V336H352v36.14a12,12,0,0,0,20.28,8.74l72.12-68.07A12.4,12.4,0,0,0,444.4,295.34Z'/></svg>"
      content : "Giao dịch không sử dụng tiền sử dụng trao đổi hàng hóa có giá trị lưu trữ được đo bằng điểm, mà được gắn kết với ngũ cốc. Điều này thực hiện việc định giá dựa trên ngũ cốc được đề cập trong tác phẩm The Wealth of Nations của Adam Smith."
    - name : "2: Money"
      icon : "<svg fill='darkcyan' height='50px' viewBox='0 0 640 512'><path d='M621.16 54.46C582.37 38.19 543.55 32 504.75 32c-123.17-.01-246.33 62.34-369.5 62.34-30.89 0-61.76-3.92-92.65-13.72-3.47-1.1-6.95-1.62-10.35-1.62C15.04 79 0 92.32 0 110.81v317.26c0 12.63 7.23 24.6 18.84 29.46C57.63 473.81 96.45 480 135.25 480c123.17 0 246.34-62.35 369.51-62.35 30.89 0 61.76 3.92 92.65 13.72 3.47 1.1 6.95 1.62 10.35 1.62 17.21 0 32.25-13.32 32.25-31.81V83.93c-.01-12.64-7.24-24.6-18.85-29.47zM48 132.22c20.12 5.04 41.12 7.57 62.72 8.93C104.84 170.54 79 192.69 48 192.69v-60.47zm0 285v-47.78c34.37 0 62.18 27.27 63.71 61.4-22.53-1.81-43.59-6.31-63.71-13.62zM320 352c-44.19 0-80-42.99-80-96 0-53.02 35.82-96 80-96s80 42.98 80 96c0 53.03-35.83 96-80 96zm272 27.78c-17.52-4.39-35.71-6.85-54.32-8.44 5.87-26.08 27.5-45.88 54.32-49.28v57.72zm0-236.11c-30.89-3.91-54.86-29.7-55.81-61.55 19.54 2.17 38.09 6.23 55.81 12.66v48.89z'/></svg>"
      content: "'Money points' cho phép sử dụng tiền để thanh toán cho các giao dịch không sử dụng tiền thông qua tiền mặt hoặc ứng dụng ngân hàng không dùng tiền mặt của nền kinh tế chính pháp."
      # content : "Pantry allows points-transactions for donations, exchange (barter), and investments. This circulates value whatever the economic situation even during stagflation or financial crisis (when banks won't let you withdraw money in order to prevent bank runs), or when your customer delays his money payments."
      
    - name : "3: Metaverse"
      icon: <svg fill='darkcyan' height='50px' viewBox='0 0 320 512'><path d='M311.9 260.8L160 353.6 8 260.8 160 0l151.9 260.8zM160 383.4L8 290.6 160 512l152-221.4-152 92.8z'/></svg>
      # icon : "<svg  viewBox='0 0 320 512' fill='darkcyan' class='text-center' height='50px'><path d='M296 160H180.6l42.6-129.8C227.2 15 215.7 0 200 0H56C44 0 33.8 8.9 32.2 20.8l-32 240C-1.7 275.2 9.5 288 24 288h118.7L96.6 482.5c-3.6 15.2 8 29.5 23.3 29.5 8.4 0 16.4-4.4 20.8-12l176-304c9.3-15.9-2.2-36-20.7-36z'/></svg>"
      content : "Các điểm có thể được chuyển đổi thành Pantry Coins để cho phép các giao dịch được quy định từ Metaverse thông qua Ethereum. Điều này hữu ích cho các giao dịch xuyên biên giới và đề xuất của chúng tôi về 'crypto-easing' (phương án thay thế cho chính sách nới lỏng định lượng)."


# duo:
#   title : "Được tạo ra cho những 'năm khủng hoảng'"
#   subtitle: "Chúng tôi đã có thể dự đoán khủng hoảng hàng năm, nhưng chúng tôi đã nghỉ khoảng 1 năm. Vì vậy, sẽ có sự không chắc chắn, nhưng không nhiều như trước."
#   banner: "/photos/protests.jpg"
#   items:
#     - image: "/graphics/crisisyears.png"
#       imagetext: "Predictions from Supereconomics"
#     - image: "/graphics/paper.jpg"
#       imagetext: "Paper"

how:
  title : "Cách hoạt động"
  steps:
    - content: "Đăng bài về sản phẩm của bạn với giá bằng điểm thay vì giá bằng tiền"
      image: "/screens/pantryshow.jpg"
      id: 1
    - content: "Gặp gỡ với những người mua hàng của bạn"
      image: "/photos/shake.jpg"
      id: 2    
    # - content: "Chống chịu với các cuộc khủng hoảng ngân hàng. Pantrypoints là một hệ thống kinh tế mới, chống chịu và sử dụng các điểm song phương thông qua việc gặp gỡ để ngăn chặn lạm phát và cho phép việc làm đầy đủ dù trong bất kỳ điều kiện kinh tế nào"
    - content: Thanh toán bằng điểm.
      image: "/screens/pay.jpg"
      id: 3
      

feedback:
  title: "Nhận xét"
  items:
    - user : "Prof. Dhanjoo Ghista, University 2020 Foundation President"
      image: "/avatars/dhanjoo.jpg"
      content: "I very much like this work:A Research Proposal For The Formalization Of The Science Of Supereconomics And The Establishment Of A Point-Based Economic System"
      link: "http://www.dhanjooghista.com"
    - user : "Prof. Gavin Kennedy, a world expert on Adam Smith"
      image: "/avatars/gavin.jpg"
      content: "I believe it is developing into a worthwhile project" 
      link: "http://adamsmithslostlegacy.blogspot.com"


##################### Call to action #####################

cta:
  title : "Đăng ký vào danh sách chờ"
  link1text: "Đúng!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "Chưa"
  link2 : "https://superphysics.org/social/economics/fallacies/equilibrium-fallacy"

---

<!--       content : "Pantrynomics có hệ thống ngân hàng đề xuất của riêng mình dựa trên các khoản tín dụng tài nguyên. Hãy coi chúng như những khế ước xã hội trong tương lai không thể chuyển nhượng và bất di bất dịch"      
    #   content : "ISAIAH Predict có tinh thần khởi nghiệp"
       -->
