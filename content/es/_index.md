---
title: La Economía Circular que Resuelve la Inflación
banner: "/photos/holdhands.jpg"
lead: Pantrypoints es un nuevo sistema económico resiliente que utiliza puntos bilaterales a través de reuniones para detener la inflación, permitir el pleno empleo y lograr la economía circular.
description: Pantrypoints es un nuevo sistema económico resiliente que utiliza puntos bilaterales a través de reuniones para detener la inflación, permitir el pleno empleo y lograr la economía circular.
# lead : "Pantrypoints es un sistema económico resiliente y nuevo que utiliza puntos bilaterales a través de encuentros para detener la inflación y permitir el pleno empleo sin importar las condiciones económicas"
# description : "Pantrypoints es un sistema económico resiliente y nuevo que utiliza puntos bilaterales a través de encuentros para detener la inflación y permitir el pleno empleo sin importar las condiciones económicas"
# youtube: "2dR8RDRRrA4"
youtube: "q04lTIW48Oo"
buttons:
  - label : "Regístrese en la lista de espera"
    color: "primary"
    link : "https://hub.pantrypoints.com/signup"
  - label : "Lea acerca de la teoría"
    color: "info"
    link : "https://superphysics.org/social/economics/"

# Regístrate en la aplicación. Lee la Ciencia de nuestro sistema.


req:
  title: Para los mongoles que desconocían el uso del dinero, una vaca era la medida de valor. La riqueza para ellos se medía en la cantidad de vacas, al igual que para los españoles la riqueza se medía en la cantidad de oro y plata. La noción mongol es más correcta. (Adam Smith)
  # link: "https://superphysics.org/research/socrates/simple-republic/book-3/chapter-3"
  # btext: "View the requirements"
  avatar: "/avatars/smith.png"


##################### Feature ##########################

features:
  image: "/photos/yay.jpg"
  title : Presentando Tri-sacciones
  subtitle: Tri-sacciones permiten transacciones con dinero, sin dinero o en el metaverso para permitir que la economía funcione bajo cualquier condición social, permitiendo una verdadera libertad económica.
  items:
    - name : "Sin dinero"
      icon : "<svg viewBox='0 0 576 512' fill='darkcyan' height='50px'><path d='M96,128A64,64,0,1,0,32,64,64,64,0,0,0,96,128Zm0,176.08a44.11,44.11,0,0,1,13.64-32L181.77,204c1.65-1.55,3.77-2.31,5.61-3.57A63.91,63.91,0,0,0,128,160H64A64,64,0,0,0,0,224v96a32,32,0,0,0,32,32V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V383.61l-50.36-47.53A44.08,44.08,0,0,1,96,304.08ZM480,128a64,64,0,1,0-64-64A64,64,0,0,0,480,128Zm32,32H448a63.91,63.91,0,0,0-59.38,40.42c1.84,1.27,4,2,5.62,3.59l72.12,68.06a44.37,44.37,0,0,1,0,64L416,383.62V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V352a32,32,0,0,0,32-32V224A64,64,0,0,0,512,160ZM444.4,295.34l-72.12-68.06A12,12,0,0,0,352,236v36H224V236a12,12,0,0,0-20.28-8.73L131.6,295.34a12.4,12.4,0,0,0,0,17.47l72.12,68.07A12,12,0,0,0,224,372.14V336H352v36.14a12,12,0,0,0,20.28,8.74l72.12-68.07A12.4,12.4,0,0,0,444.4,295.34Z'/></svg>"
      content : "Las transacciones sin dinero utilizan trueque de valor almacenado medido en puntos que están ligados a granos. Esto implementa la valoración basada en granos mencionada en La Riqueza de las Naciones de Adam Smith."
      
    - name : "Dinero"
      icon : "<svg fill='darkcyan' height='50px' viewBox='0 0 640 512'><path d='M621.16 54.46C582.37 38.19 543.55 32 504.75 32c-123.17-.01-246.33 62.34-369.5 62.34-30.89 0-61.76-3.92-92.65-13.72-3.47-1.1-6.95-1.62-10.35-1.62C15.04 79 0 92.32 0 110.81v317.26c0 12.63 7.23 24.6 18.84 29.46C57.63 473.81 96.45 480 135.25 480c123.17 0 246.34-62.35 369.51-62.35 30.89 0 61.76 3.92 92.65 13.72 3.47 1.1 6.95 1.62 10.35 1.62 17.21 0 32.25-13.32 32.25-31.81V83.93c-.01-12.64-7.24-24.6-18.85-29.47zM48 132.22c20.12 5.04 41.12 7.57 62.72 8.93C104.84 170.54 79 192.69 48 192.69v-60.47zm0 285v-47.78c34.37 0 62.18 27.27 63.71 61.4-22.53-1.81-43.59-6.31-63.71-13.62zM320 352c-44.19 0-80-42.99-80-96 0-53.02 35.82-96 80-96s80 42.98 80 96c0 53.03-35.83 96-80 96zm272 27.78c-17.52-4.39-35.71-6.85-54.32-8.44 5.87-26.08 27.5-45.88 54.32-49.28v57.72zm0-236.11c-30.89-3.91-54.86-29.7-55.81-61.55 19.54 2.17 38.09 6.23 55.81 12.66v48.89z'/></svg>"
      content : "Los puntos monetarios permiten utilizar dinero para pagar las transacciones sin dinero a través de efectivo o aplicaciones bancarias sin efectivo de la economía fiduciaria."
      
    - name : "Metaverso"
      icon: <svg fill='darkcyan' height='50px' viewBox='0 0 320 512'><path d='M311.9 260.8L160 353.6 8 260.8 160 0l151.9 260.8zM160 383.4L8 290.6 160 512l152-221.4-152 92.8z'/></svg>    
      content : "Los puntos pueden convertirse en Monedas de Despensa para permitir transacciones reguladas desde el metaverso a través de Ethereum. Esto es útil para transacciones internacionales y nuestra propuesta de cripto-alivio (nuestra alternativa a la flexibilización cuantitativa)."
    

######################### How #####################
how:
  title : "Cómo funciona"
  steps:
    - image: "/screens/po1.jpg"
      id: 1
      content: "Publica tu artículo usando un precio en puntos en lugar de un precio en dinero"
    - image: "/screens/po2.jpg"
      id: 2
      content: "Reúnete con los compradores de tu artículo"
    - image: "/screens/po3.jpg"
      id: 3
      content: "Paga con puntos"


##################### Call to action #####################

cta:
  title : "Regístrese en la lista de espera"
  link1text: "¡Sí!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "Aún no."
  link2 : "https://superphysics.org/"
---
