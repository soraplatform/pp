---
title: مقاوم للأزمات المصرفية
# title : "لنبني عالمًا أفضل بعد إعادة الضبط العظمى"
banner: "/photos/holdhands.jpg"
lead : "دعونا نحوّل عملية إعادة الضبط العظمى من اقتصادات العالم التقليدية والرأسمالية إلى اقتصاديات الطاو وشبكة الطاو وذلك من أجل مستقبل مستدامٍ."
description : "دعونا نحوّل عملية إعادة الضبط العظمى من اقتصادات العالم التقليدية والرأسمالية إلى اقتصاديات الطاو وشبكة الطاو وذلك من أجل مستقبل مستدامٍ."
youtube: "rVP73TV2X0s"
draft: true

# countries:
#   title : "Locations"
#   flags:
#     - name : "Quezon City"
#       flag : "/flags/ph.png"
#     - name : "San Pedro Laguna"
#       flag : "/flags/ph.png"
#     - name : "Kuala Lumpur (coming soon!)"
#       flag : "/flags/my.png"
#     - name : "Hanoi (coming soon!)"
#       flag : "/flags/vn.png"
#     - name : "Saigon (coming soon!)"
#       flag : "/flags/vn.png"      



##################### Feature ##########################

features:
  image: "/photos/yay.jpg"
  title : "شبكة الطاو نظام كامل بُني خصيصًا للأزمات."
  items:
    - name : "ما من مال؟ ما من مشكلة!"
      icon : "<svg  viewBox='0 0 288 512' height='50px' fill='dimgray'><path d='M209.2 233.4l-108-31.6C88.7 198.2 80 186.5 80 173.5c0-16.3 13.2-29.5 29.5-29.5h66.3c12.2 0 24.2 3.7 34.2 10.5 6.1 4.1 14.3 3.1 19.5-2l34.8-34c7.1-6.9 6.1-18.4-1.8-24.5C238 74.8 207.4 64.1 176 64V16c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v48h-2.5C45.8 64-5.4 118.7.5 183.6c4.2 46.1 39.4 83.6 83.8 96.6l102.5 30c12.5 3.7 21.2 15.3 21.2 28.3 0 16.3-13.2 29.5-29.5 29.5h-66.3C100 368 88 364.3 78 357.5c-6.1-4.1-14.3-3.1-19.5 2l-34.8 34c-7.1 6.9-6.1 18.4 1.8 24.5 24.5 19.2 55.1 29.9 86.5 30v48c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16v-48.2c46.6-.9 90.3-28.6 105.7-72.7 21.5-61.6-14.6-124.8-72.5-141.7z'/></svg>"    
      content : "تُتيح اقتصاديات الطاو إجراء المعاملات وإبرام العقود الاجتماعية المتعلقة بالمال أو المقايضة أو الموارد وفق قيمتها المتداولة في الأسواق."
      
    - name : "نظام مقاوم (وجاهز) للأزمات"
      icon : "<svg  viewBox='0 0 480 512' height='50px' fill='dimgray'><path d='M471.99 334.43L336.06 256l135.93-78.43c7.66-4.42 10.28-14.2 5.86-21.86l-32.02-55.43c-4.42-7.65-14.21-10.28-21.87-5.86l-135.93 78.43V16c0-8.84-7.17-16-16.01-16h-64.04c-8.84 0-16.01 7.16-16.01 16v156.86L56.04 94.43c-7.66-4.42-17.45-1.79-21.87 5.86L2.15 155.71c-4.42 7.65-1.8 17.44 5.86 21.86L143.94 256 8.01 334.43c-7.66 4.42-10.28 14.21-5.86 21.86l32.02 55.43c4.42 7.65 14.21 10.27 21.87 5.86l135.93-78.43V496c0 8.84 7.17 16 16.01 16h64.04c8.84 0 16.01-7.16 16.01-16V339.14l135.93 78.43c7.66 4.42 17.45 1.8 21.87-5.86l32.02-55.43c4.42-7.65 1.8-17.43-5.86-21.85z'/></svg>"
      content : "يُمكن أن تكون العقود الاجتماعية ربحية أو غير ربحية، مما يتيح لها أن تكون نافعة حتى أثناء الأزمات."
      
    - name : "وداعا أيها اللايقين!"
      icon : "<svg  viewBox='0 0 640 512' height='50px' fill='dimgray'><path d='M368 32h-96c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h96c17.67 0 32-14.33 32-32V64c0-17.67-14.33-32-32-32zM208 88h-84.75C113.75 64.56 90.84 48 64 48 28.66 48 0 76.65 0 112s28.66 64 64 64c26.84 0 49.75-16.56 59.25-40h79.73c-55.37 32.52-95.86 87.32-109.54 152h49.4c11.3-41.61 36.77-77.21 71.04-101.56-3.7-8.08-5.88-16.99-5.88-26.44V88zm-48 232H64c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h96c17.67 0 32-14.33 32-32v-96c0-17.67-14.33-32-32-32zM576 48c-26.84 0-49.75 16.56-59.25 40H432v72c0 9.45-2.19 18.36-5.88 26.44 34.27 24.35 59.74 59.95 71.04 101.56h49.4c-13.68-64.68-54.17-119.48-109.54-152h79.73c9.5 23.44 32.41 40 59.25 40 35.34 0 64-28.65 64-64s-28.66-64-64-64zm0 272h-96c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h96c17.67 0 32-14.33 32-32v-96c0-17.67-14.33-32-32-32z'/></svg>"
      content : "يُمكن أن تكون العقود الاجتماعية ربحية أو غير ربحية، مما يتيح لها أن تكون نافعة حتى أثناء الأزمات."
    

######################### How #####################
how:
  title : "كيف يعمل نظامنا؟"
  steps:
    - image: "/screens/po1.jpg"
      id: 1
      content: "أُنشر صورًا أو وصفًا لغرض تملكه"
    - image: "/screens/po2.jpg"
      id: 2
      content: "أُنشر صورًا أو وصفًا لغرض تملكه"
    - image: "/screens/po3.jpg"
      id: 3
      content: "بِع لهم الغرض أو قايضه أو امنحه وفق عقد معين سواء مقابل مال أو مجانًا"

        
################### Plat Skills ########################

circles:
  title : "المنصات:"
  cards:
    #- name : "Pantry"
    #  image : "https://sorasystem.sirv.com/icons/pantry.png"
    #  content : "A waste to wealth system that uses points and point-banks instead of currency, to de-commercialize an economy and bank the unbanked"
    #  link: "pantry"

    - name : "Pantry Hub"
      image : "/icons/pantry.png"
      content : "منصة ون (Pantry Hub) وهي شبكة اجتماعية."
      link: "/home"

    - name : "Pantry World"
      image : "/icons/world.png"
      content : "و'World' أو طاو بوول هي منصة استيراد وتصدير تتيح للجميع الاستيراد بالعملات المحلية أو بالنقاط."
      link: "/world"

    - name : "Pantrylitics"
      image : "/icons/pantrylitics.png"
      content : "فيما تتمثل المؤشرات (Pantrylitics) في لوحة تحليلات للبيانات من كلا المنصتين منصة ون (Pantry Hub) ومنصة بول (Pantry World) مما يسمح بالإشراف عليها ورسم سياسات التعامل ضمنه في الوقت الحقيقي."
      link: "/pantrylitics"


circles2:
  title : "المميزات:"
  cards:
    - name : "الموارد المصرفية"
      image : "/icons/banking.png"
      link: "/banking"
      content : "تحوّل الموارد المصرفية الإنتاجية إلى نقاط وتظهر كدخل أساسي شامل (Basic Universal Revenue). Target: Tentative in all platforms"
      
    - name : "المخزن Pantry"
      image : "/icons/pantry.png"
      link: "/hub"
      content : "وهو نظام يحوّل المُهدر إلى ثروة ويستخدم النقاط وبنوك النقاط بدل العملات، وذلك خلع رداء التجارة عن الاقتصاد وإضفاء الصفة المصرفية الاقتصادية على ما لا صفة مصرفية له (مثل الأمور المعنوية كالنزاهة والثقة والولاء والإنتاجية...)"      
      # content : "ISAIAH Predict gives insights on potential future events based on historical data"
      
    - name : "نظام للربط "
      image : "/icons/match.png"
      link: "/match"
      content : "وهو نظام ربط غرضه زيادة معدل نجاح العقود الاجتماعية. Target: Tentative in all platforms"

    - name : "منظومة تعلّم"
      image : "/icons/pl.png"
      link: "/learn"
      content : "وهو نظام تعليمي قائمة على المساقات والتدريب العمليّ يمكن أن يستخدم حتى خلال الأوبئة وأزمنة الغلاء والفقر. Target: 2023 in Pantry"
      
    - name : "نظام ضرائب مُنصف"
      image : "/icons/pt.png"
      link: "/pointtax"
      content : "وهو نظام ضريبي مزدوج القيد يمنع التهرّب الضريبي ويرفع من العائدات الضريبية بتوزيعها على عدد أكبر من الناس.. Tentative in One"
      
    - name : "منظومة صحيّة"
      image : "/icons/ph.png"
      link: "/health"
      content : "وهو نظام تشخيص صحي يتيح للناس الذين لا مال لهم الوصول للخدمات الطبية والعلاجات التي يحتاجونها. Target: Tentative in Pantry"



feedback:
  title: "Feedback"
  items:
    - user : "Prof. Dhanjoo Ghista, University 2020 Foundation President"
      image: "/avatars/dhanjoo.jpg"
      content: "I very much like this work:A Research Proposal For The Formalization Of The Science Of Supereconomics And The Establishment Of A Point-Based Economic System"
      link: "http://www.dhanjooghista.com"
    - user : "Prof. Gavin Kennedy, a world expert on Adam Smith"
      image: "/avatars/gavin.jpg"
      content: "I believe it is developing into a worthwhile project" 
      link: "http://adamsmithslostlegacy.blogspot.com"

##################### Call to action #####################

cta:
  title : "Register in the Waitlist"
  link1text: "Sure!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "I need to know more"
  link2 : "https://superphysics.org/"
  
---
