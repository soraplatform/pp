---
# title: 타고난 재무 위기에 대해 강인함
# title: 은행 위기에 대항하는 견고한 시스템입니다.
title: 인플레이션을 해결하는 순환경제
banner: "/photos/holdhands.jpg"
lead: Pantrypoints는 만남을 통해 양방향 점수를 사용하여 인플레이션을 막고 전체 고용을 허용하며 순환경제를 실현하는 강력한 새로운 경제 시스템입니다.
description: Pantrypoints는 만남을 통해 양방향 점수를 사용하여 인플레이션을 막고 전체 고용을 허용하며 순환경제를 실현하는 강력한 새로운 경제 시스템입니다.
# lead: 팬트리포인트는 견고한 새로운 경제 시스템으로서, 미팅을 통해 양자간의 포인트를 사용하여 인플레이션을 막고 어떤 경제적 상황에서도 전체 고용을 허용합니다.
# description: 팬트리포인트는 견고한 새로운 경제 시스템으로서, 미팅을 통해 양자간의 포인트를 사용하여 인플레이션을 막고 어떤 경제적 상황에서도 전체 고용을 허용합니다. 
# lead : "팬트리포인트는 견고한 새로운 경제 시스템으로, 이중 점수를 이용하여 인플레이션을 막고 어떠한 경제적 상황에서도 완전 고용을 가능하게 합니다. 이를 위해 모임을 통해 이중 점수를 사용합니다."
# description: "팬트리포인트는 견고한 새로운 경제 시스템으로, 이중 점수를 이용하여 인플레이션을 막고 어떠한 경제적 상황에서도 완전 고용을 가능하게 합니다. 이를 위해 모임을 통해 이중 점수를 사용합니다."
youtube: "N1ewbp7eTdQ"
# アプリに登録してください。私たちのシステムの科学を読んでください。
# Regístrate en la aplicación. Lee la Ciencia de nuestro sistema.


req:
  title: 돈에 대해 무지한 몽골인들에게는 소가 가치의 척도였습니다. 그들에게는 부는 소의 수로 측정되었으며, 스페인인들에게는 부는 금과 은의 양으로 측정되었습니다. 몽골인들의 생각이 더 정확합니다. (아담 스미스)
  # link: "https://superphysics.org/research/socrates/simple-republic/book-3/chapter-3"
  # btext: "View the requirements"
  avatar: "/avatars/smith.png"



##################### Feature ##########################

features:
  image: "/photos/yay.jpg"
  title : 트리섹션 소개
  subtitle: 트리섹션은 어떤 사회적 상황에서도 경제가 움직일 수 있도록 돈, 무화폐 또는 메타버스 거래를 허용하여 진정한 경제적 자유를 제공합니다
  items:
    - name : "무화폐"
      icon : "<svg viewBox='0 0 576 512' fill='darkcyan' height='50px'><path d='M96,128A64,64,0,1,0,32,64,64,64,0,0,0,96,128Zm0,176.08a44.11,44.11,0,0,1,13.64-32L181.77,204c1.65-1.55,3.77-2.31,5.61-3.57A63.91,63.91,0,0,0,128,160H64A64,64,0,0,0,0,224v96a32,32,0,0,0,32,32V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V383.61l-50.36-47.53A44.08,44.08,0,0,1,96,304.08ZM480,128a64,64,0,1,0-64-64A64,64,0,0,0,480,128Zm32,32H448a63.91,63.91,0,0,0-59.38,40.42c1.84,1.27,4,2,5.62,3.59l72.12,68.06a44.37,44.37,0,0,1,0,64L416,383.62V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V352a32,32,0,0,0,32-32V224A64,64,0,0,0,512,160ZM444.4,295.34l-72.12-68.06A12,12,0,0,0,352,236v36H224V236a12,12,0,0,0-20.28-8.73L131.6,295.34a12.4,12.4,0,0,0,0,17.47l72.12,68.07A12,12,0,0,0,224,372.14V336H352v36.14a12,12,0,0,0,20.28,8.74l72.12-68.07A12.4,12.4,0,0,0,444.4,295.34Z'/></svg>"    
      content : "무화폐 거래는 곡물에 편정된 포인트로 측정된 저장 가치 교환을 사용합니다. 이는 아담 스미스의 '국부론' 에서 언급된 곡물 기반 가치 평가를 구현한 것입니다"
    - name : "돈"
      icon : "<svg fill='darkcyan' height='50px' viewBox='0 0 640 512'><path d='M621.16 54.46C582.37 38.19 543.55 32 504.75 32c-123.17-.01-246.33 62.34-369.5 62.34-30.89 0-61.76-3.92-92.65-13.72-3.47-1.1-6.95-1.62-10.35-1.62C15.04 79 0 92.32 0 110.81v317.26c0 12.63 7.23 24.6 18.84 29.46C57.63 473.81 96.45 480 135.25 480c123.17 0 246.34-62.35 369.51-62.35 30.89 0 61.76 3.92 92.65 13.72 3.47 1.1 6.95 1.62 10.35 1.62 17.21 0 32.25-13.32 32.25-31.81V83.93c-.01-12.64-7.24-24.6-18.85-29.47zM48 132.22c20.12 5.04 41.12 7.57 62.72 8.93C104.84 170.54 79 192.69 48 192.69v-60.47zm0 285v-47.78c34.37 0 62.18 27.27 63.71 61.4-22.53-1.81-43.59-6.31-63.71-13.62zM320 352c-44.19 0-80-42.99-80-96 0-53.02 35.82-96 80-96s80 42.98 80 96c0 53.03-35.83 96-80 96zm272 27.78c-17.52-4.39-35.71-6.85-54.32-8.44 5.87-26.08 27.5-45.88 54.32-49.28v57.72zm0-236.11c-30.89-3.91-54.86-29.7-55.81-61.55 19.54 2.17 38.09 6.23 55.81 12.66v48.89z'/></svg>"
      content : "'돈 포인트' 를 사용하여 무화폐 거래를 현금 또는 현금 없는 은행 앱을 통해 지불할 수 있습니다. 이는 파이어통화 경제의 일환입니다"
    - name : "메타버스"
      icon: <svg fill='darkcyan' height='50px' viewBox='0 0 320 512'><path d='M311.9 260.8L160 353.6 8 260.8 160 0l151.9 260.8zM160 383.4L8 290.6 160 512l152-221.4-152 92.8z'/></svg>    
      content : "포인트는 이더리움을 통해 메타버스에서 규제된 거래를 위해 팬트리 코인으로 전환될 수 있습니다. 이는 국경을 넘어가는 거래 및 우리가 제안하는 '크립토 이징' (양적 완화의 대안) 에 유용합니다"
    

how:
  title : "작동 방법"  
  steps:
    - content: "상품을 돈 대신 포인트 가격으로 게시합니다."
      image: "/screens/pantryshow.jpg"
      id: 1
    - content: "상품 구매자들과 만나세요."
      image: "/photos/shake.jpg"
      id: 2    
    - content: "포인트로 지불하세요."
      image: "/screens/pay.jpg"
      id: 3

##################### Call to action #####################

cta:
  title : "대기 명단에 등록하세요."
  link1text: "네!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "지금은 아닙니다."
  link2 : "https://superphysics.org/"
  
---
