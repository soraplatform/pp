---
title: "The Maximized Farming System"
# Better health for all
subtitle: "Pantrypreneur Farm is a management app for a 'Maximum Farm'"
logo: "/logos/farm.png"
image: "/logos/farm.png"
description: "Pantrypreneur Farm is a management app for a 'Maximum Farm'"
youtube: blkb6Y_veTs 
# ELBIgpwEqSs
# youtube: "duf_nbYAa5o"
aliases:
  /farm
 

# features:
personas:
  title: "An App for your Maximum Farm"
  subtitle: "Pantrypreneur Farm lets you manage your farm to keep it maximized using crop-combinations, organic fertilizers, sound, and the Pantrypoints system."
  image: "/photos/coconuts.jpg"
  items:
    - name : "Crop Combinations"
      icon: /icons/plants.png
      # icon : "<svg fill='gray' style='height: 50px'  viewBox='0 0 640 512'><path d='M434.7 64h-85.9c-8 0-15.7 3-21.6 8.4l-98.3 90c-.1.1-.2.3-.3.4-16.6 15.6-16.3 40.5-2.1 56 12.7 13.9 39.4 17.6 56.1 2.7.1-.1.3-.1.4-.2l79.9-73.2c6.5-5.9 16.7-5.5 22.6 1 6 6.5 5.5 16.6-1 22.6l-26.1 23.9L504 313.8c2.9 2.4 5.5 5 7.9 7.7V128l-54.6-54.6c-5.9-6-14.1-9.4-22.6-9.4zM544 128.2v223.9c0 17.7 14.3 32 32 32h64V128.2h-96zm48 223.9c-8.8 0-16-7.2-16-16s7.2-16 16-16 16 7.2 16 16-7.2 16-16 16zM0 384h64c17.7 0 32-14.3 32-32V128.2H0V384zm48-63.9c8.8 0 16 7.2 16 16s-7.2 16-16 16-16-7.2-16-16c0-8.9 7.2-16 16-16zm435.9 18.6L334.6 217.5l-30 27.5c-29.7 27.1-75.2 24.5-101.7-4.4-26.9-29.4-24.8-74.9 4.4-101.7L289.1 64h-83.8c-8.5 0-16.6 3.4-22.6 9.4L128 128v223.9h18.3l90.5 81.9c27.4 22.3 67.7 18.1 90-9.3l.2-.2 17.9 15.5c15.9 13 39.4 10.5 52.3-5.4l31.4-38.6 5.4 4.4c13.7 11.1 33.9 9.1 45-4.7l9.5-11.7c11.2-13.8 9.1-33.9-4.6-45.1z'/></svg>"
      content : "Find the optimum crop combination for your mixed plots"

    - name : "Sound IoT"
      icon: /icons/sound.png    
      # icon : "<svg fill='gray' style='height: 50px'  viewBox='0 0 512 512'><path d='M216 260c0 15.464-12.536 28-28 28s-28-12.536-28-28c0-44.112 35.888-80 80-80s80 35.888 80 80c0 15.464-12.536 28-28 28s-28-12.536-28-28c0-13.234-10.767-24-24-24s-24 10.766-24 24zm24-176c-97.047 0-176 78.953-176 176 0 15.464 12.536 28 28 28s28-12.536 28-28c0-66.168 53.832-120 120-120s120 53.832 120 120c0 75.164-71.009 70.311-71.997 143.622L288 404c0 28.673-23.327 52-52 52-15.464 0-28 12.536-28 28s12.536 28 28 28c59.475 0 107.876-48.328 108-107.774.595-34.428 72-48.24 72-144.226 0-97.047-78.953-176-176-176zm-80 236c-17.673 0-32 14.327-32 32s14.327 32 32 32 32-14.327 32-32-14.327-32-32-32zM32 448c-17.673 0-32 14.327-32 32s14.327 32 32 32 32-14.327 32-32-14.327-32-32-32zm480-187.993c0-1.518-.012-3.025-.045-4.531C510.076 140.525 436.157 38.47 327.994 1.511c-14.633-4.998-30.549 2.809-35.55 17.442-5 14.633 2.81 30.549 17.442 35.55 85.906 29.354 144.61 110.513 146.077 201.953l.003.188c.026 1.118.033 2.236.033 3.363 0 15.464 12.536 28 28 28s28.001-12.536 28.001-28zM152.971 439.029l-80-80L39.03 392.97l80 80 33.941-33.941z'/></svg>"
      content : "Grow your crops with sound"
      
    - name : "Points Compensation System"
      icon: /icons/medal.png
      # icon : "<svg fill='gray' style='height: 50px' viewBox='0 0 512 512'><path d='M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z'/></svg>"
      content : "Maximum Farming uses points to compensate farming labor"

    - name : "Integrates with Pantrypoints City"
      #om
      icon: /icons/pantry50.png
      # icon : "<svg fill='gray' style='height: 50px' viewBox='0 0 640 512'><path d='M616 192H480V24c0-13.26-10.74-24-24-24H312c-13.26 0-24 10.74-24 24v72h-64V16c0-8.84-7.16-16-16-16h-16c-8.84 0-16 7.16-16 16v80h-64V16c0-8.84-7.16-16-16-16H80c-8.84 0-16 7.16-16 16v80H24c-13.26 0-24 10.74-24 24v360c0 17.67 14.33 32 32 32h576c17.67 0 32-14.33 32-32V216c0-13.26-10.75-24-24-24zM128 404c0 6.63-5.37 12-12 12H76c-6.63 0-12-5.37-12-12v-40c0-6.63 5.37-12 12-12h40c6.63 0 12 5.37 12 12v40zm0-96c0 6.63-5.37 12-12 12H76c-6.63 0-12-5.37-12-12v-40c0-6.63 5.37-12 12-12h40c6.63 0 12 5.37 12 12v40zm0-96c0 6.63-5.37 12-12 12H76c-6.63 0-12-5.37-12-12v-40c0-6.63 5.37-12 12-12h40c6.63 0 12 5.37 12 12v40zm128 192c0 6.63-5.37 12-12 12h-40c-6.63 0-12-5.37-12-12v-40c0-6.63 5.37-12 12-12h40c6.63 0 12 5.37 12 12v40zm0-96c0 6.63-5.37 12-12 12h-40c-6.63 0-12-5.37-12-12v-40c0-6.63 5.37-12 12-12h40c6.63 0 12 5.37 12 12v40zm0-96c0 6.63-5.37 12-12 12h-40c-6.63 0-12-5.37-12-12v-40c0-6.63 5.37-12 12-12h40c6.63 0 12 5.37 12 12v40zm160 96c0 6.63-5.37 12-12 12h-40c-6.63 0-12-5.37-12-12v-40c0-6.63 5.37-12 12-12h40c6.63 0 12 5.37 12 12v40zm0-96c0 6.63-5.37 12-12 12h-40c-6.63 0-12-5.37-12-12v-40c0-6.63 5.37-12 12-12h40c6.63 0 12 5.37 12 12v40zm0-96c0 6.63-5.37 12-12 12h-40c-6.63 0-12-5.37-12-12V76c0-6.63 5.37-12 12-12h40c6.63 0 12 5.37 12 12v40zm160 288c0 6.63-5.37 12-12 12h-40c-6.63 0-12-5.37-12-12v-40c0-6.63 5.37-12 12-12h40c6.63 0 12 5.37 12 12v40zm0-96c0 6.63-5.37 12-12 12h-40c-6.63 0-12-5.37-12-12v-40c0-6.63 5.37-12 12-12h40c6.63 0 12 5.37 12 12v40z'/></svg>"
      content : "Pantrypreneur Farm easiliy integrates with the Pantrypoints system"
      

######################### How #####################

how:
  title : "How it Works"
  steps:
    - id: 1
      # image: "/screens/farm/listplots.jpg"
      image: "/screens/farm2.png"
      content: "Enter the details of your plots"
    - id: 2
      image: "/screens/farm2.jpg"    
      # image: "/screens/farm/listseeds.jpg"
      content: "Choose which seeds are available"
    - id: 3
      image: "/screens/farm1.jpg"
      # image: "/screens/farm/listdata.jpg"
      content: "Get estimates on yield, points-revenue, and costs in effort (via the Effort Theory of Value)"


req:
  title: Based on the requirements in Ideal Farming 
  link: "https://superphysics.org/research/sarkar/farming/section-2/mixed"
  btext: "View the requirements"
  avatar: "/avatars/sarkar.jpg"

##################### Call to action #####################

cta:
  title : "I want Farm for my Agribusiness"
  link1text: "Sure!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "I need to know more"
  link2 : "https://superphysics.org/research/sarkar/farming/section-1/agriculture"
---

