---
title: "Your Own Jobs Site"
# Better health for all
subtitle: "Pantrypreneur Work is a jobs site for your own company."
logo: "/logos/work.png"
image: "/logos/work.png"
description: "Pantrypreneur Work is a jobs site for your own company. It lets you post jobs and add questions to filter applicants more easily."
# youtube: "3FHG_VoDlhs"
# youtube: "oJXjxLYQQ08"
# youtube: "ldAX9XGTvX0"
# https://www.youtube.com/watch?v=ldAX9XGTvX0
# youtube: "oBFBDtAmUvk"
# youtube: "EP2y4jjGE6Q"
applink: https://hub.pantrypoints.com/signup
apptext: "Register in the Waitlist"
aliases:
  /jobs
  /pantrypreneur/jobs
  /work
  

# features:
personas:
  title: "Digitize Human Resources Tasks"
  subtitle: "Pantrypreneur Work integrates basic Human Resources functionalities to help you manage your staff more easily"
  image: "/photos/pawns.jpg"
  items:
    - name : "Personality Questions"
      icon: /icons/q.png
      #pills
      # icon : "<svg style='height: 50px' fill='dimgray' viewBox='0 0 512 512'><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d='M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z'/></svg>"
      content : "Filter the applicants based on their personality"
      
    - name : "Can be Integrated with HR Features"
      #om
      icon: /icons/banker.png
      # icon : "<svg style='height: 50px' fill='dimgray' viewBox='0 0 448 512'><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d='M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm95.8 32.6L272 480l-32-136 32-56h-96l32 56-32 136-47.8-191.4C56.9 292 0 350.3 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-72.1-56.9-130.4-128.2-133.8z'/></svg>"
      content : "Pantrypreneur Work can connect with Pantrypreneur Klock and Appraise to form a basic HR system"
      

######################### How #####################

how:
  title : "How it Works"
  steps:
    - image: "/screens/jobs/jobslist.jpg"
      id: 1
      content: "Post your job opening and add some personality questions"  
    - image: "/screens/match/2.jpg"
      id: 2
      content: "Filter the applicants based on their personality match to your job opening"
    - image: "/screens/klock/klock.jpg"
      id: 3
      content: "Connect the employment data with other Pantrypreneur Features like Klock and Appraise"

# duo:
#   banner: "/photos/holdhands.jpg"
#   title : "The Freedom to Choose"
#   subtitle: "With Pantry Health, there are no excuses not to get cured or prevent disease"
#   items:
#     - image: "/graphics/doctor.png"
#       imagetext: "Pantry Health combines all data to come up with possible solutions, referencing both Western and Alternative Medicine, through ISAIAH"
#     - image: "/screens/services2.jpg"
#       imagetext: "After you choose the treatment that best matches with you, browse the local third-party suppliers, some of which allow moneyless payments"



# feedback:
#   title: "Feedback"
#   items:
#     # - user : "remotehour.co"
#     #   image: "/avatars/blankavatar.png"
#     #   content: "Oh your progress is excellent! Nice work :) With Covid-19, we came to be careful about our health. So your product will be demanded"
#     #   link: "http://remotehour.co"

#     # - user : "iflux.app"
#     #   image: "/avatars/blankavatar.png"
#     #   content: "A very awesome platform. I cannot wait for the app to be released" 
#     #   link: "https://iflux.app"

#     # - user : "finzen.app"
#     #   image: "/icons/finzen.png"
#     #   content: "In general, I don't put much trust on alternative medicine, but I think that it does have a place in our modern health system and it can be a great complement. " 
#     #   link: "https://finzen.app"      

#     - user : "matayog.com"
#       image: "/icons/matayog.png"
#       content: "It's exactly what we are promoting in our wellness center -- a holistic health system!" 
#       link: "https://matayog.com"

#     - user : "seobuddy.com"
#       image: "/icons/seobuddy.jpg"
#       content: "I appreciate the effort and love you put in this vision." 
#       link: "https://seobuddy.com"

    # - user : "Aabshar (Pioneer tournament), June 2020"
    #   image: "/avatars/blankavatar.png"
    #   content: "Looking forward to mobile app and website. I believe new medicines are more advanced"
      # link: "http://www.dhanjooghista.com"
    # - user : "mon.rocks"
    #   image: "/avatars/blankavatar.png"
    #   content: "Good effort. Anything medtech is especially crucial these days" 
    #   link: "https://mon.rocks"

req:
  title: Based on the requirements in Book 1, Chapter 8a of the Wealth of Nations
  link: "https://superphysics.org/research/smith/wealth-of-nations/book-1/chapter-8a"
  btext: "View the requirements"
  avatar: "/avatars/smith.png"

##################### Call to action #####################

cta:
  title : "I want Work for my Company"
  link1text: "Sure!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "I need to know more"
  link2 : "https://superphysics.org/research/smith/wealth-of-nations/book-1/chapter-8e"
---

