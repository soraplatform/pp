---
# title: "Learn freely"
title: Reduce Teaching Burdens
# title: Pantry Learn
subtitle: "Pantrypreneur Educate is a platform for small schools to ease administrative work"
# are social education systems derived from Adam Smith's educational reform proposal in the Wealth of Nations
logo: "/logos/educate.png"
image: "/og/educate.jpg"
description: "Pantrypreneur Educate and Apprentice are social education systems derived from Adam Smith's educational reform proposal in the Wealth of Nations"
# youtube: aNVyHBc1CEI
youtube: By4cXCekyDY
# youtube: "3FHG_VoDlhs"
# youtube: "DjGsLaUvYcc"
# youtube: "_6oLDaX3T00"
applink: https://hub.pantrypoints.com/signup
apptext: "Register in the Waitlist"
aliases:
  /learn
  /educate
  /pantrypreneur/learn



features:
  image: /photos/bedcrop.jpg
  title : "Digitize Your School's Administrative Work"
  sub: "Pantrypreneur Educate lessens the burden of administrative work so you can focus on teaching"
  # title : "Points-based learning and payment"
  # sub: "Pantry will help the unemployed and unbanked get access to food through their local community"
  items:
    - name : "Attendance"
      icon: "<svg height='50px' fill='dimgray' viewBox='0 0 640 512'><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d='M496 224c-79.6 0-144 64.4-144 144s64.4 144 144 144 144-64.4 144-144-64.4-144-144-144zm64 150.3c0 5.3-4.4 9.7-9.7 9.7h-60.6c-5.3 0-9.7-4.4-9.7-9.7v-76.6c0-5.3 4.4-9.7 9.7-9.7h12.6c5.3 0 9.7 4.4 9.7 9.7V352h38.3c5.3 0 9.7 4.4 9.7 9.7v12.6zM320 368c0-27.8 6.7-54.1 18.2-77.5-8-1.5-16.2-2.5-24.6-2.5h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h347.1c-45.3-31.9-75.1-84.5-75.1-144zm-96-112c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128z'/></svg>"
      #hand holding
      # icon : "<svg  viewBox='0 0 640 512' height='50px' fill='dimgray'><path d='M434.7 64h-85.9c-8 0-15.7 3-21.6 8.4l-98.3 90c-.1.1-.2.3-.3.4-16.6 15.6-16.3 40.5-2.1 56 12.7 13.9 39.4 17.6 56.1 2.7.1-.1.3-.1.4-.2l79.9-73.2c6.5-5.9 16.7-5.5 22.6 1 6 6.5 5.5 16.6-1 22.6l-26.1 23.9L504 313.8c2.9 2.4 5.5 5 7.9 7.7V128l-54.6-54.6c-5.9-6-14.1-9.4-22.6-9.4zM544 128.2v223.9c0 17.7 14.3 32 32 32h64V128.2h-96zm48 223.9c-8.8 0-16-7.2-16-16s7.2-16 16-16 16 7.2 16 16-7.2 16-16 16zM0 384h64c17.7 0 32-14.3 32-32V128.2H0V384zm48-63.9c8.8 0 16 7.2 16 16s-7.2 16-16 16-16-7.2-16-16c0-8.9 7.2-16 16-16zm435.9 18.6L334.6 217.5l-30 27.5c-29.7 27.1-75.2 24.5-101.7-4.4-26.9-29.4-24.8-74.9 4.4-101.7L289.1 64h-83.8c-8.5 0-16.6 3.4-22.6 9.4L128 128v223.9h18.3l90.5 81.9c27.4 22.3 67.7 18.1 90-9.3l.2-.2 17.9 15.5c15.9 13 39.4 10.5 52.3-5.4l31.4-38.6 5.4 4.4c13.7 11.1 33.9 9.1 45-4.7l9.5-11.7c11.2-13.8 9.1-33.9-4.6-45.1z'/></svg>"
      content : "Students clock in for each class"
      # content : "Students pay with their work and learn on the job without the debt"
      
    # - name : "For Exchangers"
      #question
    - name : "Points-Capable"      
      icon : "<svg height='50px' fill='dimgray' viewBox='0 0 576 512'><path d='M96,128A64,64,0,1,0,32,64,64,64,0,0,0,96,128Zm0,176.08a44.11,44.11,0,0,1,13.64-32L181.77,204c1.65-1.55,3.77-2.31,5.61-3.57A63.91,63.91,0,0,0,128,160H64A64,64,0,0,0,0,224v96a32,32,0,0,0,32,32V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V383.61l-50.36-47.53A44.08,44.08,0,0,1,96,304.08ZM480,128a64,64,0,1,0-64-64A64,64,0,0,0,480,128Zm32,32H448a63.91,63.91,0,0,0-59.38,40.42c1.84,1.27,4,2,5.62,3.59l72.12,68.06a44.37,44.37,0,0,1,0,64L416,383.62V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V352a32,32,0,0,0,32-32V224A64,64,0,0,0,512,160ZM444.4,295.34l-72.12-68.06A12,12,0,0,0,352,236v36H224V236a12,12,0,0,0-20.28-8.73L131.6,295.34a12.4,12.4,0,0,0,0,17.47l72.12,68.07A12,12,0,0,0,224,372.14V336H352v36.14a12,12,0,0,0,20.28,8.74l72.12-68.07A12.4,12.4,0,0,0,444.4,295.34Z'/></svg>"
      content : "Pay in points if money is lacking"
      # Do Language Exchange based on points
     # content : "Learn the modules that you want for the job that you want"
      
    - name : "Grades"
      #users
      icon : "<svg height='50px' fill='dimgray' viewBox='0 0 512 512'><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d='M496 384H64V80c0-8.84-7.16-16-16-16H16C7.16 64 0 71.16 0 80v336c0 17.67 14.33 32 32 32h464c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16zM464 96H345.94c-21.38 0-32.09 25.85-16.97 40.97l32.4 32.4L288 242.75l-73.37-73.37c-12.5-12.5-32.76-12.5-45.25 0l-68.69 68.69c-6.25 6.25-6.25 16.38 0 22.63l22.62 22.62c6.25 6.25 16.38 6.25 22.63 0L192 237.25l73.37 73.37c12.5 12.5 32.76 12.5 45.25 0l96-96 32.4 32.4c15.12 15.12 40.97 4.41 40.97-16.97V112c.01-8.84-7.15-16-15.99-16z'/></svg>"
      content : "Visualize student performance and transfer student profiles between schools" 
      # Hire the people with the skills that you need, without paying for those that you don't"


######################### Service #####################
how:
  title : "How it Works"
  # for Exchanges
  steps:
    - id: 1
      image: "/screens/learn1.jpg"
      content: "The students browse the classes in their school"
      # content: "Browse the list of language exchangers and meetup for a class"
      # content: "List the modules or skill sets that are required by the job that you want, or be an apprentice of the business that you want to join"
    - id: 2
      image: "/screens/learn2.jpg"
      content: "The student clocks in the class he is registered in. The teacher can enter test scores to visualize the performance of each student"
      # content: "Pay in points after class"
      # content: "Enroll in the modules or complete the requirements of the apprenticeship"
    - id: 3
      image: "/screens/learn3.jpg"
      content: "Students can pay in points when money is lacking"
      # content: "Take the test in your desired company to get hired, or get hired by your trainer"



    # content : "<ol><li></li><li></li><li></li><li>Pay off your learning with your work</li></ol>"
    # quote: "Most of the improvements made in several branches of philosophy were not made in universities.. For a long time, several of those universities chose to remain the sanctuaries for exploded systems and obsolete prejudices after they had been hunted out of other parts of the world.. In general, the richest and best endowed universities were the slowest in adopting those improvements."
    # quota: "/avatars/smith.png"
    # button:
    #   enable : false
    #   label : "See active cities"
    #   link : "#"

segment:
  articles:
    - title : "Integrates with ISAIAH Match"
      image: "/og/match.jpg"
      content : "Pantrypreneur Educate comes with our personality tool called ISAIAH Match to match students with the proper teachers or lessons."



req:
  title: Based on the requirements in Book 5, Chapter 1 of the Wealth of Nations
  link: "https://superphysics.org/research/smith/wealth-of-nations/book-5/chapter-1/part-3zd"
  btext: "Read the requirements"
  avatar: "/avatars/smith.png"


feedback:
  title: Awards
  items:
    - user : World Vision Philippines
      image: https://sorasystem.sirv.com/logos/wv.jpg
      content: Social Impact Challenge Manila 2019
      # link: "https://worldvision.org.ph/news/social-innovation"

##################### Call to action #####################

cta:
  title : "Register in the Waitlist"
  link1text: "Sure"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "I'm not convinced yet"
  link2 : "https://superphysics.org/research/smith/wealth-of-nations/book-5/chapter-1/part-3zd"
  
---
