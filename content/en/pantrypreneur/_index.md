---
title: "Digitalize Your Business"
subtitle: "Pantrypreneur is a platform for micro and small businesses for digitalizing common business tasks"
logo: "/logos/pr.png"
image: "/og/preneur.jpg"
description: "Pantrypreneur is a platform for micro and small businesses for digitalizing common business tasks"
# youtube: "ZE7LrTVmfaw"
# youtube: "rVP73TV2X0s"
# youtube: H83IsXa2tPk
# youtube: lRZbEcKMqpc
youtube: zIrblIJF4Nk
#  EP2y4jjGE6Q"
applink: https://hub.pantrypoints.com/signup
apptext: "Register in the Waitlist"
aliases:
  /preneur



circles:
  title: "Modules"
  cards:
    - name : "Appraise"
      image : /icons/appraise.png
      content : "A basic performance appraisal system for your employees"
      footer: "<span class='text-secondary'>Being developed</span>"                        

    # - name : "Inventory"
    #   image : /icons/pi2.png
    #   content : "A basic inventory system that connects with points-payments"

      # link: pantrylitics
    - name : "Jobs"
      image : /icons/jobs.png
      content : "A job application platform for your company"
      # link: jobs
      footer: "<span class='text-secondary'>Being developed</span>"

    - name : "Klock"
      image : /icons/clock.png
      content : "An online bundy clock for your employees"
      footer: "<span class='text-danger'>To be developed</span>"

    - name : "Rewards"
      image : /icons/gift.png
      content : "A rewards platform for your customers for their frequent purchases"
      link: /rewards
      footer: "<span class='text-success'>Deployed</span>"      

    # - name : "Sched"
    #   image : /icons/cal.png
    #   content : "A booking system for your customers"
    #   footer: "<span class='text-danger'>To be developed</span>"
      # link: pantrylitics

    - name : "Query"
      image : /icons/chat.png
      content : "A basic system for handling customer queries and feedback"
      footer: "<span class='text-secondary'>Deployed</span>"
      # link: pantrylitics


######################### Service #####################

how:
  title : "How it Works"
  steps:
    - image: "/screens/elivirs.jpg"
      id: 1
      content: "We make a website for your company where you can update the details by yourself via Github. The website allows customers to enter their queries"  
      # content: "Post jobs on Pantry Hub and let applicants apply through your Pantrypreneur Job Module"  
    # - image: "/screens/po1.jpg"
    #   id: 2
    #   content: "Upon payment, tell the seller your Pantry ID"  
    - image: "/screens/appraise.png"
      id: 2
      content: "Your website connects to a web app where you can view the customer queries or orders. The web app allows you to record staff attendance using a Klock Module, as well as to appraise them through an Appraise Module"
    - image: "/screens/rewards/rewards.jpg"
      id: 3
      content: "Reward your customers for their purchases through the Rewards Module"

    #- title : ""
    #  images:
    #  - "https://sorasystem.sirv.com/cards/receipt.jpeg"
    #  content : "."
    #  quote: "Where the riches are engrossed by a few, these must contribute very largely to the supplying of the public necessities. But when the riches are dispersed among multitudes, the burden feels light on every shoulder.<br> <cite>- David Hume</cite>"
    #  quota: "/avatars/hume.png"         
      # button:
      #  enable : true
      #  label : "What's Basic Universal Revenue?"
      #  link : "https://superphysics.org/social/economicswhat-is-basic-universal-revenue/"


    # - title : "Fairtax: Double-entry ends tax evasion and leakage"
    #   images:
    #   - "https://sorasystem.sirv.com/cards/fairtaxreceipt.jpg"
    #   content : "Pantry Fairtax or shared direct taxation is a double entry tax system where each tax is split between the buyer and the seller. The seller's payments for each transaction must match the buyers"
    #   # will use a double-entry system on the three tax classes where the interest of the buyers, renters, and employees checks the interest of the sellers, landlords, and employers, and the public interest of the government checks the combined interest of the two parties, who then check the government through elections. The simplicity of the system empowers everyone to check each other!
    #   quote: "The ancient Tenths and Fifteenths were taxes so usual in England. They were taxes of the same kind as the taille.<br> <cite>- Adam Smith</cite>"
    #   quota: "/avatars/smith.png"         
      #button:
      #  enable : true
      #  label : "What's Basic Universal Revenue?"
      #  link : "https://superphysics.org/social/economicswhat-is-basic-universal-revenue/" 



circles1:
  title : "Current Implementations"
  subtitle: "Pantrypreneur is currently being tested by the following"
  cards:
    - name : Bambang PWD Association
      image : /icons/bambang.jpg
      content : "Bambang PWD Association is testing Pantrypreneur jobs to get jobs for disabled people"
      # link: world
      footer: "<span class='has-text-dark'>Pasig</span>"
    - name : AMSAI E-Learning
      image : /logos/amsai.png
      content : "AMSAI E-Learning is testing appraise as a student grading system"
      # link: world
      footer: "<span class='has-text-dark'>Laguna</span>"
    # - name : Matayog
    #   image : /icons/matayog.png
    #   content : "Matayog is a yoga center that is testing Pantrypreneur Sched to book yoga classes"
      # link: pantrylitics
      footer: "<span class='has-text-dark'>Paranaque</span>"      
    - name : Elivir's Restaurant
      image : /icons/elivirs.png
      content : "Elivir's Restaurant is testing Pantrypreneur Rewards as a Loyalty system"
      # link: pantrylitics
      footer: "<span class='has-text-dark'>Quezon City</span>"      
    - name : Ayus
      image : /logos/ayus.png
      content : "Ayus is a homeopathy practice that is testing Query for customer queries"
      # ISAIAH Health for predicting disease probability
      footer: "<span class='has-text-dark'>Las Pinas</span>"

req:
  title: Based on the requirements in Chapter 11 of The Economist by Xenophon
  link: "https://superphysics.org/research/socrates/the-economist/chapter-11"
  btext: "Read the requirements"
  avatar: "/avatars/socrates.png"


##################### Call to action #####################

cta:
  title : "Register in the Waitlist"
  link1text: "Sure!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "I'm not convinced yet"
  link2 : "https://superphysics.org/"

---
