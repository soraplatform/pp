---
title: "Your Access to the Metaverse"
subtitle: "Point Coins connects Pantrypoints to the Metaverse"
logo: "/logos/coins.png"
image: "/og/coins.jpg"
description: "Point Coins connect Pantrypoints to the Metaverse and facilitates Pantrypoints World"
youtube: "wKR3-nLxec8"
# youtube: "J6F2_PF2wbo"
# youtube: "oJXjxLYQQ08"
applink: https://hub.pantrypoints.com/signup
apptext: "Register in the Waitlist"
aliases:
  /coins


# features:
personas:
  image: "/photos/taxes.jpg"
  title : "'Coin-easing' instead of Quantitative Easing"
  subtitle: "Point Coins will allow Coin-easing via Pantry Nation and Pool Clearing via Pantry World"
  items:
    - name : "Coin-easing"
      icon: /icons/eth.png
      # icon : "<svg fill='dimgray' height='50px' viewBox='0 0 320 512'><path d='M311.9 260.8L160 353.6 8 260.8 160 0l151.9 260.8zM160 383.4L8 290.6 160 512l152-221.4-152 92.8z'/></svg>"
      content : "Provide fiat liquidity to entrepreneurs to keep the economy going"      

    - name : "Nano-lending"
      #hand paper
      icon: /icons/swap.png
      # icon : "<svg viewBox='0 0 576 512' fill='dimgray' height='50px'><path d='M96,128A64,64,0,1,0,32,64,64,64,0,0,0,96,128Zm0,176.08a44.11,44.11,0,0,1,13.64-32L181.77,204c1.65-1.55,3.77-2.31,5.61-3.57A63.91,63.91,0,0,0,128,160H64A64,64,0,0,0,0,224v96a32,32,0,0,0,32,32V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V383.61l-50.36-47.53A44.08,44.08,0,0,1,96,304.08ZM480,128a64,64,0,1,0-64-64A64,64,0,0,0,480,128Zm32,32H448a63.91,63.91,0,0,0-59.38,40.42c1.84,1.27,4,2,5.62,3.59l72.12,68.06a44.37,44.37,0,0,1,0,64L416,383.62V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V352a32,32,0,0,0,32-32V224A64,64,0,0,0,512,160ZM444.4,295.34l-72.12-68.06A12,12,0,0,0,352,236v36H224V236a12,12,0,0,0-20.28-8.73L131.6,295.34a12.4,12.4,0,0,0,0,17.47l72.12,68.07A12,12,0,0,0,224,372.14V336H352v36.14a12,12,0,0,0,20.28,8.74l72.12-68.07A12.4,12.4,0,0,0,444.4,295.34Z'/></svg>"
      content : "Allow tiny lending in crypto for Clearing Funds"
      


req:
  title: Based on the requirements of EF Schumacher
  link: "https://superphysics.org/research/schumacher/pool-clearing/part-1"
  btext: "Read the requirements"
  avatar: "/avatars/schu.png"  


segment:
  articles:
    - title : "Gets rids of stagflation"
      image: "/photos/code.jpg"
      # - "https://ik.imagekit.io/sora/charts/isaiah_1rlCw9Hzb.png"
      content: "Coin-easing pumps fiat directly into productive entrepreneurs so that they can scale and produce more goods and services for society. This is different from Quantitative Easing which pumps money into banks, and Universal Basic Income which pumps money into unproductive people."
      # quote: "'Desire is the actual essence of man, in so far as it is conceived, as determined to a particular activity by some given modification of itself.' <cite>Spinoza, The Ethics, Part 3</cite>"
      # quota: "/avatars/spinoza.jpg"
      # button:
      #   enable : true
      #   label : "What is ISAIAH?"
      #   link : "https://superphysics.org/superphysics/solutions/isaiah"


# feedback:
#   title: "Competitions"
#   items:
#     - user : "Urban Land Institute Philippines"
#       image: "/icons/uli200.jpg"
#       content: "Top 5 ULI Urban Innovation Challenge 2019"
#       link: "http://philippines.uli.org"
#     - user : "UBX"
#       image: "/icons/uhack200.jpg"
#       content: "Top 10 U:Hack 2019" 
#       link: "http://ubx.ph"


# feedback2:
#   title: "Feedback"
#   items:
#     - user : "seditionmusic.com"
#       image: "/icons/sedition.png"
#       content: "'This is a very interesting concept. One thing you cannot neglect is the legal system. For this to become internationally adopted, I believe you're going to have to propose a movement towards a universal law system (which I support)'"
#       # link: "https://www.seditionmusic.com"
#     - user : "mylivewellhome.com"
#       image: "/icons/livewell.png"
#       content: "'I love the idea of streamlining and improving governmental processes.'" 
#       # link: "https://mylivewellhome.com/"


##################### Call to action #####################

cta:
  title : "We need Coins"
  link1text: "Send an inquiry"
  link1 : "/contact"
  link2text: "I need to know more"
  link2 : "https://superphysics.org/research/schumacher/pool-clearing/part-1"

---
