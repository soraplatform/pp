---
title : "Transitioning the world to a points-based economic system"
#subtitle: We build the online presence
description: "We offer the following services to our partners"
date: 2020-10-06T08:47:36+00:00
lastmod: 2023-03-27T08:47:36+00:00
youtube: "lRZbEcKMqpc"
banner: "/photos/team.jpg"
logo: "/logos/services.png"


### COUNTRIES ###


shots:
  # bannerfeat: "https://res.cloudinary.com/nara/image/upload/v1567936990/photos/incacroplowres.jpg" 
  title : "Our Services"
  # subtitle: "Put your business online without the hassle"
  cards:
    - name : "Build"
      image : "/icons/b.png"
      content : "We build the online presence for businesses and organizations"

    - name : "Coins"
      image : "/icons/bank.png"
      content : "Coins will facilitate cross-border points transactions"
      
    - name : "Match"
      image : "/icons/match.png"
      content : "We match users with the proper career or potential disease"

    - name : "Analytics"
      image : "/icons/pantrylitics.png"
      content : "See the points dynamics of your business or city"

    - name : "Predict"
      image : "/icons/ip.png"
      content : "Get predictions on what might happen to your country or city"

    - name : "Points-Taxation"
      image : "/icons/t.png"
      content : "Get a source of non monetary revenue for your city"


##################### Call to action #####################

cta:
  title : "Register in the Waitlist"
  link1text: "Sure!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "I need to know more"
  link2 : "https://superphysics.org/social/economics/fallacies/equilibrium-fallacy"

---
