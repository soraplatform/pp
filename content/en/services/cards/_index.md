---
title: "'Bardit' Cards for Offline Use"
subtitle: Bardit cards combine barter and credit without interest rates 
logo: "/logos/cards.png"
image: "/og/pantrypoints.jpg"
description: "Pantrypoints Cards are the offline bardit system that combines barter and credit"
# youtube: "OV1JHTiDdkQ"
youtube: ZErxZt4V2t4
applink: /contact
apptext: "Send us an inquiry"
aliases:
  /cards


# features:
personas:
  image: "/photos/holdhands.jpg" 
  # image: "https://res.cloudinary.com/nara/image/upload/v1567936990/photos/incacroplowres.jpg" 
  title: "Bardit = Barter + Credit"
  subtitle: "Bardit allows any 2 parties to create value"
  # subtitle: "The Inca created a moneyless system that was able to sustain their empire"
  items:
    - name : "Moneyless"
      icon: /icons/moneyno.png
      # icon : "<svg  viewBox='0 0 288 512' height='50px' fill='dimgray'><path d='M209.2 233.4l-108-31.6C88.7 198.2 80 186.5 80 173.5c0-16.3 13.2-29.5 29.5-29.5h66.3c12.2 0 24.2 3.7 34.2 10.5 6.1 4.1 14.3 3.1 19.5-2l34.8-34c7.1-6.9 6.1-18.4-1.8-24.5C238 74.8 207.4 64.1 176 64V16c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v48h-2.5C45.8 64-5.4 118.7.5 183.6c4.2 46.1 39.4 83.6 83.8 96.6l102.5 30c12.5 3.7 21.2 15.3 21.2 28.3 0 16.3-13.2 29.5-29.5 29.5h-66.3C100 368 88 364.3 78 357.5c-6.1-4.1-14.3-3.1-19.5 2l-34.8 34c-7.1 6.9-6.1 18.4 1.8 24.5 24.5 19.2 55.1 29.9 86.5 30v48c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16v-48.2c46.6-.9 90.3-28.6 105.7-72.7 21.5-61.6-14.6-124.8-72.5-141.7z'/></svg>"
      content : "Money is not essential nor critical to Points Cards"

    - name : "Decentralized"
      icon: /icons/decen.png    
      # icon : "<svg  viewBox='0 0 480 512' height='50px' fill='dimgray'><path d='M471.99 334.43L336.06 256l135.93-78.43c7.66-4.42 10.28-14.2 5.86-21.86l-32.02-55.43c-4.42-7.65-14.21-10.28-21.87-5.86l-135.93 78.43V16c0-8.84-7.17-16-16.01-16h-64.04c-8.84 0-16.01 7.16-16.01 16v156.86L56.04 94.43c-7.66-4.42-17.45-1.79-21.87 5.86L2.15 155.71c-4.42 7.65-1.8 17.44 5.86 21.86L143.94 256 8.01 334.43c-7.66 4.42-10.28 14.21-5.86 21.86l32.02 55.43c4.42 7.65 14.21 10.27 21.87 5.86l135.93-78.43V496c0 8.84 7.17 16 16.01 16h64.04c8.84 0 16.01-7.16 16.01-16V339.14l135.93 78.43c7.66 4.42 17.45 1.8 21.87-5.86l32.02-55.43c4.42-7.65 1.8-17.43-5.86-21.85z'/></svg>"
      content : "Parties dictate their own prices relative to rice or grains"
      
    - name : "Flexible"
      icon: /icons/swap.png
      # icon : "<svg  viewBox='0 0 640 512' height='50px' fill='dimgray'><path d='M368 32h-96c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h96c17.67 0 32-14.33 32-32V64c0-17.67-14.33-32-32-32zM208 88h-84.75C113.75 64.56 90.84 48 64 48 28.66 48 0 76.65 0 112s28.66 64 64 64c26.84 0 49.75-16.56 59.25-40h79.73c-55.37 32.52-95.86 87.32-109.54 152h49.4c11.3-41.61 36.77-77.21 71.04-101.56-3.7-8.08-5.88-16.99-5.88-26.44V88zm-48 232H64c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h96c17.67 0 32-14.33 32-32v-96c0-17.67-14.33-32-32-32zM576 48c-26.84 0-49.75 16.56-59.25 40H432v72c0 9.45-2.19 18.36-5.88 26.44 34.27 24.35 59.74 59.95 71.04 101.56h49.4c-13.68-64.68-54.17-119.48-109.54-152h79.73c9.5 23.44 32.41 40 59.25 40 35.34 0 64-28.65 64-64s-28.66-64-64-64zm0 272h-96c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h96c17.67 0 32-14.33 32-32v-96c0-17.67-14.33-32-32-32z'/></svg>"
      content : "Parties can pay in goods, services, or money"

    - name : "Natural KYC"
      icon: /icons/jobs.png
      # icon : "<svg fill='dimgray' height='50px'  viewBox='0 0 640 512'><path d='M224 256A128 128 0 1 0 96 128a128 128 0 0 0 128 128zm96 64a63.08 63.08 0 0 1 8.1-30.5c-4.8-.5-9.5-1.5-14.5-1.5h-16.7a174.08 174.08 0 0 1-145.8 0h-16.7A134.43 134.43 0 0 0 0 422.4V464a48 48 0 0 0 48 48h280.9a63.54 63.54 0 0 1-8.9-32zm288-32h-32v-80a80 80 0 0 0-160 0v80h-32a32 32 0 0 0-32 32v160a32 32 0 0 0 32 32h224a32 32 0 0 0 32-32V320a32 32 0 0 0-32-32zM496 432a32 32 0 1 1 32-32 32 32 0 0 1-32 32zm32-144h-64v-80a32 32 0 0 1 64 0z'/></svg>"
      content : "Knowing your customer is done through natural meetups instead of through artificial technology"

    # - name : "Taxation"
    #   icon : "<svg fill='dimgray' height='50px'  viewBox='0 0 640 512'><path d='M224 256A128 128 0 1 0 96 128a128 128 0 0 0 128 128zm96 64a63.08 63.08 0 0 1 8.1-30.5c-4.8-.5-9.5-1.5-14.5-1.5h-16.7a174.08 174.08 0 0 1-145.8 0h-16.7A134.43 134.43 0 0 0 0 422.4V464a48 48 0 0 0 48 48h280.9a63.54 63.54 0 0 1-8.9-32zm288-32h-32v-80a80 80 0 0 0-160 0v80h-32a32 32 0 0 0-32 32v160a32 32 0 0 0 32 32h224a32 32 0 0 0 32-32V320a32 32 0 0 0-32-32zM496 432a32 32 0 1 1 32-32 32 32 0 0 1-32 32zm32-144h-64v-80a32 32 0 0 1 64 0z'/></svg>"
    #   content : "Knowing your customer is done through natural meetups instead of through artificial technology"

  # = image_tag 'https://sorasystem.sirv.com/photos/quipu.jpg', class: 'img-fluid rounded'


######################### How #####################

how:
  title : "How it Works"  
  steps:
    # - image: "https://sorasystem.sirv.com/photos/pantry.jpg"
    - id: 1
      content: "Get 2 points cards and fill them with the details of the other person"  
      image: "/graphics/card1.jpg"
    - id: 2 
      image: "/graphics/card2.jpg"
      content: "Meet up with that person and fill up the details of both cards with the actual transaction"
    - id: 3
      image: "/graphics/card3.jpg"
      content: "Give the other card to the other person as his proof of the transaction. Repeat the process in a future transaction to clear the barter debt"


req:
  title: Based on the requirements in the Wealth of Nations
  # Book 2, Chapter 2 of
  link: "https://superphysics.org/research/smith/wealth-of-nations/book-5/chapter-3j/"
  btext: "Read the requirements"
  avatar: "/avatars/smith.png"



# feedback:
#   title: Awards
#   items:
#     - user : Startup Weekend Cambodia 2017
#       image: https://sorasystem.sirv.com/logos/startupweekend.png
#       content: Top 5 in the Fintech Edition as Debt Clearing System
#       link: http://communities.techstars.com

#     - user : Infinity Blockchain Labs Nationwide Vietnam Blockathon 2017
#       image: https://sorasystem.sirv.com/logos/blockathon.jpg
#       content: 2nd Place as Social ROSCA
#       link: http://blockchainlabs.asia


##################### Call to action #####################

cta:
  title : "Order Point Cards"
  link1text: "Sure!"
  link1 : "/contact"
  link2text: "I need to know more"
  link2 : "https://superphysics.org/social/economics/solutions/points-banking"

---
