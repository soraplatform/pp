---
title: "Know your nation"
subtitle: "ISAIAH Predict is the social prediction tool of the Pantrypoints System that uses Supersociology"
logo: "/logos/predict.png"
image: "/og/predict.jpg"
description: "ISAIAH Predict is the social prediction tool of Pantrypoints System that uses Supersociology"
youtube: "5vmCe8w3UEg"
applink: "https://hub.pantrypoints.com"
apptext: "Try the Prototype"
aliases:
  /predict


# features:
personas:
  title : "Data-Driven Predictions"
  sub: "ISAIAH Predict uses historical data to predic outcomes in society"
  image: "/photos/freelance.jpg"  
  items:
    - name : "Elections"
      icon: /icons/appraise.png
      # icon : "<svg height='60px' fill='gray' height='50px' viewBox='0 0 448 512'><path d='M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm95.8 32.6L272 480l-32-136 32-56h-96l32 56-32 136-47.8-191.4C56.9 292 0 350.3 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-72.1-56.9-130.4-128.2-133.8z'/></svg>"
      content : "Know which candidates might win in advance"
      
    - name : "Economics"
      icon: /icons/shop.png
      # icon : "<svg  fill='gray' height='50px' viewBox='0 0 512 512'><path d='M496 384H64V80c0-8.84-7.16-16-16-16H16C7.16 64 0 71.16 0 80v336c0 17.67 14.33 32 32 32h464c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16zM464 96H345.94c-21.38 0-32.09 25.85-16.97 40.97l32.4 32.4L288 242.75l-73.37-73.37c-12.5-12.5-32.76-12.5-45.25 0l-68.69 68.69c-6.25 6.25-6.25 16.38 0 22.63l22.62 22.62c6.25 6.25 16.38 6.25 22.63 0L192 237.25l73.37 73.37c12.5 12.5 32.76 12.5 45.25 0l96-96 32.4 32.4c15.12 15.12 40.97 4.41 40.97-16.97V112c.01-8.84-7.15-16-15.99-16z'/></svg>"
      content : "Are you sure your business will be right match for the coming economy?"
      
    - name : "Social Phenomena and Trends"
      icon: /icons/jobless.png
      # icon : "<svg  fill='gray' height='50px' viewBox='0 0 512 512'><path d='M500 384c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12H12c-6.6 0-12-5.4-12-12V76c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v308h436zM372.7 159.5L288 216l-85.3-113.7c-5.1-6.8-15.5-6.3-19.9 1L96 248v104h384l-89.9-187.8c-3.2-6.5-11.4-8.7-17.4-4.7z'/></svg>"
      content : "Are you living in a country or city that is best for you?"
      
    - name : "Criminal Tendencies (with ISAIAH Match)"
      icon: /icons/thief.png    
      # icon : "<svg fill='gray' height='50px'   viewBox='0 0 448 512'><path d='M325.4 289.2L224 390.6 122.6 289.2C54 295.3 0 352.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-70.2-54-127.1-122.6-133.2zM32 192c27.3 0 51.8-11.5 69.2-29.7 15.1 53.9 64 93.7 122.8 93.7 70.7 0 128-57.3 128-128S294.7 0 224 0c-50.4 0-93.6 29.4-114.5 71.8C92.1 47.8 64 32 32 32c0 33.4 17.1 62.8 43.1 80-26 17.2-43.1 46.6-43.1 80zm144-96h96c17.7 0 32 14.3 32 32H144c0-17.7 14.3-32 32-32z'/></svg>"
      content : "Prevent crime by pre-emptive monitoring"
      

######################### Service #####################


how:
  title: "Ask about the future"
  # title : "Don't be caught off-guard again"  
  steps:
    # - image: "https://sorasystem.sirv.com/photos/pantry.jpg"
    # - image: "/graphics/biz.jpg"
    - id: 1
      image: "/screens/predict/1.jpg"
      content: Ask about an event
      # content: "Ask us a question about your society or business intention"  
    - id: 2
      image: "/screens/predict/2.jpg"
      content: "We'll run your question through our model to come up with an answer"
    - id: 3
      image: "/screens/predict/2.jpg"
      content: "We'll run your question through our model to come up with an answer"      
    # - id: 3
    #   image: "/graphics/predictstaff.png"
    #   content: "Add more questions for other aspects of your business or field of interest"



segment:
  articles:
    - title : "Clearing Fund Predictions"
      image: "/screens/fundsui.png"
      content : "ISAIAH Predict is essential in predicting commodity prices in order to allocate the proper interest rates for clearing funds for respective commodities" 
      button:
        label: "What are Clearing Funds?"
        link : "https://www.superphysics.org/research/schumacher/pool-clearing/part-3"


# feedback:
#   title: "Feedback"
#   enabled: false
#   item:
#     - user : "captaindex (Pioneer tournament), April 2020"
#       image: "/avatars/blankavatar.png"
#       content: "Really good thing! I, too, think personality is an important factor in many fields"
#       # link: "https://intelli-network.com"
#     - user : "brainsprays"
#       image: "/icons/brainsprays.png"
#       content: "I'm a long time Myers Briggs (ENTJ) and Enneagram (9) fan so love this stuff" 
#       link: "https://brainsprays.com"

req:
  title: Based on the requirements in The Republic
  link: "https://superphysics.org/research/socrates/simple-republic/book-8/chapter-1/"
  btext: "Read the requirements"
  avatar: "/avatars/socrates.png"


##################### Call to action #####################

cta:
  enable : true
  title : "Send us a Predict-question to get started"
  subtitle: "If we don't reply it means your message got buried in the spam, so please send it again or multiple times"  
  # title : "Do you want to use Predict?"
  link1text: "Ok!"
  link1 : "/contact"
  link2text: "I'm not convinced yet"
  link2 : "https://superphysics.org/social/supersociology/precrisis-years/"

---
