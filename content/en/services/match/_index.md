---
title: "Find the right match. Or just avoid the bad ones"
subtitle: "ISAIAH Match is our personality matching system for education, jobs, products, fake news, and relationships, as a part of a new morality-based socio-economic system"
logo: "/logos/match.png"
image: "/og/match.jpg"
description: "ISAIAH Match is our personality matching system for education, jobs, products, fake news, and relationships, as a part of a new morality-based socio-economic system"
youtube: 5alMsLDgc00
# youtube: "YjpyR0yVNLY"
# youtube: "oBFBDtAmUvk"
applink: https://hub.pantrypoints.com/signup
apptext: "Register in the Waitlist"
aliases:
  /match

  
personas:
# features:
  image: /photos/shake.jpg
  title : "Matchmaking isn't just for relationships"
  subtitle: "ISAIAH's 'skills' can help solve chronic problems in society that are unsolved by science"
  items:
    - name : "Employers"
      #usertie
      icon: /icons/banker.png   
      # icon : "<svg  viewBox='0 0 448 512' height='50px' fill='blue'><path d='M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm95.8 32.6L272 480l-32-136 32-56h-96l32 56-32 136-47.8-191.4C56.9 292 0 350.3 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-72.1-56.9-130.4-128.2-133.8z'/></svg>"
      content : "Have you ever hired the wrong employee? Getting the wrong person for the job could be very costly"
      
    - name : "Education"
      icon: /icons/teacher.png    
      # icon : "<svg  viewBox='0 0 640 512' height='50px' fill='purple'><path d='M622.34 153.2L343.4 67.5c-15.2-4.67-31.6-4.67-46.79 0L17.66 153.2c-23.54 7.23-23.54 38.36 0 45.59l48.63 14.94c-10.67 13.19-17.23 29.28-17.88 46.9C38.78 266.15 32 276.11 32 288c0 10.78 5.68 19.85 13.86 25.65L20.33 428.53C18.11 438.52 25.71 448 35.94 448h56.11c10.24 0 17.84-9.48 15.62-19.47L82.14 313.65C90.32 307.85 96 298.78 96 288c0-11.57-6.47-21.25-15.66-26.87.76-15.02 8.44-28.3 20.69-36.72L296.6 284.5c9.06 2.78 26.44 6.25 46.79 0l278.95-85.7c23.55-7.24 23.55-38.36 0-45.6zM352.79 315.09c-28.53 8.76-52.84 3.92-65.59 0l-145.02-44.55L128 384c0 35.35 85.96 64 192 64s192-28.65 192-64l-14.18-113.47-145.03 44.56z'/></svg>"
      content : "Are you sure you're studying the course that's best for you? Get it right with Match"
      
    # - name : "Products"
    #   icon : "<svg  viewBox='0 0 640 512' height='50px' fill='dimgray'><path d='M192 384h192c53 0 96-43 96-96h32c70.6 0 128-57.4 128-128S582.6 32 512 32H120c-13.3 0-24 10.7-24 24v232c0 53 43 96 96 96zM512 96c35.3 0 64 28.7 64 64s-28.7 64-64 64h-32V96h32zm47.7 384H48.3c-47.6 0-61-64-36-64h583.3c25 0 11.8 64-35.9 64z'/></svg>"
    #   content : "Never launch a product again without being sure whether there are buyers in that area"
    
    # - name : "Politics"
    #   icon : "<svg  viewBox='0 0 512 512' height='50px' fill='dimgray'><path d='M504.971 199.362l-22.627-22.627c-9.373-9.373-24.569-9.373-33.941 0l-5.657 5.657L329.608 69.255l5.657-5.657c9.373-9.373 9.373-24.569 0-33.941L312.638 7.029c-9.373-9.373-24.569-9.373-33.941 0L154.246 131.48c-9.373 9.373-9.373 24.569 0 33.941l22.627 22.627c9.373 9.373 24.569 9.373 33.941 0l5.657-5.657 39.598 39.598-81.04 81.04-5.657-5.657c-12.497-12.497-32.758-12.497-45.255 0L9.373 412.118c-12.497 12.497-12.497 32.758 0 45.255l45.255 45.255c12.497 12.497 32.758 12.497 45.255 0l114.745-114.745c12.497-12.497 12.497-32.758 0-45.255l-5.657-5.657 81.04-81.04 39.598 39.598-5.657 5.657c-9.373 9.373-9.373 24.569 0 33.941l22.627 22.627c9.373 9.373 24.569 9.373 33.941 0l124.451-124.451c9.372-9.372 9.372-24.568 0-33.941z'/></svg>"
    #   content : "Is your city full of liberals or conservatives? Find the city that supports your ideology"

    - name : "Health Risk"
      icon: /icons/meds.png
      # icon : "<svg  viewBox='0 0 576 512' height='50px' fill='pink'><path d='M112 32C50.1 32 0 82.1 0 144v224c0 61.9 50.1 112 112 112s112-50.1 112-112V144c0-61.9-50.1-112-112-112zm48 224H64V144c0-26.5 21.5-48 48-48s48 21.5 48 48v112zm139.7-29.7c-3.5-3.5-9.4-3.1-12.3.8-45.3 62.5-40.4 150.1 15.9 206.4 56.3 56.3 143.9 61.2 206.4 15.9 4-2.9 4.3-8.8.8-12.3L299.7 226.3zm229.8-19c-56.3-56.3-143.9-61.2-206.4-15.9-4 2.9-4.3 8.8-.8 12.3l210.8 210.8c3.5 3.5 9.4 3.1 12.3-.8 45.3-62.6 40.5-150.1-15.9-206.4z'/></svg>"
      content : "Is your current personality matching you with a future diabetes at 40? Know your future health risks before you end up with them"

    - name : "Marriage"
      icon: /icons/banking.png
      # icon : "<svg fill='red' class='p-2' height='50px' viewBox='0 0 512 512'><path d='M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z'/></svg>"
      content : "We automate the traditional matchmaker with machine learning"

    - name : "Criminality"
      icon: /icons/thief.png
      # icon : "<svg fill='gray' class='p-2' height='50px' viewBox='0 0 448 512'><path d='M325.4 289.2L224 390.6 122.6 289.2C54 295.3 0 352.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-70.2-54-127.1-122.6-133.2zM32 192c27.3 0 51.8-11.5 69.2-29.7 15.1 53.9 64 93.7 122.8 93.7 70.7 0 128-57.3 128-128S294.7 0 224 0c-50.4 0-93.6 29.4-114.5 71.8C92.1 47.8 64 32 32 32c0 33.4 17.1 62.8 43.1 80-26 17.2-43.1 46.6-43.1 80zm144-96h96c17.7 0 32 14.3 32 32H144c0-17.7 14.3-32 32-32z'/></svg>"
      content: "Reduce the chance of criminal behavior in your event or organization"
      # content : "Is your city full of liberals or conservatives? Find the city that supports your ideology"


platform:
  title : "Match allows robust, granular, yet efficient personality profiling that is physically verifiable"
  item:
    - name : "Myers-Briggs"
      image : "/icons/chartmyers.jpg"
      content : "Myers-Briggs has 4 dimensions"

    - name : "Match"
      image : "/icons/chartmatch.png"
      content : "Match has 10 -- The job applicant answers fewer questions than Myers-Briggs, but reveals much more information"

    - name : "IBM Personality Insights"
      image : "https://ik.imagekit.io/sora/charts/ibmpersonality_aaYPK09FT.jpg"
      content : "IBM Personality Insights has too many"


######################### Service #####################

how:
  title : "How it Works"
  steps:
    - image: "/screens/match/1.jpg"
      id: 1
      content: "Answer 20 questions on the mobile app, indicating your city"  
    - image: "/screens/match/2.jpg"
      id: 2
      content: "Know your classification"
    - image: "https://ik.imagekit.io/sora/ui/sydneyskills_4gRvQ48Ss.png"
      id: 3
      content: "Browse or post job opportunities in your city, according to your classification"


segment:
  articles:
    - title : "Powered by ISAIAH. Based on Spinoza."
      image: "/photos/code.jpg"
      # - "https://ik.imagekit.io/sora/charts/isaiah_1rlCw9Hzb.png"
      content: "Our mobile and web apps work together with machine learning to create ISAIAH. Match is one of the 'skills' of ISAIAH. If our proposed social system is based on Socrates, our governence system is based on David Hume, and our economic system is based on Adam Smith, our proposed personality system is based on Spinoza's Ethics which proposes predestination that checks liberalism.<br> A hamburger lover would think that he is eating hamburgers out of his free will. But metaphysically, his love for burgers is a <a href='https://en.wikipedia.org/wiki/Bandha_(Jainism)'>predetermined bondage</a> arising from a quality of his soul, which can then be revealed by data. This then would support the belief that we exist inside a matrix that the Hindus call the <a href='https://en.wikipedia.org/wiki/Brahma'>Brahma</a>"
      quote: "'Desire is the actual essence of man, in so far as it is conceived, as determined to a particular activity by some given modification of itself.' <cite>Spinoza, The Ethics, Part 3</cite>"
      quota: "/avatars/spinoza.jpg"
      button:
        enable : true
        label : "What is ISAIAH?"
        link : "https://superphysics.org/superphysics/solutions/isaiah"



feedback:
  title: "Feedback"
  items:
    - user : "whoelse.ai"
      image: "/icons/whoelse.png"
      content: "Amazing progress! I really liked how you turned a theory into a product offering. Keep up the good work!"
      link: "https://whoelse.ai"
    # - user : "captaindex (Pioneer tournament), April 2020"
    #   image: "/avatars/blankavatar.png"
    #   content: "Really good thing! I, too, think personality is an important factor in many fields"
      # link: "https://intelli-network.com"
    - user : "brainsprays.com"
      image: "/icons/brainsprays.png"
      content: "I'm a long time Myers Briggs (ENTJ) and Enneagram (9) fan so love this stuff" 
      link: "https://brainsprays.com"





##################### Call to action #####################

cta:
  title : "Register in the Waitlist"
  link1text: "Sure!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "I need to know more"
  link2 : "https://superphysics.org/social/supersociology/principles/part-1/chapter-01/"

---
