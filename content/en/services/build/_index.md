---
title: Build the Online Presence for Your Business 
# We build the online presence for your business
subtitle: We build the online presence -- website, web app, social media, SEO -- for your businesses with minimal money-cost, or even barter
# Pantry Peers is a small dedicated team that builds the Pantry system for Pantrynomics
# subtitle: Pantry Peers is a small dedicated team creating an online economy for micro and small businesses 
logo: "/logos/build.png"
image: "/photos/team.jpg"
description: We build the online presence -- website, web app, social media, SEO -- for your businesses with minimal money-cost
# "Pantry Peers is a small dedicated team that enables offline micro and small businesses to enter the digital age"
# youtube: "OV1JHTiDdkQ"
# "BXuA4483muo"
# youtube: "3FHG_VoDlhs"
# youtube: "DjGsLaUvYcc"
# youtube: lRZbEcKMqpc
# youtube: "xT37nH1w-xU"
youtube: zIrblIJF4Nk
# applink: "https://elivirs.com"
# apptext: "Check out a real project"
aliases:
  /build


shots:
  # bannerfeat: "https://res.cloudinary.com/nara/image/upload/v1567936990/photos/incacroplowres.jpg" 
  title : "Our Build Package"
  subtitle: "Put your business online without the hassle"
  cards:
    - name : "Basic graphics and logos"
      image : "/icons/harrys.png"
      content : "We build basic graphics using Canva and other online tools"

    - name : "Website, Domain, basic content"
      image : "/screens/harrysweb.jpg"
      content : "We build your site based on our templates and add your own original content, optimized for search engines"
      
    - name : "Social Network and Youtube"
      image : "/screens/harrys.jpg"
      content : "We build your Facebook Page and Youtube Channel if you haven't done them yet"

    - name : "Analytics"
      image : "/screens/ganalytics2.jpg"
      content : "We set up a Google Analytics and Search Console for you"


######################### How #####################

how:
  title : "Start Building the Digital Presence of Your Business"  
  steps:
    # - image: "https://sorasystem.sirv.com/photos/pantry.jpg"
    - image: "/graphics/biz.jpg"
      id: 1
      content: "Tell us your business idea and your target market, content, etc"  
    - image: "/screens/harrysweb.jpg"
      id: 2
      content: "We build the online presence of your business. If it gains traction within a year, then we hand it over to your control. If it fails, then we either pivot or abandon it just like a startup. In this way, your startup costs will be much lower"
    - image: "/screens/ui.jpg"
      id: 3
      content: "You can then tap us to add a web app, mobile app, or cashless payment in the future, or even try our moneyless system (this last possibility is our <a href='/docs/supereconomics/economy-as-a-service/'>ultimate goal</a>)"


# req:
#   title: Based on the requirements in Book 2, Chapter 2 of the Wealth of Nations
#   link: "https://superphysics.org/research/smith/wealth-of-nations/book-2/chapter-3c"
#   btext: "Read the requirements"
#   avatar: "/avatars/smith.png"



# feedback:
#   title: Awards
#   item:
#     - user : NASA Space Apps Challenge 2018
#       image: /icons/nasa.jpg
#       content: Most Inspiring Award as Disaster Relief App
#       # link: http://communities.techstars.com

#     - user : DISH Hackathon 2018
#       image: /logos/dish.jpg
#       content: Special Prize for SORA Galleon
#       # link: http://blockchainlabs.asia

#     - user : Startup Weekend Cambodia 2017
#       image: https://sorasystem.sirv.com/logos/startupweekend.png
#       content: Top 5 in the Fintech Edition as Debt Clearing System
#       link: http://communities.techstars.com

#     - user : Infinity Blockchain Labs Nationwide Vietnam Blockathon 2017
#       image: https://sorasystem.sirv.com/logos/blockathon.jpg
#       content: 2nd Place as Social ROSCA
#       link: http://blockchainlabs.asia

#     - user : AWS Hackdays 2020
#       image: /icons/aws.jpg
#       content: Top 5 for SORA Health
#       # link: http://communities.techstars.com

#     - user : AWS Hackdays 2019
#       image: /icons/aws.jpg
#       content: Top 5 for SORA Relief

#     - user : SLUSH Vietnam 2017
#       image: /covers/slush.jpg
#       content: Semifinalist
#       # link: http://blockchainlabs.asia
                

##################### Call to action #####################

cta:
  title : "Send us a message to get started"
  subtitle: "If we don't reply it means your message got buried in the spam, so please send it again or multiple times"
  link1text: "Sure!"
  link1 : "/contact"
  link2text: "I'm looking for something else"
  link2 : "https://google.com"

---
