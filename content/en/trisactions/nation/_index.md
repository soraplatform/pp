---
title: "A cash payment system between Pantrypoints Cities"
subtitle: "Derived from EF Schumacher's Multilateral Clearing"
logo: /logos/nation.png
image: /og/world.jpg
description: "Pantrypoints Nation is a clearing system between Pantrypoints Cities"
youtube: LmN4WlEghHE
# youtube: LYK6QnR2wMY
# youtube: "uyphJyQBgn8"
# youtube: "zZQnx0YGWyk"
# youtube: "-GRz8O-QD8Q"
# applink: https://hub.pantrypoints.com/signup
# apptext: "Register in the Waitlist"
applink: https://www.superphysics.org/research/schumacher/pool-clearing/part-1
apptext: "View his proposal"
aliases:
  /nation



# features:
personas:
  image: "/photos/ship2k.jpg"  
  title : "A Free Trade Nation"
  subtitle: "Free trade is the freedom from middle men"
  items:
    - name : "For Producers"
      icon: /icons/farmer.png
      # icon : "<svg  viewBox='0 0 512 512' height='50px' fill='dimgray'><path d='M64 96H0c0 123.7 100.3 224 224 224v144c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V320C288 196.3 187.7 96 64 96zm384-64c-84.2 0-157.4 46.5-195.7 115.2 27.7 30.2 48.2 66.9 59 107.6C424 243.1 512 147.9 512 32h-64z'/></svg>"
      content : "Get cash for your produce or manufactures"
      
    - name : "For Logistics"
      icon: /icons/bus.png
      # icon : "<svg  viewBox='0 0 640 512' height='50px' fill='dimgray'><path d='M50.2 375.6c2.3 8.5 11.1 13.6 19.6 11.3l216.4-58c8.5-2.3 13.6-11.1 11.3-19.6l-49.7-185.5c-2.3-8.5-11.1-13.6-19.6-11.3L151 133.3l24.8 92.7-61.8 16.5-24.8-92.7-77.3 20.7C3.4 172.8-1.7 181.6.6 190.1l49.6 185.5zM384 0c-17.7 0-32 14.3-32 32v323.6L5.9 450c-4.3 1.2-6.8 5.6-5.6 9.8l12.6 46.3c1.2 4.3 5.6 6.8 9.8 5.6l393.7-107.4C418.8 464.1 467.6 512 528 512c61.9 0 112-50.1 112-112V0H384zm144 448c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48z'/></svg>"
      content : "Know regular patterns in demand so you can optimize your costs"
      
    - name : "For Consumers"
      icon: /icons/appraise.png
      # icon : "<svg  viewBox='0 0 576 512' height='50px' fill='dimgray'><path d='M560 288h-80v96l-32-21.3-32 21.3v-96h-80c-8.8 0-16 7.2-16 16v192c0 8.8 7.2 16 16 16h224c8.8 0 16-7.2 16-16V304c0-8.8-7.2-16-16-16zm-384-64h224c8.8 0 16-7.2 16-16V16c0-8.8-7.2-16-16-16h-80v96l-32-21.3L256 96V0h-80c-8.8 0-16 7.2-16 16v192c0 8.8 7.2 16 16 16zm64 64h-80v96l-32-21.3L96 384v-96H16c-8.8 0-16 7.2-16 16v192c0 8.8 7.2 16 16 16h224c8.8 0 16-7.2 16-16V304c0-8.8-7.2-16-16-16z'/></svg>"
      content : "Get direct access to produce and manufactures to reduce inflation"
    
    - name : "For Government"
      icon: /icons/gov.png
      # icon : "<svg  viewBox='0 0 512 512' height='50px' fill='dimgray'><path d='M504.971 199.362l-22.627-22.627c-9.373-9.373-24.569-9.373-33.941 0l-5.657 5.657L329.608 69.255l5.657-5.657c9.373-9.373 9.373-24.569 0-33.941L312.638 7.029c-9.373-9.373-24.569-9.373-33.941 0L154.246 131.48c-9.373 9.373-9.373 24.569 0 33.941l22.627 22.627c9.373 9.373 24.569 9.373 33.941 0l5.657-5.657 39.598 39.598-81.04 81.04-5.657-5.657c-12.497-12.497-32.758-12.497-45.255 0L9.373 412.118c-12.497 12.497-12.497 32.758 0 45.255l45.255 45.255c12.497 12.497 32.758 12.497 45.255 0l114.745-114.745c12.497-12.497 12.497-32.758 0-45.255l-5.657-5.657 81.04-81.04 39.598 39.598-5.657 5.657c-9.373 9.373-9.373 24.569 0 33.941l22.627 22.627c9.373 9.373 24.569 9.373 33.941 0l124.451-124.451c9.372-9.372 9.372-24.568 0-33.941z'/></svg>"
      content : "Get information on supply and demand in real-time"



######################### Service #####################
how:
  title : "How it Works"
  steps:
    - image: "/screens/po1.jpg"
      id: 1
      content: "Post your product for sale"
    - image: "/screens/po3.jpg"
      id: 2
      content: "Get offers for your product"
    - image: "/photos/shake.jpg"
      id: 3
      content: "Accept offers and get paid in local currency to facilitate the sale"        
    # - title : ""
    #   images:
    #   - "https://sorasystem.sirv.com/screens/poolquote.jpg"
    #   content : "Pool is our implementation of the Pool Clearing proposal written by EF Schumacher as a solution to World War II"
    #   button:
    #     enable : true
    #     label : ""
    #     link : ""

req:
  title: Based on EF Schumacher's Multilateral Clearing
  link: "https://superphysics.org/solutions/pantrynomics/multilateral-pool-clearing"
  btext: "What's multilateral clearing?"
  avatar: "/avatars/ef.png"


# feedback:
#   title: "Feedback"
#   items:
#     - user : "intelli-network.com"
#       image: "/icons/intelli.jpg"
#       content: "Really awesome idea. I think you are spot on with focusing on the supply chain issues we just witnessed during the COVID crisis"
#       link: "https://intelli-network.com"
#     - user : "Arewa Kasuwa"
#       image: "/logos/arewa.png"
#       content: "I love the global trading idea. I would love to use your platform if it works well for Africans" 
#       link: "https://arewaKasuwa.com.ng"

# feedback:
#   title: Awards
#   item:
#     - user : DISH Blockchain Competition
#       image: https://sorasystem.sirv.com/logos/wv.jpg
#       content: Special Prize 2019
#       link: "https://worldvision.org.ph/news/social-innovation"

##################### Call to action #####################
cta:
  title : Register in the Waitlist
  link1text: "Sure!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "I'm not convinced yet"
  link2 : "https://superphysics.org/social/economics"
---

