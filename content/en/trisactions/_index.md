---
title: True Economic Democracy
# is an app for making urban life easier
subtitle: "Pantrypoints implements trisactions in order to create an Economy-as-a-Service or EaaS platform to facilitate True Economic Democracy" 
logo: "/logos/tri.png"
image: "/og/tri.jpg"
description: Hub is the central part of Pantry Points Network, a new economic system based on points
# youtube: "wKR3-nLxec8"
# youtube: "IRxnH_W43ko"
youtube: 6b9R04CgS6w
applink: https://hub.pantrypoints.com/signup
apptext: "Register in the Waitlist"
aliases:
  "/hub"

# , a ready economic system out-of-the-box designed to work with or without money"


# personas: 
#   title: Introducing Tri-sactions
#   # title: "A Complete Economic System, Resilient Against Crises"
#   subtitle: "Trisactions allow money, moneyless, or metaverse transactions in order to allow the economy to run under any social conditions, allowing true economic freedom"
#   # subtitle: "Implements the time-tested, but forgotten principles of Classical Economics, from Socrates to Adam Smith, instead of Neoclassical Economics from Marshall to Keynes"
#   image: "/photos/yay.jpg"
#   items:
#     - name : "1: Moneyless"
#       # icon : "<svg viewBox='0 0 576 512' fill='darkcyan' height='50px'><path d='M96,128A64,64,0,1,0,32,64,64,64,0,0,0,96,128Zm0,176.08a44.11,44.11,0,0,1,13.64-32L181.77,204c1.65-1.55,3.77-2.31,5.61-3.57A63.91,63.91,0,0,0,128,160H64A64,64,0,0,0,0,224v96a32,32,0,0,0,32,32V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V383.61l-50.36-47.53A44.08,44.08,0,0,1,96,304.08ZM480,128a64,64,0,1,0-64-64A64,64,0,0,0,480,128Zm32,32H448a63.91,63.91,0,0,0-59.38,40.42c1.84,1.27,4,2,5.62,3.59l72.12,68.06a44.37,44.37,0,0,1,0,64L416,383.62V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V352a32,32,0,0,0,32-32V224A64,64,0,0,0,512,160ZM444.4,295.34l-72.12-68.06A12,12,0,0,0,352,236v36H224V236a12,12,0,0,0-20.28-8.73L131.6,295.34a12.4,12.4,0,0,0,0,17.47l72.12,68.07A12,12,0,0,0,224,372.14V336H352v36.14a12,12,0,0,0,20.28,8.74l72.12-68.07A12.4,12.4,0,0,0,444.4,295.34Z'/></svg>"
#       icon: /icons/moneyno.png  
#       content : "Moneyless transactions use stored-value barter which we call 'bardits' (barter-credits). These are measured in points which are pegged to grains. This implements the grain-based valuation in The Wealth of Nations by Adam Smith"

#     - name : "2: Money"
#       icon: /icons/money.png   
#       # icon : "<svg fill='darkcyan' height='50px' viewBox='0 0 640 512'><path d='M621.16 54.46C582.37 38.19 543.55 32 504.75 32c-123.17-.01-246.33 62.34-369.5 62.34-30.89 0-61.76-3.92-92.65-13.72-3.47-1.1-6.95-1.62-10.35-1.62C15.04 79 0 92.32 0 110.81v317.26c0 12.63 7.23 24.6 18.84 29.46C57.63 473.81 96.45 480 135.25 480c123.17 0 246.34-62.35 369.51-62.35 30.89 0 61.76 3.92 92.65 13.72 3.47 1.1 6.95 1.62 10.35 1.62 17.21 0 32.25-13.32 32.25-31.81V83.93c-.01-12.64-7.24-24.6-18.85-29.47zM48 132.22c20.12 5.04 41.12 7.57 62.72 8.93C104.84 170.54 79 192.69 48 192.69v-60.47zm0 285v-47.78c34.37 0 62.18 27.27 63.71 61.4-22.53-1.81-43.59-6.31-63.71-13.62zM320 352c-44.19 0-80-42.99-80-96 0-53.02 35.82-96 80-96s80 42.98 80 96c0 53.03-35.83 96-80 96zm272 27.78c-17.52-4.39-35.71-6.85-54.32-8.44 5.87-26.08 27.5-45.88 54.32-49.28v57.72zm0-236.11c-30.89-3.91-54.86-29.7-55.81-61.55 19.54 2.17 38.09 6.23 55.81 12.66v48.89z'/></svg>"
#       content: "'Money points' allow money to pay for the moneyless transactions via cash or cashless banking apps of the fiat economy"
#       # content : "Pantry allows points-transactions for donations, exchange (barter), and investments. This circulates value whatever the economic situation even during stagflation or financial crisis (when banks won't let you withdraw money in order to prevent bank runs), or when your customer delays his money payments."
      
#     - name : "3: Metaverse"
#       icon: /icons/eth.png   
#       # icon: <svg fill='darkcyan' height='50px' viewBox='0 0 320 512'><path d='M311.9 260.8L160 353.6 8 260.8 160 0l151.9 260.8zM160 383.4L8 290.6 160 512l152-221.4-152 92.8z'/></svg>
#       # icon : "<svg  viewBox='0 0 320 512' fill='darkcyan' class='text-center' height='50px'><path d='M296 160H180.6l42.6-129.8C227.2 15 215.7 0 200 0H56C44 0 33.8 8.9 32.2 20.8l-32 240C-1.7 275.2 9.5 288 24 288h118.7L96.6 482.5c-3.6 15.2 8 29.5 23.3 29.5 8.4 0 16.4-4.4 20.8-12l176-304c9.3-15.9-2.2-36-20.7-36z'/></svg>"
#       content : "The points can be converted to Pantry Coins to allow regulated transactions from the Metaverse via Ethereum. This is useful for cross-border transactions"



# featuresgrid:

personas:
  image: "/photos/shake.jpg"
  title : "The Points Economy"
  subtitle: "Unlike fiat money which is an arbitrary store of value, points are pegged to rice or grains. This allows easier points-taxation, points-accounting and analytics, while eliminating the need for currency exchange."
  # All of these use the time-factor which is also essential for a high EQ or emotional quotient, as opposed to the money system which requires a high IQ and selfish-interest (in Pantrypoints, machine learning will help with the IQ part)"
  items:
    - name : Loyalty Points
      icon: /icons/loy.png
      # icon : "<svg height='100px' fill='gold' viewBox='0 0 512 512'><path d='M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z'/></svg>"
      content : "These are points given by businesses to their customers to incentivize repeat purchases. This is used to 'train' people in the use of points"
      footer: "<span class='has-text-dark'>Target Date: Deployed</span>"
    - name : Donation Points
      icon: /icons/donate.png
      # icon : "<svg height='100px' fill='hotpink' viewBox='0 0 512 512'><path d='M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z'/></svg>"    
      content : "These are points used for donations and relief. We use this primarily to incentivize [food rescue](https://circle.pantrypoints.com) and plastic waste collection to realize a moneyless circular economy"
      footer: "<span class='has-text-dark'>Target Date: 2023</span>"
    - name : Exchange Points
      icon: /icons/swap.png
      #icon : "<svg height='100px' fill='darkcyan' viewBox='0 0 512 512'><path d='M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z'/></svg>"
      content : "These are points for barter transactions. Unlike social points which do not seek something in return, trade points do. This is the foundation of Points-banking"
      footer: "<span class='has-text-dark'>Target Date: 2023</span>"      
    - name : Investment Points
      icon: /icons/invest.png
      # icon : "<svg height='100px' fill='dodgerblue' viewBox='0 0 512 512'><path d='M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z'/></svg>"
      content : "These are trade points designed to spur investments when money is lacking. This requires the other point-types to be working beforehand."
      # link: /services/pantrylitics
      footer: "<span class='has-text-dark'>Target Date: 2025</span>"      
    - name : Insurance Points
      icon: /icons/insure.png
      icon : "<svg height='100px' fill='purple' viewBox='0 0 512 512'><path d='M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z'/></svg>"
      content : "These are trade points for long term claim, upon old age. We implement this as social insurance in case money-based insurance becomes oppressive"
      footer: "<span class='has-text-dark'>Target Date: 2022</span>"
    - name : Energy Points
      icon: /icons/energy.png
      # icon : "<svg height='100px' fill='black' viewBox='0 0 512 512'><path d='M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z'/></svg>"
      content : "These are trade points for electricity, usually from biomass sources. This incentivizes waste segregation and is part of the circular economy"
      footer: "<span class='has-text-dark'>Target Date: 2022</span>"
    - name : Money Points
      icon: /icons/money.png
      # icon : "<svg height='100px' fill='forestgreen' viewBox='0 0 512 512'><path d='M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z'/></svg>"
      content : "These are trade points paid in money to reduce existing trade points balance"
      footer: "<span class='has-text-dark'>Target Date: 2023</span>"
    - name : Tax Points
      icon: /icons/tax.png
      # icon : "<svg height='100px' fill='tomato' viewBox='0 0 512 512'><path d='M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z'/></svg>"
      content : "These are points given by businesses to their customers to incentivize repeat purchases. This is used to 'train' people in the use of points"
      footer: "<span class='has-text-dark'>Target Date: 2027</span>"


segment:
  articles:
    - title : "Facilitates Economy as a Service (EaaS)"
      image: "/graphics/eaas.png"
      content : "Pantrypoints and Pantrypreneur make up our Economy as a Service system that allows it to integrate with the current money-system. <p>Pantrypoints allows points-transaction between independent users.</p><p>Pantrypreneur digitizes common business operations to increase efficiency and funnel productivity towards the Pantrypoints system</p>"

#   image: "/photos/holdhands.jpg"
#   items:
#     - image: "/graphics/eaas.png"
#       text: " Community Pantrypoints and Business Pantrypoints implement Trisactions primarily through Pantrypoints City"
#       linktext: "What is Pantrypoints City?"
#       link: "/trisactions/city"
    # - image: "/graphics/pantrypreneur.jpg"
    #   text: " Pantrypreneur is a platform for micro and small businesses that connects to each other via Pantrypoints City"
    #   linktext: "What is Pantrypreneur?"
    #   link: "/pantrypreneur"




circles1:
  title : "Current Implementations"
  subtitle: "The Pantrypoints system is currently being tested by the following"
  cards:
    - name : SCENAC
      image : /icons/scenac.png
      content : "SCENAC is testing Point Cards as a moneyless social insurance system"
      footer: "<span class='has-text-dark'>Las Pinas</span>"
    - name : Angel's Shelter 
      image : /icons/angels.png
      content : "Angel's Shelter is an animal shelter that uses Pantrypoints Build to get moneyless donations"
      footer: "<span class='has-text-dark'>Binan Laguna</span>"
    - name : Savilas Vietnam
      image : /icons/savilas.png
      content : "Savilas is a new seafood buyer testing exports via Pantrypoints World"
      # link: pantrylitics
      footer: "<span class='has-text-dark'>Ho Chi Minh</span>"
    - name : Food Rescue Philippines
      image : /icons/foodrescueph.png
      content : "Food Rescue Philippines is a volunteer group that will test Pantrypoints Circle for incentivizing food donations"
      # link: pantrylitics
      footer: "<span class='has-text-dark'>Mandaluyong</span>"

# circles1:
#   title : "Social Resource Planning (SRP) Platforms"
#   subtitle: "These serve the different point types to create the points-economy as a sustainable alternative to the crisis-prone money-economy"
#   cards:
#     - name : Hub
#       image : /icons/pantry.png
#       content : "A hyperlocal plaform that facilitates points-based transactions"
#       link: home
#       footer: "<span class='has-text-dark'>Target Date: Rolling Deploy</span>"
#     - name : Nation
#       image : /icons/nation.png
#       content : "A free trade system between Pantry Hubs that allows both cash and kind payments"
#       link: nation
#       footer: "<span class='has-text-dark'>Target Date: Depends on the Hubs</span>"
#     - name : World
#       image : /icons/w.png
#       content : "A world trade system that allows imports/exports with local currency or Trade Points"
#       link: world
#       footer: "<span class='has-text-dark'>Target Date: Depends on Pantry Nation</span>"
#     - name : Pantrylitics
#       image : /icons/pantrylitics.png
#       content : "Analytics for Pantry Hub and World data for real-time policymaking, inspired by the Bloomberg terminal"
#       link: pantrylitics
#       footer: "<span class='has-text-dark'>Target Date: Depends on the Hubs</span>"      


circles2:
  title : "Micro-Small Enterprise Resource Planning (MERP) Platforms"
  subtitle: "Designed for the digital transformation of micro and small businesses, as the heart of the new points-economy"
  cards:
    - name : "Circle"
      image : /icons/c.jpg
      # link: "/health"
      content: "A circular economy platform for communities that addresses food waste, plastic waste, and kitchen waste and turns them in to wealth"
      footer: "Target Date: <span class='text-info'>Prototyping (waiting for harvest)</span>"      

    - name : "Doc"
      image : /icons/d.png
      # link: "/health"
      content: "A booking platform for clinics that allows patients to transfer their records"
      footer: "Target Date: <span class='text-secondary'>Tentative (still looking for a clinic)</span>"      
      # content : "A health system that allows access to medical services and treatments for people without money"
      # footer: "<span class='has-text-dark'>Target Date: 2023</span>"

    - name : "Learn"
      image : "/icons/l.png"
      link: "learn"
      content : "A teaching administration platform for recording student attendance and performance"
      footer: "Target Date: <span class='text-secondary'>Tentative (still looking for a school)</span>"      
      # content : "A teaching administration platform for availing of trainings and apprenticeships without money"
      # footer: "<span class='has-text-dark'>Target Date: 2024</span>"

    - name : "Pantrypreneur"
      image : "/icons/p.png"
      link: "preneur"      
      content : "A platform for micro and small businesses that has an online bundy clock, job listing, customer rewards, and employee appraisal system"
      footer: "Target Date: <span class='text-success'>Deployed</span>"      


circles3:
  title : "Universal Features"
  subtitle: "Available to both social (SRP) and business (MERP) platforms"
  cards:
    - name : "Match"
      image : "/icons/match.png"
      link: "/match"
      content : "A matching system for users"
      footer: "<span class='has-text-dark'>Target Date: Rolling Deploy</span>"

    - name : Points Banking
      image : /icons/pb.png
      content : "Allows transfer of points between users, as well as facilitating Insurance Points and Investment Points"
      link: banking
      footer: "<span class='has-text-dark'>Target Date: Dependent on Hubs</span>"

    - name : Pointtax
      image : /icons/pt.png
      link: "/pointtax"
      content : "A points-based taxation system"
      footer: "<span class='has-text-dark'>Target Date: Dependent on Govern</span>"

# that prevents tax evasion and increases tax revenue by spreading it to more people


circles4:
  title : "Features for Social (SRP) Platforms"
  subtitle: "Pantry provides features to facilitate a sustainable, integrated, circular economy"
  cards:
    # - name : Circle
    #   image : /icons/c.png
    #   content : "A points-based waste to wealth system that implements the circular economy"
    #   link: /circle
    #   footer: "<span class='has-text-dark'>Target Date: 2023</span>"

    # - name : "Beacon"
    #   image : /icons/pb.png
    #   link: "beacon"
    #   content : "A local news system that funds local journalists and writers so they don't have to sell their integrity"
    #   footer: "<span class='has-text-dark'>Platforms: Hub</span>"

    # - name : Circle
    #   image : /icons/pc.png
    #   content : "A points-based donations system that builds a circular economy"
    #   link: /circle
    #   footer: "<span class='has-text-dark'>Target Date: 2023</span>"

    - name : Health
      image : /icons/ph.png
      content : "A health platform that allows alternative medicine and moneyless payments"
      link: /health
      footer: "Target Date: <span class='text-info'>Dependent on Doc</span>"

    - name : Invest
      image : /icons/pi.png
      content : "A points-based investment system"
      link: /invest
      footer: "Target Date: <span class='text-info'>Dependent on Points-banking</span>"

    - name : "Govern"
      image : /icons/pg.png
      link: "/govern"      
      content : "A governance feature that helps make government services more efficient"
      footer: "Target Date: <span class='text-info'>Dependent on Hubs</span>"


circles5:
  title : "Features for Business (MERP) Platforms"
  subtitle: "Pantry provides features for micro and small businesses to accomplish common business tasks in order to help them keep up with giant corporations at a fraction of the cost"
  cards:
    - name : Appraise
      image : /icons/pa.png
      content : "An appraisal system for students and employees"
      # link: /appraise
      footer: "Target Date: <span class='text-info'>2024</span>"

    - name : Jobs
      image : /icons/pj.png
      content : "A job application system that integrates personality questionnaire"
      link: /jobs
      footer: "Target Date: <span class='text-info'>2023</span>"

    - name : "Rewards"
      image : "/icons/pr.png"
      link: "/rewards"      
      content : "A rewards platform for customers to encourage repeat purchases"
      footer: "Target Date: <span class='text-success'>Deployed</span>"

    - name : "Sched"
      image : "/icons/ps.png"
      # link: "/rewards"      
      content : "A booking system for micro and small businesses"
      footer: "Target Date: <span class='text-info'>Tentative</span>"


# features:
#   image: "/photos/freelance.jpg"
#   title : "Everything that you need"
#   # subtitle: "With the following features"
#   items:
#     - name : "Jobs"
#       icon : "<svg  viewBox='0 0 448 512' fill='gray' height='50px'><path d='M325.4 289.2L224 390.6 122.6 289.2C54 295.3 0 352.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-70.2-54-127.1-122.6-133.2zM32 192c27.3 0 51.8-11.5 69.2-29.7 15.1 53.9 64 93.7 122.8 93.7 70.7 0 128-57.3 128-128S294.7 0 224 0c-50.4 0-93.6 29.4-114.5 71.8C92.1 47.8 64 32 32 32c0 33.4 17.1 62.8 43.1 80-26 17.2-43.1 46.6-43.1 80zm144-96h96c17.7 0 32 14.3 32 32H144c0-17.7 14.3-32 32-32z'/></svg>"
#       #content : "Find or post jobs in your city"

#     - name : "Marketplaces"
#       icon : "<svg  viewBox='0 0 640 512' fill='gray' height='50px'><path d='M320 384H128V224H64v256c0 17.7 14.3 32 32 32h256c17.7 0 32-14.3 32-32V224h-64v160zm314.6-241.8l-85.3-128c-6-8.9-16-14.2-26.7-14.2H117.4c-10.7 0-20.7 5.3-26.6 14.2l-85.3 128c-14.2 21.3 1 49.8 26.6 49.8H608c25.5 0 40.7-28.5 26.6-49.8zM512 496c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V224h-64v272z'/></svg>"
#       # content : "Find or post products and pay with points"

#     - name : "Public Transporation Map"
#       icon : "<svg  viewBox='0 0 512 512' fill='gray' height='50px'><path d='M488 128h-8V80c0-44.8-99.2-80-224-80S32 35.2 32 80v48h-8c-13.25 0-24 10.74-24 24v80c0 13.25 10.75 24 24 24h8v160c0 17.67 14.33 32 32 32v32c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32v-32h192v32c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32v-32h6.4c16 0 25.6-12.8 25.6-25.6V256h8c13.25 0 24-10.75 24-24v-80c0-13.26-10.75-24-24-24zM160 72c0-4.42 3.58-8 8-8h176c4.42 0 8 3.58 8 8v16c0 4.42-3.58 8-8 8H168c-4.42 0-8-3.58-8-8V72zm-48 328c-17.67 0-32-14.33-32-32s14.33-32 32-32 32 14.33 32 32-14.33 32-32 32zm128-112H128c-17.67 0-32-14.33-32-32v-96c0-17.67 14.33-32 32-32h112v160zm32 0V128h112c17.67 0 32 14.33 32 32v96c0 17.67-14.33 32-32 32H272zm128 112c-17.67 0-32-14.33-32-32s14.33-32 32-32 32 14.33 32 32-14.33 32-32 32z'/></svg>"
#       # content : "Know the bus and train routes and schedules"
      
#     - name : "Events"
#       icon : "<svg  viewBox='0 0 640 512' fill='gray' height='50px'><path d='M206.86 245.15c-35.88 10.45-59.95 41.2-57.53 74.1 11.4-12.72 28.81-23.7 49.9-30.92l7.63-43.18zM95.81 295L64.08 115.49c-.29-1.62.28-2.62.24-2.65 57.76-32.06 123.12-49.01 189.01-49.01 1.61 0 3.23.17 4.85.19 13.95-13.47 31.73-22.83 51.59-26 18.89-3.02 38.05-4.55 57.18-5.32-9.99-13.95-24.48-24.23-41.77-27C301.27 1.89 277.24 0 253.32 0 176.66 0 101.02 19.42 33.2 57.06 9.03 70.48-3.92 98.48 1.05 126.58l31.73 179.51c14.23 80.52 136.33 142.08 204.45 142.08 3.59 0 6.75-.46 10.01-.8-13.52-17.08-28.94-40.48-39.5-67.58-47.61-12.98-106.06-51.62-111.93-84.79zm97.55-137.46c-.73-4.12-2.23-7.87-4.07-11.4-8.25 8.91-20.67 15.75-35.32 18.32-14.65 2.58-28.67.4-39.48-5.17-.52 3.94-.64 7.98.09 12.1 3.84 21.7 24.58 36.19 46.34 32.37 21.75-3.82 36.28-24.52 32.44-46.22zM606.8 120.9c-88.98-49.38-191.43-67.41-291.98-51.35-27.31 4.36-49.08 26.26-54.04 54.36l-31.73 179.51c-15.39 87.05 95.28 196.27 158.31 207.35 63.03 11.09 204.47-53.79 219.86-140.84l31.73-179.51c4.97-28.11-7.98-56.11-32.15-69.52zm-273.24 96.8c3.84-21.7 24.58-36.19 46.34-32.36 21.76 3.83 36.28 24.52 32.45 46.22-.73 4.12-2.23 7.87-4.07 11.4-8.25-8.91-20.67-15.75-35.32-18.32-14.65-2.58-28.67-.4-39.48 5.17-.53-3.95-.65-7.99.08-12.11zm70.47 198.76c-55.68-9.79-93.52-59.27-89.04-112.9 20.6 25.54 56.21 46.17 99.49 53.78 43.28 7.61 83.82.37 111.93-16.6-14.18 51.94-66.71 85.51-122.38 75.72zm130.3-151.34c-8.25-8.91-20.68-15.75-35.33-18.32-14.65-2.58-28.67-.4-39.48 5.17-.52-3.94-.64-7.98.09-12.1 3.84-21.7 24.58-36.19 46.34-32.37 21.75 3.83 36.28 24.52 32.45 46.22-.73 4.13-2.23 7.88-4.07 11.4z'/></svg>"
#       # content : "Find events"
      
#     - name : "Education"
#       icon : "<svg  viewBox='0 0 640 512' fill='gray' height='50px'><path d='M206.86 245.15c-35.88 10.45-59.95 41.2-57.53 74.1 11.4-12.72 28.81-23.7 49.9-30.92l7.63-43.18zM95.81 295L64.08 115.49c-.29-1.62.28-2.62.24-2.65 57.76-32.06 123.12-49.01 189.01-49.01 1.61 0 3.23.17 4.85.19 13.95-13.47 31.73-22.83 51.59-26 18.89-3.02 38.05-4.55 57.18-5.32-9.99-13.95-24.48-24.23-41.77-27C301.27 1.89 277.24 0 253.32 0 176.66 0 101.02 19.42 33.2 57.06 9.03 70.48-3.92 98.48 1.05 126.58l31.73 179.51c14.23 80.52 136.33 142.08 204.45 142.08 3.59 0 6.75-.46 10.01-.8-13.52-17.08-28.94-40.48-39.5-67.58-47.61-12.98-106.06-51.62-111.93-84.79zm97.55-137.46c-.73-4.12-2.23-7.87-4.07-11.4-8.25 8.91-20.67 15.75-35.32 18.32-14.65 2.58-28.67.4-39.48-5.17-.52 3.94-.64 7.98.09 12.1 3.84 21.7 24.58 36.19 46.34 32.37 21.75-3.82 36.28-24.52 32.44-46.22zM606.8 120.9c-88.98-49.38-191.43-67.41-291.98-51.35-27.31 4.36-49.08 26.26-54.04 54.36l-31.73 179.51c-15.39 87.05 95.28 196.27 158.31 207.35 63.03 11.09 204.47-53.79 219.86-140.84l31.73-179.51c4.97-28.11-7.98-56.11-32.15-69.52zm-273.24 96.8c3.84-21.7 24.58-36.19 46.34-32.36 21.76 3.83 36.28 24.52 32.45 46.22-.73 4.12-2.23 7.87-4.07 11.4-8.25-8.91-20.67-15.75-35.32-18.32-14.65-2.58-28.67-.4-39.48 5.17-.53-3.95-.65-7.99.08-12.11zm70.47 198.76c-55.68-9.79-93.52-59.27-89.04-112.9 20.6 25.54 56.21 46.17 99.49 53.78 43.28 7.61 83.82.37 111.93-16.6-14.18 51.94-66.71 85.51-122.38 75.72zm130.3-151.34c-8.25-8.91-20.68-15.75-35.33-18.32-14.65-2.58-28.67-.4-39.48 5.17-.52-3.94-.64-7.98.09-12.1 3.84-21.7 24.58-36.19 46.34-32.37 21.75 3.83 36.28 24.52 32.45 46.22-.73 4.13-2.23 7.88-4.07 11.4z'/></svg>"
      #content : "Unlike other events apps that combine events from different cities, Pantry Hub focuses on events only for that city"      
    # - name : "Permit processes"
    #   icon : "<svg  viewBox='0 0 384 512' fill='gray' height='50px'><path d='M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z'/></svg>"
    #   content : "Want to renovate your house? Know the government regulations first"
    
    # - name : "Participation"
    #   icon : "<svg  viewBox='0 0 448 512' fill='gray' height='50px'><path d='M408.781 128.007C386.356 127.578 368 146.36 368 168.79V256h-8V79.79c0-22.43-18.356-41.212-40.781-40.783C297.488 39.423 280 57.169 280 79v177h-8V40.79C272 18.36 253.644-.422 231.219.007 209.488.423 192 18.169 192 40v216h-8V80.79c0-22.43-18.356-41.212-40.781-40.783C121.488 40.423 104 58.169 104 80v235.992l-31.648-43.519c-12.993-17.866-38.009-21.817-55.877-8.823-17.865 12.994-21.815 38.01-8.822 55.877l125.601 172.705A48 48 0 0 0 172.073 512h197.59c22.274 0 41.622-15.324 46.724-37.006l26.508-112.66a192.011 192.011 0 0 0 5.104-43.975V168c.001-21.831-17.487-39.577-39.218-39.993z'/></svg>"
    #   content : "What paint color should the city hall use? Vote for local ordinances"

      # - name : "Real Estate Rentals"
      #   icon : "<svg  viewBox='0 0 576 512' fill='gray' height='50px'><path d='M560 64c8.84 0 16-7.16 16-16V16c0-8.84-7.16-16-16-16H16C7.16 0 0 7.16 0 16v32c0 8.84 7.16 16 16 16h15.98v384H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h240v-80c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16v80h240c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16h-16V64h16zm-304 44.8c0-6.4 6.4-12.8 12.8-12.8h38.4c6.4 0 12.8 6.4 12.8 12.8v38.4c0 6.4-6.4 12.8-12.8 12.8h-38.4c-6.4 0-12.8-6.4-12.8-12.8v-38.4zm0 96c0-6.4 6.4-12.8 12.8-12.8h38.4c6.4 0 12.8 6.4 12.8 12.8v38.4c0 6.4-6.4 12.8-12.8 12.8h-38.4c-6.4 0-12.8-6.4-12.8-12.8v-38.4zm-128-96c0-6.4 6.4-12.8 12.8-12.8h38.4c6.4 0 12.8 6.4 12.8 12.8v38.4c0 6.4-6.4 12.8-12.8 12.8h-38.4c-6.4 0-12.8-6.4-12.8-12.8v-38.4zM179.2 256h-38.4c-6.4 0-12.8-6.4-12.8-12.8v-38.4c0-6.4 6.4-12.8 12.8-12.8h38.4c6.4 0 12.8 6.4 12.8 12.8v38.4c0 6.4-6.4 12.8-12.8 12.8zM192 384c0-53.02 42.98-96 96-96s96 42.98 96 96H192zm256-140.8c0 6.4-6.4 12.8-12.8 12.8h-38.4c-6.4 0-12.8-6.4-12.8-12.8v-38.4c0-6.4 6.4-12.8 12.8-12.8h38.4c6.4 0 12.8 6.4 12.8 12.8v38.4zm0-96c0 6.4-6.4 12.8-12.8 12.8h-38.4c-6.4 0-12.8-6.4-12.8-12.8v-38.4c0-6.4 6.4-12.8 12.8-12.8h38.4c6.4 0 12.8 6.4 12.8 12.8v38.4z'/></svg>"
      # content : "Find available and needed real estate for sale or for rent"

    # - name : "Utilities"
    #   icon : "<svg  viewBox='0 0 320 512' fill='gray' height='50px'><path d='M296 160H180.6l42.6-129.8C227.2 15 215.7 0 200 0H56C44 0 33.8 8.9 32.2 20.8l-32 240C-1.7 275.2 9.5 288 24 288h118.7L96.6 482.5c-3.6 15.2 8 29.5 23.3 29.5 8.4 0 16.4-4.4 20.8-12l176-304c9.3-15.9-2.2-36-20.7-36z'/></svg>"
    #   content : "Get to know the processes for availing of electricty, water, and telecoms"

    # - name : "Local News"
    #   icon : "<svg  viewBox='0 0 576 512' fill='gray' height='50px'><path d='M552 64H88c-13.255 0-24 10.745-24 24v8H24c-13.255 0-24 10.745-24 24v272c0 30.928 25.072 56 56 56h472c26.51 0 48-21.49 48-48V88c0-13.255-10.745-24-24-24zM56 400a8 8 0 0 1-8-8V144h16v248a8 8 0 0 1-8 8zm236-16H140c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h152c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12zm208 0H348c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h152c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12zm-208-96H140c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h152c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12zm208 0H348c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h152c6.627 0 12 5.373 12 12v8c0 6.627-5.373 12-12 12zm0-96H140c-6.627 0-12-5.373-12-12v-40c0-6.627 5.373-12 12-12h360c6.627 0 12 5.373 12 12v40c0 6.627-5.373 12-12 12z'/></svg>"
      # content : "Get local news in English and the local language. Each city has its own online newspaper!"



how:
  title : "How it Works"
  steps:
    - content: "Post your item using a points price instead of a money price"
      image: "/screens/pantryshow.jpg"
      id: 1
    - content: "Meetup with the buyers of your item"
      image: "/photos/shake.jpg"
      id: 2    
    - content: "Pay in points, cash, cashless, or ethereum, depending on availability"
      image: "/screens/pay.jpg"
      id: 3
      
    # - image: "/screens/register.png"
    #   id: 1
    #   content: "Register in the waitlist app and indicate your city"
    # - image: "/screens/po2.jpg"
    #   id: 2
    #   content: "We'll inform you when the app will be ready for your city"
    # - image: "/screens/po3.jpg"
    #   id: 3
    #   content: "Use the app for transactions with or without money"



# req:
#   title: Based on the requirements in Book 5, Chapter 2 of the Republic
#   link: "https://superphysics.org/research/socrates/simple-republic/book-5/chapter-2"
#   btext: "Read the requirements"
#   avatar: "/avatars/socrates.png"


##################### Call to action #####################
cta:
  enable : true
  title : "Do you want to do Trisactions?"
  link1text: "Yes, I'm in!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "I'm not convinced yet"
  link2 : "https://superphysics.org/social/economics"
---
