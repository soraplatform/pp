---
title: Bank the Unbanked
subtitle: Pantrypoints Banking is a low-cost moneyless banking system that circulates the economy unserved by the financial system 
logo: "/logos/banking.png"
image: "/og/banking.jpg"
description: "Pantrypoints Banking is a low-cost moneyless banking system that circulates the economy unserved by the financial system"
# youtube: "OV1JHTiDdkQ"
# youtube: zOVoXqOcI7E
youtube: WbV48G5dFtc
applink: https://hub.pantrypoints.com/signup
apptext: "Register in the Waitlist"
aliases:
  /banking
  /services/banking


# features:
personas:
  image: "/photos/inca.jpg" 
  # image: "https://res.cloudinary.com/nara/image/upload/v1567936990/photos/incacroplowres.jpg" 
  title: "Inspired by the Moneyless, Paperless system of the Inca"
  subtitle: "Points-banking allows the regulated transfer of points between users"
  items:
    - name : "Moneyless Ok!"
      icon: /icons/moneyno.png
      content : "Points Banking allows economies to work even after a total financial collapse"
    - name : "Decentralized"
      icon: /icons/decen.png    
      content : "Parties keep their own information preventing info asymmetry"
    - name : "Flexible"
      icon: /icons/cards.png
      content : "Points Banking does not use blockchain and can work offline, just as the Inca were offline"
    - name : "Asynchronous"
      icon: /icons/clock.png
      content : "Parties do not need to fulfill their part immediately"      
    - name : "Cost Effective"
      icon: /icons/money.png
      content : "Points Banking can be deployed on the cloud to reduce costs"
    - name : "Integrates with Taxation"
      content : "Points Banking will integrate with Pointtax to allow tax payments in kind"
      icon: /icons/tax.png
  # = image_tag 'https://sorasystem.sirv.com/photos/quipu.jpg', class: 'img-fluid rounded'


######################### How #####################

how:
  title : "How it Works"  
  steps:
    # - image: "https://sorasystem.sirv.com/photos/pantry.jpg"
    - id: 1
      content: "Gain points with other users and companies in the moneyless economy"  
      image: "/screens/po3.jpg"
    - id: 2 
      image: "/screens/xfer.png"
      content: "Transfer your points to other users to gain access to their goods and services"
    - id: 3
      image: "/screens/po1.jpg"
      content: "Use your points as usual"


req:
  title: Based on the requirements in the Wealth of Nations
  # Book 2, Chapter 2 of
  link: "https://superphysics.org/research/smith/wealth-of-nations/book-2/chapter-3c"
  btext: "Read the requirements"
  avatar: "/avatars/smith.png"



feedback:
  title: Awards
  items:
    - user : Startup Weekend Cambodia 2017
      image: https://sorasystem.sirv.com/logos/startupweekend.png
      content: Top 5 in the Fintech Edition as Debt Clearing System
      link: http://communities.techstars.com

    - user : Infinity Blockchain Labs Nationwide Vietnam Blockathon 2017
      image: https://sorasystem.sirv.com/logos/blockathon.jpg
      content: 2nd Place as Social ROSCA
      # link: http://blockchainlabs.asia


##################### Call to action #####################

cta:
  title : "Register in the Waitlist!"
  link1text: "Sure!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "I need to know more"
  link2 : "https://superphysics.org/social/economics/solutions/points-banking"

---
