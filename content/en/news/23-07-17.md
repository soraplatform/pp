---
title: "New Partner: Greenlife Coconut Products"
image: "/graphics/greenlife.jpg"
date: 2023-07-17
author: Jose Felizco
description: "We are happy to have Greenlife as our partner"
---


We are happy to have Greenlife as our partner. They are a manufacturer of coconut products in Tayabas Quezon, exporting to various countries.  

They will use our points system to increase sales of their coconut products locally, and will test it for cross-border transactions.

{{< img src="/graphics/greenlife.jpg" alt="Greenlife Coconut Products" >}}

