---
title: "Palm Pink Web"
image: "/screens/pink.jpg"
date: 2019-03-24
description: "We put up our Palm Pink Website!"
updates: "Dec 2020: Palm Pink is now ISAIAH Match" 
---


We put up our Palm Pink Website which impelements our proposed science of Bio Superphysics. It looks at patterns in one's palm to classify that person, using machine learning.  


{{< img src="/screens/pink.jpg" alt="Palm Pink" >}}

