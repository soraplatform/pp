---
title: "New Partner: Tabang Masa"
image: "/icons/tabang.png"
date: 2023-06-10
author: Jose Mordeno
description: "Tabang Masa is a grassroots organization that undertakes disaster relief and rescue"
# tags: ['Government']
categories: ['Partners']
---


We welcome Tabang Masa into the Pantrypoints system. 

Tabang Masa is a grassroots organization that undertakes disaster relief and rescue in Davao City.


{{< img src="/icons/tabang.png" alt="Tabang Masa" >}}

