---
title: "Pantrypreneur Educate Overview"
description: "Pantrypreneur Educate is a small-school administration system"
# lead: "Pantry Circle is designed for food rescue and plastic waste recycling"
date: 2020-10-13T15:21:01+02:00
lastmod: 2020-10-13T15:21:01+02:00
images: []
menu:
  docs:
    parent: "educate"
weight: 130
toc: true
---


Type | Description 
--- | ---
Mobile  | Pantrypreneur  Educate is a small-school offline administration system


{{< youtube aNVyHBc1CEI >}}


## Intent

It focuses on helping teachers manage their students. It comes from our own experience as teacher-students:

- Klock + Query 
- Appraise
- Jobs


---

## Models

See the individual features

## Changelog

- 2022-07: Abandoned Pantry Apprentice to focus on Pantrypreneur  Educate. Pantry Apprentice is now part of Pantrypreneur Work without the original intent.
