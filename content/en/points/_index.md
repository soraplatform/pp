---
title: Points Types
# is an app for making urban life easier
subtitle: "Here are the types of points in the Pantry system"
logo: "/logos/pantry.png"
image: "/og/city.jpg"
description: Hub is the central part of Pantry Points Network, a new economic system based on points


grid:
  items:
    - image: "/points/recycling.png"
    - image: "/points/tradebanking.png"
    - image: "/points/social.png"
    - image: "/points/loyalty.png"            
    - image: "/points/investment.png"
    - image: "/points/insurance.png"                                
    - image: "/points/tax.png"
---
