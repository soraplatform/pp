---
title : "The Circular Economy That Solves Inflation"
# "Get Points for What You Do
# Resilient Against Banking Crises
description: "Pantrypoints is a resilient and disruptive new economic system that uses bilateral points through meetups in order to stop inflation, allow full employment, and realize the circular economy"
# whatever the economic conditions
lead: "Pantrypoints is a resilient and disruptive new economic system that uses bilateral points through meetups in order to stop inflation, allow full employment, and realize the circular economy"
# wrong date
date: 2020-10-06T08:47:36+00:00
lastmod: 2022-07-27T08:47:36+00:00
youtube: AfByl9Mywi4
#youtube: "rVP73TV2X0s"
banner: "/photos/holdhands.jpg"
buttons:
  - label : "Register in the Waitlist"
    color: "primary"
    link : "https://hub.pantrypoints.com/signup"
  - label : "Check out the science"
    color: "info"
    link : "https://superphysics.org/social/economics/"



### COUNTRIES ###

countries:
  title : "Locations"
  subtitle: Here are the cities where we are prototyping our system. Feel free to add your city by registering in the waitlist. 
  # subtitle: "We'll deploy an app for your city once the waitlist for that city gets filled."
  flags:
    - name : "Las Pinas"
      flag : "/flags/ph.png"
    - name : "San Pedro Laguna"
      flag : "/flags/ph.png"
    - name : "Kuala Lumpur (coming soon!)"
      flag : "/flags/my.png"
    - name : "Hanoi (coming soon!)"
      flag : "/flags/vn.png"
    - name : "Saigon (coming soon!)"
      flag : "/flags/vn.png"      


req:
  title: Among the Mongols who are ignorant of the use of money, cattle are the measures of value. Wealth to them consisted in cattle, as to the Spaniards it consisted in gold and silver. Of the two, the Mongol notion, was nearest to the truth. (Adam Smith)
  # link: "https://superphysics.org/research/socrates/simple-republic/book-3/chapter-3"
  # btext: "View the requirements"
  avatar: "/avatars/smith.png"




### FEATURES ###

personas:
  title: Introducing Asynchronous Tri-sactions
  # title: "A Complete Economic System, Resilient Against Crises"
  subtitle: "Trisactions allow money, moneyless, or metaverse transactions without waiting for fulfillment from the other party. This allows the economy to run under any social conditions, allowing true economic freedom"
  # subtitle: "Implements the time-tested, but forgotten principles of Classical Economics, from Socrates to Adam Smith, instead of Neoclassical Economics from Marshall to Keynes"
  image: "/photos/yay.jpg"
  items:
    - name : "1: Moneyless"
      icon: /icons/moneyno.png
      # icon : "<svg viewBox='0 0 576 512' fill='darkcyan' height='50px'><path d='M96,128A64,64,0,1,0,32,64,64,64,0,0,0,96,128Zm0,176.08a44.11,44.11,0,0,1,13.64-32L181.77,204c1.65-1.55,3.77-2.31,5.61-3.57A63.91,63.91,0,0,0,128,160H64A64,64,0,0,0,0,224v96a32,32,0,0,0,32,32V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V383.61l-50.36-47.53A44.08,44.08,0,0,1,96,304.08ZM480,128a64,64,0,1,0-64-64A64,64,0,0,0,480,128Zm32,32H448a63.91,63.91,0,0,0-59.38,40.42c1.84,1.27,4,2,5.62,3.59l72.12,68.06a44.37,44.37,0,0,1,0,64L416,383.62V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V352a32,32,0,0,0,32-32V224A64,64,0,0,0,512,160ZM444.4,295.34l-72.12-68.06A12,12,0,0,0,352,236v36H224V236a12,12,0,0,0-20.28-8.73L131.6,295.34a12.4,12.4,0,0,0,0,17.47l72.12,68.07A12,12,0,0,0,224,372.14V336H352v36.14a12,12,0,0,0,20.28,8.74l72.12-68.07A12.4,12.4,0,0,0,444.4,295.34Z'/></svg>"
      content : "Moneyless transactions use stored-value barter measured in points which are pegged to grains. This implements the grain-based valuation mentioned in The Wealth of Nations by Adam Smith"

    - name : "2: Money"
      icon: /icons/money.png
      # icon : "<svg fill='darkcyan' height='50px' viewBox='0 0 640 512'><path d='M621.16 54.46C582.37 38.19 543.55 32 504.75 32c-123.17-.01-246.33 62.34-369.5 62.34-30.89 0-61.76-3.92-92.65-13.72-3.47-1.1-6.95-1.62-10.35-1.62C15.04 79 0 92.32 0 110.81v317.26c0 12.63 7.23 24.6 18.84 29.46C57.63 473.81 96.45 480 135.25 480c123.17 0 246.34-62.35 369.51-62.35 30.89 0 61.76 3.92 92.65 13.72 3.47 1.1 6.95 1.62 10.35 1.62 17.21 0 32.25-13.32 32.25-31.81V83.93c-.01-12.64-7.24-24.6-18.85-29.47zM48 132.22c20.12 5.04 41.12 7.57 62.72 8.93C104.84 170.54 79 192.69 48 192.69v-60.47zm0 285v-47.78c34.37 0 62.18 27.27 63.71 61.4-22.53-1.81-43.59-6.31-63.71-13.62zM320 352c-44.19 0-80-42.99-80-96 0-53.02 35.82-96 80-96s80 42.98 80 96c0 53.03-35.83 96-80 96zm272 27.78c-17.52-4.39-35.71-6.85-54.32-8.44 5.87-26.08 27.5-45.88 54.32-49.28v57.72zm0-236.11c-30.89-3.91-54.86-29.7-55.81-61.55 19.54 2.17 38.09 6.23 55.81 12.66v48.89z'/></svg>"
      content: "'Money points' allow money to pay for the moneyless transactions via cash or cashless banking apps of the fiat economy"
      # content : "Pantry allows points-transactions for donations, exchange (barter), and investments. This circulates value whatever the economic situation even during stagflation or financial crisis (when banks won't let you withdraw money in order to prevent bank runs), or when your customer delays his money payments."
      
    - name : "3: Metaverse"
      icon: /icons/eth.png    
      # icon: <svg fill='darkcyan' height='50px' viewBox='0 0 320 512'><path d='M311.9 260.8L160 353.6 8 260.8 160 0l151.9 260.8zM160 383.4L8 290.6 160 512l152-221.4-152 92.8z'/></svg>
      # icon : "<svg  viewBox='0 0 320 512' fill='darkcyan' class='text-center' height='50px'><path d='M296 160H180.6l42.6-129.8C227.2 15 215.7 0 200 0H56C44 0 33.8 8.9 32.2 20.8l-32 240C-1.7 275.2 9.5 288 24 288h118.7L96.6 482.5c-3.6 15.2 8 29.5 23.3 29.5 8.4 0 16.4-4.4 20.8-12l176-304c9.3-15.9-2.2-36-20.7-36z'/></svg>"
      content : "The points can be converted to Pantry Coins to allow regulated transactions from the Metaverse via Ethereum. This is useful for cross-border transactions and our proposed 'crypto-easing' (our alternative to quantitative easing)"
      


personas2: 
  title: Which are you?
  image: "/photos/freelance.jpg"
  items:
    - name : "I'm a teacher"
      icon: /icons/teacher.png
      content : "I need to take attendance and visualize student performance"
      link: /pantrypreneur/educate
      buttontext: Go
    - name : "I'm a micro-small business"
      icon: /icons/shop.png    
      content : "I need online presence as a website, web app, mobile app, or social media"
      link: /services/build/
      buttontext: Go
    - name : "I'm Unemployed"
      icon: /icons/jobless.png
      content : "I need to get a job but don't have the required skills"      
      link: https://circle.pantrypoints.com/
      buttontext: Go      
    - name : "I'm health-conscious"
      icon: /icons/apple.png
      content: "Healtcare is expensive so I need to eat right"
      link: /trisactions/health/
      buttontext: Go
    - name : "I'm an investor"
      icon: /icons/investor.png
      content: "I want my investments to make a lot of impact"
      link: /trisactions/invesure/
      buttontext: Go
    - name : "I'm a banker"
      icon: /icons/banker.png
      content: "I want a solution to banking crises. The stress is too much."
      link: /trisactions/banking/
      buttontext: Go      
    - name : "I'm a farmer"
      content: "I need a solution to global warming!"    
      icon: /icons/farmer.png
      link: /pantrypreneur/farm/
      buttontext: Go
    - name : "I'm with the Government"
      icon: /icons/gov.png
      content : "We need to earn more revenue for public services"
      link: /services/tax/
      buttontext: Go


how:
  title : "How it Works"  
  steps:
    - content: "Post your item using a points price instead of a money price"
      image: "/screens/pantryshow.jpg"
      id: 1
    - content: "Meetup with the buyers of your item"
      image: "/photos/shake.jpg"
      id: 2    
    - content: "Pay in points"
      image: "/screens/pay.jpg"
      id: 3
      

# triobutton:
#   title : "For Community, Conglomerates, and Citizens"
#   subtitle: "The Pantrypoints system will cover the whole economy, even world trade and taxation"
#   banner: "/photos/protests.jpg"
#   items:
#     - image: "/logos/comtri.png"
#       text: " Community Pantrypoints and Business Pantrypoints implement Trisactions through Pantrypoints City"
#       linktext: "What are Trisactions?"
#       link: "/trisactions"
#     - image: "/logos/pr.png"
#       text: " Pantrypreneur is a platform for micro and small businesses to get used to trisactions. Think of it as a micro-small ERP."
#       linktext: "What is Pantrypreneur?"
#       link: "/pantrypreneur"
#     - image: "/logos/services.png"
#       text: "Services facilitate the digital transformation of micro-small businesses at a low cost, getting them into the Pantrypoints and Pantrypreneur systems"
#       linktext: "What services are offered?"
#       link: "/services"      


# duo:
#   title : "Made for the Crisis Years"
#   subtitle: "The false assumptions by Neoclassical Economics naturally create immense wealth for the self at the expense of recurring crises for society. We started building Pantrypoints in 2015 for the long crisis which we predicted to start in 2019 caused by Keynesian 'liquidity preference' and Marginalist 'profit maximization'"
#   banner: "/photos/protests.jpg"
#   items:
#     - image: "/graphics/crisisyears.png"
#       imagetext: "Predictions from Pantrynomics"
#     - image: "/graphics/paper.jpg"
#       imagetext: "Paper"


# circles6:
#   title : "Services"
#   subtitle: "We help bring micro and small businesses into the digital world in order to transition them into points"
#   cards:
#     - name : "Build"
#       image : "/icons/b.png"
#       link: "/build"  
#       content : "We build websites and apps for micro and small businesses through points instead of money, as part of their digital transformation"
#       footer: "Target Date: <span class='text-success'>Ongoing</span>"

#     - name : "Predict"
#       image : /icons/ip.png
#       content : "We give guidance to micro and small businesses based on the social or economic predictions by our models"
#       link: /predict
#       footer: "Target Date: <span class='text-success'>Ongoing</span>"


duo2:
  title : "Originally Built by the Inca"
  subtitle: "The Inca made a large scale points-based economic system using strings for points. The Spanish destroyed it and replaced it with the current crisis-prone money-based system."
  banner: "/photos/inca.jpg"
  link: "https://gizmodo.com/the-greatest-mystery-of-the-inca-empire-was-its-strange-5872764"
  linktext: "Read how the Inca system worked"  
  items:
    - image: "/photos/qullqa.jpg"
      imagetext: "We implement the Qulqa system as pantries"
    - image: "/photos/quipu.jpg"
      imagetext: "We implement the Quipu strings system as points, thus Pantrypoints"


# numbers:
#   title : "Steady Progress"
#   subtitle: "Unlike the money system that has booms and busts, Pantrypoints has had steady growth despite crises, because it goes with Nature and not against it"
#   cards:
#     - value : "16"
#       name : "Partners"
#       footer: "<span class='has-text-dark'>As of December 2022</span>"
#     - value : "< 1,000"
#       name : "Registered users"
#       footer: "<span class='has-text-dark'>As of December 2022</span>"


feedback:
  title: "Feedback"
  items:
    - user : "Prof. Dhanjoo Ghista, University 2020 Foundation President"
      image: "/avatars/dhanjoo.jpg"
      content: "I very much like this work: A Research Proposal For The Formalization Of The Science Of Supereconomics And The Establishment Of A Point-Based Economic System"
      link: "http://www.dhanjooghista.com"
    - user : "Prof. Gavin Kennedy, a world expert on Adam Smith"
      image: "/avatars/gavin.jpg"
      content: "I believe it is developing into a worthwhile project" 
      link: "http://adamsmithslostlegacy.blogspot.com"
      

##################### Call to action #####################

cta:
  title : "Register in the Waitlist"
  link1text: "Sure!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "I need to know more"
  link2 : "https://superphysics.org/social/economics/fallacies/equilibrium-fallacy"

---
