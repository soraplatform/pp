---
title : "Tahan Terhadap Krisis Perbankan"
description: "Pantrypoints adalah sistem ekonomi yang tahan, yang menggunakan poin bilateral melalui pertemuan untuk menghentikan inflasi dan memungkinkan penyerapan tenaga kerja penuh, terlepas dari kondisi ekonomi"
lead: "Pantrypoints adalah sistem ekonomi yang tahan, yang menggunakan poin bilateral melalui pertemuan untuk menghentikan inflasi dan memungkinkan penyerapan tenaga kerja penuh, terlepas dari kondisi ekonomi"
# wrong date
date: 2020-10-06T08:47:36+00:00
lastmod: 2022-07-27T08:47:36+00:00
youtube: RCqxWx1zI_c
# youtube: "rVP73TV2X0s"


banner: "/photos/holdhands.jpg"
buttons:
  - label : "Daftar dalam Daftar Tunggu"
    color: "primary"
    link : "https://hub.pantrypoints.com/signup"
  - label : "Tingnan ang Supereconomics"
    color: "info"
    link : "https://superphysics.org/social/economics"


req:
  title: Bagi orang-orang Mongol yang tidak mengenal uang, seekor sapi adalah ukuran nilai. Kekayaan bagi mereka diukur dalam jumlah sapi, sama seperti bagi orang Spanyol kekayaan diukur dalam jumlah emas dan perak. Konsep Mongol lebih benar. (Adam Smith)
  # link: "https://superphysics.org/research/socrates/simple-republic/book-3/chapter-3"
  # btext: "View the requirements"
  avatar: "/avatars/smith.png"




triobutton:
  title : "Untuk Komunitas, Konglomerat, dan Warga."
  subtitle: "Sistem Pantrypoints akan mencakup seluruh ekonomi, termasuk perdagangan dunia dan perpajakan"
  banner: "/photos/protests.jpg"
  items:
    - image: "/logos/comtri.png"
      text: "Pantrypoints Komunitas dan Pantrypoints Bisnis mengimplementasikan Trisaksi melalui Pantrypoints City"
      linktext: "What are Trisactions?"
      link: "/trisactions"
    - image: "/logos/pr.png"
      text: "Pantrypreneur adalah platform bagi bisnis mikro dan kecil untuk beradaptasi dengan Trisaksi. Bayangkan itu sebagai sistem ERP mikro-kecil."
      linktext: "What is Pantrypreneur?"
      link: "/pantrypreneur"
    - image: "/logos/services.png"
      text: "Layanan memfasilitasi transformasi digital bisnis mikro dan kecil dengan biaya rendah, membawanya ke dalam sistem Pantrypoints dan Pantrypreneur."
      linktext: "What services are offered?"
      link: "/services"


### FEATURES ###

features: 
  title: Tri-sactions
  # title: "A Complete Economic System, Resilient Against Crises"
  # subtitle: "Sa trisactions, pwedeng mag-transact sa pamamagitan ng pera, barter, at cryptocurrencies para mapalaya ang ekonomiya."
  # subtitle: "Implements the time-tested, but forgotten principles of Classical Economics, from Socrates to Adam Smith, instead of Neoclassical Economics from Marshall to Keynes"
  image: "/photos/yay.jpg"
  items:
    - name : "1: Tak Beruang"
      icon : "<svg viewBox='0 0 576 512' fill='darkcyan' height='50px'><path d='M96,128A64,64,0,1,0,32,64,64,64,0,0,0,96,128Zm0,176.08a44.11,44.11,0,0,1,13.64-32L181.77,204c1.65-1.55,3.77-2.31,5.61-3.57A63.91,63.91,0,0,0,128,160H64A64,64,0,0,0,0,224v96a32,32,0,0,0,32,32V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V383.61l-50.36-47.53A44.08,44.08,0,0,1,96,304.08ZM480,128a64,64,0,1,0-64-64A64,64,0,0,0,480,128Zm32,32H448a63.91,63.91,0,0,0-59.38,40.42c1.84,1.27,4,2,5.62,3.59l72.12,68.06a44.37,44.37,0,0,1,0,64L416,383.62V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V352a32,32,0,0,0,32-32V224A64,64,0,0,0,512,160ZM444.4,295.34l-72.12-68.06A12,12,0,0,0,352,236v36H224V236a12,12,0,0,0-20.28-8.73L131.6,295.34a12.4,12.4,0,0,0,0,17.47l72.12,68.07A12,12,0,0,0,224,372.14V336H352v36.14a12,12,0,0,0,20.28,8.74l72.12-68.07A12.4,12.4,0,0,0,444.4,295.34Z'/></svg>"
      content : "Transaksi tanpa uang menggunakan barter dengan nilai yang disimpan dalam bentuk poin yang diikat pada biji-bijian. Ini mengimplementasikan penilaian berbasis biji-bijian yang disebutkan dalam The Wealth of Nations oleh Adam Smith."

    - name : "2: Uang"
      icon : "<svg fill='darkcyan' height='50px' viewBox='0 0 640 512'><path d='M621.16 54.46C582.37 38.19 543.55 32 504.75 32c-123.17-.01-246.33 62.34-369.5 62.34-30.89 0-61.76-3.92-92.65-13.72-3.47-1.1-6.95-1.62-10.35-1.62C15.04 79 0 92.32 0 110.81v317.26c0 12.63 7.23 24.6 18.84 29.46C57.63 473.81 96.45 480 135.25 480c123.17 0 246.34-62.35 369.51-62.35 30.89 0 61.76 3.92 92.65 13.72 3.47 1.1 6.95 1.62 10.35 1.62 17.21 0 32.25-13.32 32.25-31.81V83.93c-.01-12.64-7.24-24.6-18.85-29.47zM48 132.22c20.12 5.04 41.12 7.57 62.72 8.93C104.84 170.54 79 192.69 48 192.69v-60.47zm0 285v-47.78c34.37 0 62.18 27.27 63.71 61.4-22.53-1.81-43.59-6.31-63.71-13.62zM320 352c-44.19 0-80-42.99-80-96 0-53.02 35.82-96 80-96s80 42.98 80 96c0 53.03-35.83 96-80 96zm272 27.78c-17.52-4.39-35.71-6.85-54.32-8.44 5.87-26.08 27.5-45.88 54.32-49.28v57.72zm0-236.11c-30.89-3.91-54.86-29.7-55.81-61.55 19.54 2.17 38.09 6.23 55.81 12.66v48.89z'/></svg>"
      content: "'Poin uang' memungkinkan uang untuk digunakan dalam transaksi tanpa uang melalui aplikasi perbankan tunai atau non-tunai dalam ekonomi fiat."
    - name : "3: Metaverse"
      icon: <svg fill='darkcyan' height='50px' viewBox='0 0 320 512'><path d='M311.9 260.8L160 353.6 8 260.8 160 0l151.9 260.8zM160 383.4L8 290.6 160 512l152-221.4-152 92.8z'/></svg>
      content : "Poin-poin dapat dikonversi menjadi Pantry Coins untuk memungkinkan transaksi yang diatur dari Metaverse melalui Ethereum."
      
# Ini berguna untuk transaksi lintas batas dan proposal 'crypto-easing' kami (alternatif kami untuk pelonggaran kuantitatif).


duo2:
  title : "Dibangun oleh orang Inca."
  subtitle: "Orang Inca membuat sistem ekonomi tanpa uang dan tanpa kertas dalam skala besar yang menggunakan gudang lokal untuk menyimpan sumber daya, dan tali untuk mencatat data. Orang Spanyol menghancurkannya dan menggantinya dengan sistem berbasis uang yang rentan terhadap krisis saat ini."
  banner: "/photos/inca.jpg"
  link: "https://gizmodo.com/the-greatest-mystery-of-the-inca-empire-was-its-strange-5872764"
  linktext: "Baca bagaimana sistem Inca berfungsi."
  items:
    - image: "/photos/qullqa.jpg"
      imagetext: "Kami mengimplementasikan sistem Qulqa sebagai pantries."
    - image: "/photos/quipu.jpg"
      imagetext: "Kami mengimplementasikan sistem tali Quipu sebagai poin, sehingga Pantrypoints."


how:
  title : "Bagaimana Cara Kerjanya"
  steps:
    - content: "Pasang item Anda dengan harga poin alih-alih harga uang"
      image: "/screens/pantryshow.jpg"
      id: 1
    - content: "Bertemu dengan pembeli item Anda"
      image: "/photos/shake.jpg"
      id: 2    
    - content: "Bayar dengan poin"
      image: "/screens/pay.jpg"
      id: 3
      


feedback:
  title: "Feedback"
  items:
    - user : "Prof. Dhanjoo Ghista, University 2020 Foundation President"
      image: "/avatars/dhanjoo.jpg"
      content: "I very much like this work: A Research Proposal For The Formalization Of The Science Of Supereconomics And The Establishment Of A Point-Based Economic System"
      link: "http://www.dhanjooghista.com"
    - user : "Prof. Gavin Kennedy, a world expert on Adam Smith"
      image: "/avatars/gavin.jpg"
      content: "I believe it is developing into a worthwhile project" 
      link: "http://adamsmithslostlegacy.blogspot.com"


##################### Call to action #####################

cta:
  title : "Daftar dalam Daftar Tunggu"
  link1text: "Ya!"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "Tidak sekarang"
  link2 : "https://superphysics.org/social/economics/fallacies/equilibrium-fallacy"

---
